(function(){
	var locale = "ar";
	var rtl = true;
	var data = {
		"Node" : "Вузел",
		"Edge" : "Зв'язок",
		"Search" : "بحث",
		"Restore all hidden" : "استعادة جميع مخفي",
		"Filter":"فلتر",
		"Show all labels" : "Імена",
		"Show time panel" : "Стрічка часу",
		"Show map" : "Мапа",
		"Icon" : "Знак",
		"Font" : "Шрифт",
		"Font-size" : "Розмір шрифту",
		"Color" : "Колір",
		"Border" : "Кайма",
		"Type" : "Тип",
		"Width" : "Ширина",
		"Re-Arrange" : "Перебудувати",
		"Snapshot as": "Знімок",
		"All" : "Все",
		"Selected" : "Вибране",
		"Import JSON" : "Імпорт JSON",
		"Load Case data" : "Завантажити з АПИ",
		"Custom styled" : "Custom styled",
		"Delete note" : "Видалити",
		"Add note" : "Зберегти коментар"
	};
	VLA._registerLocale(locale, data, rtl);
})();
