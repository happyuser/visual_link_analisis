var custom_config = {
	filter_fields: ["degree","name", "id", "labelV", "__jobId", "__jobItemId"],
	view_fields: ["name", "id", "labelV"],

	default_type: "default",
	default_connection_type: "default",

	connection_types: [
		{
			name: "default",
			dir: "normal",
			color: "#838383",
			targetArrow: "triangle",
			sourceArrow: "none"
		},
		{
			name: "Family",
			dir: "mutual",
			color: "#00ff00",
			targetArrow: "triangle",
			sourceArrow: "none"
		},
		{
			name: "Colleague",
			dir: "none",
			color: "#0078ff",
			targetArrow: "triangle",
			sourceArrow: "none"
		},
		{
			name: "Professional",
			dir: "normal",
			color: "#00ffff",
			targetArrow: "triangle",
			sourceArrow: "none"
		},
		{
			name: "Factual",
			dir: "normal",
			color: "#000000",
			targetArrow: "triangle",
			sourceArrow: "none"
		},
		{
			name: "Concluded",
			dir: "normal",
			color: "#ff9d00",
			targetArrow: "triangle",
			sourceArrow: "none"
		}
	],

	types: [
		{
			name: "person",
			rule: "labelV='person'",
			icon: "user",
			fillColor: "green",
			borderColor: "orange"
		},
		{
			name: "website",
			rule: "labelV='website'",
			icon: "internet-explorer",
			fillColor: "blue"
		},
		{
			name: "company",
			rule: "labelV='company'",
			icon: "briefcase",
			fillColor: "#0078ff"
		},
		{
			name: "company_number",
			rule: "labelV='company_number'",
			icon: "barcode",
			fillColor: "#00d87b"
		},
		{
			name: "officer_id",
			rule: "labelV='officer_id'",
			icon: "tag",
			fillColor: "#00cee1"
		},
		{
			name: "location",
			rule: "labelV='location'",
			icon: "map-marker",
			fillColor: "#ff0000"
		}

	]
}
