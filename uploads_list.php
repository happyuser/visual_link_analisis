<?php
// commonStorageDocument/myDocuments
// ?userId=21220&caseseedId=1&orderBy=uploadedOn&orderIn={asc|desc}&pageNumber=1&recordsPerPage=10
// caseseedId, orderBy, orderIn, pageNumber, recordsPerPage are optional
//$_GET['userId'] = "21220";
//$_GET['caseSeedId'] = "12";

$userId = $_GET['userId'];
$caseSeedId = $_GET['caseSeedId'];
$pageNumber = max(1, (int)$_GET['pageNumber']);
$recordsPerPage = max(5, (int)$_GET['recordsPerPage']);

$api_url = "http://188.166.40.105:8080/elementexploration-1.0.7-SNAPSHOT/rest/api/";
$command = "commonStorageDocument/myDocuments";
$params = array('userId' => $userId, 'pageNumber' => $pageNumber);
$params['orderBy'] = 'uploadedOn';
$params['orderIn'] = 'desc';

if ($caseSeedId)
	$params['caseseedId'] = $caseSeedId;
if ($recordsPerPage)
	$params['recordsPerPage'] = $recordsPerPage;

$get_url = $api_url.$command."?".http_build_query($params);

$json_text = file_get_contents($get_url);

$data = json_decode($json_text);


$getDocCommand = "commonStorageDocument/downloadDocument";
// docId=1
$getDocParams = array("userId"=>21220);
//echo "<pre>";
//print_r($data);
//echo "</pre>";

$fields = array('docId','title','remark','size','type','uploadedOn','uploadedBy','modifiedBy','modifiedOn','docName');

echo "<table><tr>";
foreach ($fields as $field)
{
	echo "<th>$field</th>";
}
echo "</tr>";

foreach ($data->result as $doc)
{
	echo "<tr>";
	foreach ($fields as $field) {
		// 1467267367000
		$value = $doc->{$field};
		if ($value && ($field=='uploadedOn' || $field=='modifiedOn'))
		{
			$value = substr($value, 0, -3);
			$value = date("Y-m-d H:i", $value);
		}
		echo "<td>{$value}</td>";
	}

	$getDocParams['docId'] = $doc->docId;
	$view_url = $api_url.$getDocCommand."?".http_build_query($getDocParams);
	echo "<td><a href='$view_url'>download</a></td>";
	echo "</tr>";
}
echo "</table>";

/*
    [paginationInformation] => stdClass Object
        (
            [title] => /elementexploration-1.0.7-SNAPSHOT/rest/api/commonStorageDocument/myDocuments
            [kind] => list
            [totalResults] => 18
            [count] => 8
            [index] => 2
            [startIndex] => 0
            [inputEncoding] => utf-8
            [outputEncoding] => utf-8
        )
 */