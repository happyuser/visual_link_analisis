var gulp = require('gulp'),
	// csso = require('gulp-csso'), // minify CSS
	// myth = require('gulp-myth'), // Myth - http://www.myth.io/
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	cleanCSS = require('gulp-clean-css'),
	sourcemaps = require('gulp-sourcemaps');
//var stripDebug = require('gulp-strip-debug');

var version;

var paths = {
	dev: {
		files: {
			css: [
				'./src/css/normalize.css',
				'./src/css/google-fonts.css',
				'./src/css/font-awesome-4.6.3/css/font-awesome.css',
				'./src/css/selectric.css',
				'./src/css/jquery.qtip.css',
				'./src/css/cytoscape.js-navigator.css',
				'./src/css/cytoscape.js-panzoom.css',
				'./src/css/spectrum.css',
				'./src/css/ion.rangeSlider.css',
				'./src/css/ion.rangeSlider.skinModern.css'
			],
			jquery: './src/js-libs/jquery-1.12.4.js',
			// jquery: './src/js-libs/jquery-3.1.1.js',
			js: [
				'./src/js-libs/jquery.selectric.min.js',
				'./src/js-libs/typeahead.bundle.js',
				'./src/js-libs/jquery.qtip.js',
				'./src/js-libs/spectrum.js',
				'./src/js-libs/random-color.js',
				'./src/js-libs/cola.v3.min.js',
				'./src/js-libs/cytoscape_2.7.10.js',
				'./src/js-libs/cytoscape-cola.js',
				'./src/js-libs/cytoscape-cose-bilkent.js',
				'./src/js-libs/cytoscape-ngraph.forcelayout.js',
				'./src/js-libs/cytoscape-markov-cluster.js',
				'./src/js-libs/cytoscape-cxtmenu.js',
				'./src/js-libs/cytoscape-navigator.js',
				'./src/js-libs/cytoscape-panzoom.js',
				'./src/js-libs/cytoscape-qtip.js',
				'./src/js-libs/cytoscape-undo-redo.js',
				'./src/js-libs/gexf.min.js',
				'./src/js-libs/moment.js',
				'./src/js-libs/ion.rangeSlider.js'
			]
		},
		dest: {
			css: "./css/",
			js: "./js/"
		}
	},
	// UNFINISHED!!!!
	production: {
		files: {
			css: [
				'./src/css/normalize.css',
				'./src/css/google-fonts.css',
				'./src/css/font-awesome-4.6.3/css/font-awesome.css',
				'./src/css/selectric.css',
				'./src/css/jquery.qtip.css',
				'./src/css/cytoscape.js-navigator.css',
				'./src/css/cytoscape.js-panzoom.css',
				'./css/main.css'
			],
			jquery: './src/js-libs/jquery-1.12.4.js',
			// jquery: './src/js-libs/jquery-3.1.1.js',
			js: [
				'./src/js-libs/jquery.selectric.min.js',
				'./src/js-libs/typeahead.bundle.js',
				'./src/js-libs/jquery.qtip.js',
				'./src/js-libs/spectrum.js',
				'./src/js-libs/random-color.js',
				'./src/js-libs/cola.v3.min.js',
				'./src/js-libs/cytoscape_2.7.10.js',
				'./src/js-libs/cytoscape-cola.js',
				'./src/js-libs/cytoscape-cose-bilkent.js',
				'./src/js-libs/cytoscape-ngraph.forcelayout.js',
				'./src/js-libs/cytoscape-markov-cluster.js',
				'./src/js-libs/cytoscape-cxtmenu.js',
				'./src/js-libs/cytoscape-navigator.js',
				'./src/js-libs/cytoscape-panzoom.js',
				'./src/js-libs/cytoscape-qtip.js',
				'./src/js-libs/cytoscape-undo-redo.js',
				'./src/js-libs/gexf.min.js',
				'./js/app.js'
			]
		},
		dest: {
			css: "./css/",
			js: "./js/"
		}
	}
};

gulp.task('dev libs', ["dev libs:css", "dev libs:js"]);
gulp.task('build prod', ["prod: css", "prod: js"]);

gulp.task('dev libs:css', function() {
	"use strict";
	// css
	gulp.src(paths.dev.files.css)
		.pipe(sourcemaps.init())
		.pipe(concat('libs-bundle.css'))
//		.pipe(myth()) // добавляем префиксы - http://www.myth.io/
		.pipe(cleanCSS()) // минимизируем css
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.dev.dest.css)); // записываем css

	gulp.src("./src/css/font-awesome-4.6.3/fonts/*")
		.pipe(gulp.dest(paths.dev.dest.css+"../fonts/")); // записываем fonts
});

gulp.task('dev libs:js', function() {
	"use strict";

	var js_files_with_jquery = [paths.dev.files.jquery].concat(paths.dev.files.js);

	gulp.src(js_files_with_jquery)
		.pipe(sourcemaps.init())
		.pipe(concat('libs-bundle-jq.js'))
		//.pipe(uglify()) // минимизируем
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.dev.dest.js)); // записываем css

	gulp.src(paths.dev.files.js)
		.pipe(sourcemaps.init())
		.pipe(concat('libs-bundle.js'))
		//.pipe(uglify()) // минимизируем
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.dev.dest.js)); // записываем css
});










// ------ PRODUCTION

gulp.task('prod: css', function() {
	"use strict";
	// css
	gulp.src(paths.production.files.css)
		.pipe(concat('vla.css'))
		//		.pipe(myth()) // добавляем префиксы - http://www.myth.io/
		.pipe(cleanCSS()) // минимизируем css
		.pipe(gulp.dest('../PRODUCTION/css/')); // записываем css
});

gulp.task('prod: js', function() {
	"use strict";
	var js_files_with_jquery = [paths.production.files.jquery].concat(paths.production.files.js);

	gulp.src(js_files_with_jquery)
		.pipe(concat('vla.jq-bundled.min.js'))
		// .pipe(stripDebug())
		.pipe(uglify({drop_console:true})) // минимизируем
		.pipe(gulp.dest('../PRODUCTION/js/')); // записываем css


	gulp.src(paths.production.files.js)
		.pipe(concat('vla.min.js'))
		// .pipe(stripDebug())
		.pipe(uglify({drop_console:true})) // минимизируем
		.pipe(gulp.dest('../PRODUCTION/js/')); // записываем css
});
















gulp.task('version', function( next ){
	var now = new Date();
	version = process.env['VERSION'];

	if( version ){
		done();
	} else {
		exec('git rev-parse HEAD', function( error, stdout, stderr ){
			var sha = stdout.substring(0, 10); // shorten so not huge filename

			version = [ 'snapshot', sha, +now ].join('-');
			done();
		});
	}

	function done(){
		console.log('Using version number `%s` for building', version);
		next();
	}

});