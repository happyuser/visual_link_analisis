<?php
	$vla_options = array(
	"target_html_element" => "vla",
	"userId" => 21220,
	"layout" => "cose",
	"tip_event" => "click",
	"autoload" => array(
		"type" => "json",
		"caseId" => "santana",
		"limit" => 100
		)
	);
	if ($_GET['layout']) {
		$vla_options['layout'] = $_GET['layout'];
	}
	if ($_GET['loc'])
	{
		$vla_options['locale'] = $_GET['loc'];
	}
	if ($_GET['test1'])
	{
		$vla_options['debug'] = true;
		$vla_options['autoload'] = array(
			"type" => "json",
			"url" => "sample_graphs/getData_12_50.json"
		);
	}
	if ($_GET['file'])
	{
		$vla_options['debug'] = true;
		$vla_options['autoload'] = array(
			"type" => $_GET['type']? $_GET['type'] : 'json',
			"url" => $_GET['file']
		);
	}
	if ($_GET['test2'])
	{
		$vla_options['debug'] = true;
		$vla_options['autoload'] = array(
			"type" => "json",
			"url" => "sample_graphs/getData_12_100.json"
		);
	}
	if ($_GET['test_cluster'])
	{
		$vla_options['debug'] = true;
		$vla_options['autoload'] = array(
			"type" => "cytoscape",
			"url" => "sample_graphs/cluster_test1.json"
		);
	}
	if ($_GET['test_time'])
	{
		$vla_options['debug'] = true;
		$vla_options['autoload'] = array(
			"type" => "json",
			"url" => "sample_graphs/getData_12_50_timed.json"
		);
	}
	if ($_GET['test_map'])
	{
		$vla_options['layout'] = "cose";
		$vla_options['debug'] = true;
		$vla_options['autoload'] = array(
			"type" => "json",
			"url" => "sample_graphs/mapped2_noted.json"
		);
	}
	if ($_GET['caseId']) {
		$vla_options['autoload']['caseId'] = $_GET['caseId'];
	}
	if ($_GET['limit']) {
		$vla_options['autoload']['limit'] = $_GET['limit'];
	}

	$title = "";
	if ($vla_options['autoload']['caseId']) {
		$title = "case " .$vla_options['autoload']['caseId'];
	} else if ($vla_options['autoload']['url']) {
		$title = "file " .$vla_options['autoload']['url'];
	}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

	<title>VLA DEMO: <?php echo $title ?></title>

	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/libs-bundle.css" type="text/css" />
	<style type="text/css">
		body {
			font-family: arial, helvetica, sans-serif;
			font-size: 14px;
			margin: 0;
			padding: 0;
		}

		#vla {
			width: 100%;
			height: 100%;
			position: absolute;
			left: 0;
			top: 0;
			z-index: 999;
			overflow: hidden;
		}
	</style>
</head>
<body>
	<div id="vla"></div>

	<script src="js/libs-bundle-jq.js"></script>
	<script src="js/latlon-spherical.js"></script>
	<script src="js/app.js"></script>
	<script src="js/custom_config.js"></script>
	<script src="js/app_locale_ua.js"></script>
	<script src="js/app_locale_ar.js"></script>
	<script>
		var vla_inst;
		$(function(){
			var vla_options = <?php echo json_encode($vla_options) ?>;
			if (typeof custom_config != 'undefined')
				vla_options = $.extend({}, custom_config, vla_options);
			vla_inst = new VLA.Main(vla_options);
		});
	</script>
</body>
</html>
