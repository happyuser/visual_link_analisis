<?php
$caseId = $_GET["caseId"];
$limit = ((int) $_GET['limit'] > 0) ? (int) $_GET['limit'] : 100;

if (is_numeric($caseId)) {
	$ee_server = "http://188.166.40.105:8080/elementexploration-1.0.7-SNAPSHOT/rest/api/";
	$api_call_url = "caseseed/getData?caseSeedId=" .$caseId. "&limit=" .$limit;
} else {
	$ee_server = "http://95.85.58.85:3101/api/";
	$api_call_url = "cases/" . urlencode($caseId) . "/graph/" . $limit;
}

$res = get_url($ee_server . $api_call_url);


echo $res;

exit;

function get_url($url, $post_data=false, $return_headers = false)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	if ($post_data) {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
	}

	// receive server response ...
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// do not follow "Location" redirects
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

	if ($return_headers)
	{
		curl_setopt($ch, CURLOPT_HEADER, 1);
	}
	$server_output = curl_exec($ch);

	if (curl_error($ch))
	{
		$response = new stdClass();
		$response->error_n = curl_errno($ch);
		$response->error = curl_error($ch);
		return json_encode($response);
	}

	curl_close($ch);

	// further processing ....
	if ($server_output != "") {

		if ($return_headers) {
			$server_output = explode("\r\n\r\n", $server_output);

			$headers = array();
			foreach (explode("\r\n", $server_output[0]) as $i => $line) {
				if ($i === 0)
					$headers['http_code'] = $line;
				else {
					list ($key, $value) = explode(': ', $line);

					$headers[$key] = $value;
				}
			}
			$server_output[0] = $headers;
		}

		return $server_output;
	} else {
	}
	return "";
}
