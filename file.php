<?php
$local_filename = $_GET['fn'];
$download_filename = $_GET['f'];
if ($local_filename && file_exists("files/".$local_filename))
{
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Disposition: attachment;filename=" .$download_filename);
	header("Content-Transfer-Encoding: binary ");

	echo file_get_contents("files/".$local_filename);
	exit;
}
