/// <reference path="cytoscape.d.ts" />

//import * as m from 'cytoscape';


declare module Cy {
	export interface Instance {
		panzoom: any;
		//panzoom(options?: panzoomOptions): void;
	}
}

export interface panzoomOptions {
	/**
	 * zoom factor per zoom tick
	 *
	 * The default value is 0.05
	 */
	zoomFactor?: number;
	/**
	 * how many ms between zoom ticks
	 *
	 * The default value is 45
	 */
	zoomDelay?: number;
	/**
	 * min zoom level
	 *
	 * The default value is 0.1
	 */
	minZoom?: number;
	/**
	 * max zoom level
	 *
	 * The default value is 10
	 */
	maxZoom?: number;
	/**
	 * padding when fitting
	 *
	 * The default value is 50
	 */
	fitPadding?: number;
	/**
	 * how many ms in between pan ticks
	 *
	 * The default value is 10
	 */
	panSpeed?: number;
	/**
	 * max pan distance per tick
	 *
	 * The default value is 10
	 */
	panDistance?: number;
	/**
	 * the length of the pan drag box in which the vector for panning is calculated (bigger = finer control of pan speed and direction)
	 *
	 * The default value is 75
	 */
	panDragAreaSize?: number;
	/**
	 * the slowest speed we can pan by (as a percent of panSpeed)
	 *
	 * The default value is 0.25
	 */
	panMinPercentSpeed?: number;
	/**
	 * radius of inactive area in pan drag box
	 *
	 * The default value is 8
	 */
	panInactiveArea?: number;
	/**
	 * min opacity of pan indicator (the draggable nib); scales from this to 1.0
	 *
	 * The default value is 0.5
	 */
	panIndicatorMinOpacity?: number;
	/**
	 * a minimal version of the ui only with zooming (useful on systems with bad mousewheel resolution)
	 *
	 * The default value is false
	 */
	zoomOnly?: boolean;
	/**
	 * selector of elements to fit
	 *
	 * The default value is undefined
	 */
	fitSelector?: string;
	animateOnFit?: () => boolean;
	/**
	 * duration of animation on fit
	 *
	 * The default value is 1000
	 */
	fitAnimationDuration?: number;

	/**
	 * icon class names
	 *
	 * The default value is 'fa fa-minus'
	 */
	sliderHandleIcon?: string;
	/**
	 * icon class names
	 *
	 * The default value is 'fa fa-plus'
	 */
	zoomInIcon: string;
	/**
	 * icon class names
	 *
	 * The default value is 'fa fa-minus'
	 */
	zoomOutIcon: string;
	/**
	 * icon class names
	 *
	 * The default value is 'fa fa-expand'
	 */
	resetIcon: string;
}
