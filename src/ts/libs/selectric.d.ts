/// <reference path="jquery.d.ts" />

interface JQuery {
	selectric(options?: Object): JQuery;
	selectric(method: string): JQuery;
}