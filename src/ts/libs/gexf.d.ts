// Type definitions for gexf 0.2.5
// Project: https://github.com/Yomguithereal/gexf
// Definitions by: Vyacheslav Solovjov <https://github.com/XiaNi>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped


declare namespace gexf {
	export var version: string;

	type Colour = string; // 'rgb(xx,xx,xx)' or '#xxxxxx'

	type EdgeShape = 'solid' | 'dotted' | 'dashed' | 'double';
	interface EdgeVizDefinition {
		color?: Colour;
		thickness?: number;
		shape?: EdgeShape;
	}
	type EdgeType = 'directed' | 'undirected' | 'mutual';
	interface EdgeDefinition {
		id: string;
		source: string;
		target: string;
		type?: EdgeType;
		weight?: number;
		viz?: EdgeVizDefinition;
	}

	type NodeShape = 'disc' | 'square'| 'triangle'| 'diamond'| 'image'
	interface NodeVizDefinition {
		color?: Colour;
		size?: number;
		position?: {x: number,y: number,z: number};
		shape: NodeShape;
	}
	interface NodeDefinition {
		id: string | number;
		label: string;
		attributes?: any;
		viz?: NodeVizDefinition;
	}

	type AttributeType = 'integer' | 'long' | 'double' | 'float' | 'boolean' | 'liststring' | 'string' | 'anyURI';
	interface AttributeDefinition {
		id: string | number;
		type: AttributeType;
		title: string;
	}

	interface Model {
		node?: AttributeDefinition[]; // array of node possible attributes. see output data for precisions.
		edge?: AttributeDefinition[]; //array of edge possible attributes. see output data for precisions.
	}

	interface Meta {
		lastmodifieddate?: string; //
		creator?: string;
		keywords?: string;
		description: string;
	}

	interface WriterParams {
		meta?: Meta; //?object: an object of metadata for the graph.
		defaultEdgeType?: EdgeType; // ['undirected']: default edge type.
		encoding?: string; // ['UTF-8']: encoding of the XML file.
		mode?: "static" | "dynamic"; // mode of the graph. static or dynamic for instance.
		model?: Model; // object: an object containing the models of the nodes and/or edges.

		nodes?: NodeDefinition[] // array of nodes to pass at instantiation time.
		edges?: EdgeDefinition[] // : array of edges to pass at instantiation time.
		implementation?: DOMImplementation; // the DOM implementation to build the XML document. Will take the browser's one by default of xmldom's one in node.
		serializer?: XMLSerializer; //the XMLSerializer class to serialize the XML document. Will default to the browser's one or xmldom's one in node.
		namespace?: string; //?string ['http://www.gexf.net/1.2draft']: gexf_reader XML namespace to use.
		vizNamespace?: string; //?string ['http:///www.gexf.net/1.2draft/viz']: gexf_reader viz XML namespace to use.
		version?: string; // ?string ['1.2']: version of gexf_reader to produce.
	}

	/**
	 * Synchronously fetch the gexf file and parse it
	 * @param url
	 */
	export function fetch(url: string): any;

	/**
	 * Asynchronously fetch the gexf file and parse it
	 * @param url
	 * @param callback
	 */
	export function fetch(url: string, callback: (graph: any) => void);

	export function parse(string: string): any;

	export function create(params: WriterParams): writer;


	export class writer {
		document: XMLDocument;

		/**
		 * Adding a single node to the gexf_reader document.
		 * @param data
		 */
		addNode(data: NodeDefinition);

		/**
		 * Adding a single edge to the gexf_reader document.
		 * @param data
		 */
		addEdge(data: EdgeDefinition);

		/**
		 * Same as passing a meta parameter at instantiation.
		 * @param value
		 */
		setMeta(value: any);

		/**
		 * Same as passing a models.node parameter at instantiation.
		 * @param value
		 */
		setNodeModel(value: AttributeDefinition[]);

		/**
		 * Same as passing a models.edge parameter at instantiation.
		 * @param value
		 */
		setEdgeModel(value: AttributeDefinition[]);

		/**
		 * Add a single node attribute definition to the node model
		 * @param def
		 */
		addNodeAttribute(def);

		/**
		 * Add a single edge attribute definition to the edge model
		 * @param def
		 */
		addEdgeAttribute(def);

		/**
		 * Produce the string representation of the gexf_reader document.
		 */
		serialize(): string;
	}
}