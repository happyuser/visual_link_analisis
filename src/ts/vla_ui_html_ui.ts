namespace VLA {
	export namespace UI {
		export abstract class HTML_UI {
			protected parent_element: JQuery;
			protected html_element: JQuery;
			protected options:any;

			constructor(target_div: JQuery, options?:any) {
				this.options = options;
				this.parent_element = target_div;
				this.build_html_dom();
				this.bind_events();
				this.html_element.appendTo(this.parent_element);
			}

			protected abstract bind_events(): void;

			protected abstract build_html_dom(): void;

			public on(event_name, callback) {
				// console.log("bind.on[" + "vla."+event_name + "]", callback);
				this.parent_element.on("vla." + event_name, callback);
			}

			protected trigger(event_name: string, data?: any[]|Object) {
				// console.log("trigger[" + "vla."+event_name + "] on ", this.html_element);
				$(this.html_element).trigger("vla." + event_name, data);
			}

			addClass(class_name: string) {
				this.html_element.addClass(class_name);
			}

			removeClass(class_name: string) {
				this.html_element.removeClass(class_name);
			}

			hasClass(class_name: string) {
				this.html_element.hasClass(class_name);
			}

			hide() {
				this.html_element.hide();
			}

			show() {
				this.html_element.show();
			}
		}

		export abstract class HTML_INPUT extends HTML_UI {
			protected control: JQuery;

			value(val?) {
				if (this.control)
					if (val)
						this.control.val(val);
					else
						return this.control.val();
			}
		}
	}
}