/// <reference path="libs/jquery.d.ts" />
/// <reference path="vla_data.ts" />
/// <reference path="vla_ui_graph.ts" />
/// <reference path="vla_ui_panel.ts" />
/// <reference path="libs/typeahead.d.ts" />
'use strict';
var VLA;
(function (VLA) {
    var Main = (function () {
        function Main() {
            this.app_init_done = false;
            console.time("app constructor");
            // dependencies_loaded = load_dependencies();
        }
        Main.prototype.init = function (params) {
            console.time("app init");
            // $.when(dependencies_loaded).then( () => {
            // 	console.time("dependencies_loaded!");
            // 	this._init(target_html_element_id, userId, caseSeedId);
            // }, () => {
            // 	console.error("loading dependencies failed!");
            // }, (file:string, loaded:number, total:number) => {
            // 	console.info("progress", file, loaded, total);
            // });
            var defaults = {
                target_html_element: "",
                userId: 0,
            };
            this.options = $.extend({}, defaults, params);
            console.info("options", this.options);
            this._init();
        };
        Main.prototype._init = function () {
            var _this = this;
            console.time("app _init");
            this.viewport = $("#" + this.options.target_html_element);
            // this.userId = userId;
            // this.caseSeedId = caseSeedId;
            this.ui_graph = new VLA.UI.Graph(this.viewport);
            this.ui_panel = new VLA.UI.Panel(this.viewport);
            this.data_server = new VLA.DataServer(this.options.userId);
            var graph_ui_init_progress = this.ui_graph.init();
            // var data_load_progress = this.data_server.getRemote();
            var data_autoload_progress;
            if (this.options.autoload)
                data_autoload_progress = this.data_server.load_and_import(this.options.autoload);
            else {
                data_autoload_progress = $.Deferred();
                data_autoload_progress.resolve();
            }
            $.when(graph_ui_init_progress).then(function () {
                _this.data_server.set_graph_object(_this.ui_graph.cytoscape());
            });
            $.when(graph_ui_init_progress, data_autoload_progress)
                .then(function () {
                console.info('ui init done, remote data loaded');
                //console.time("ui_graph.add");
                //this.ui_graph.add(this.data.nodes, this.data.edges);
                //console.timeEnd("ui_graph.add");
                _this.data_server.add_data_on_graph();
                console.log("update ui");
                _this.ui_graph.update();
                _this.bindUI();
                _this.ui_graph.busy(false);
                console.timeEnd("app init");
            }, function (status) {
                console.log("Initialisations failed", status);
            }, function (status) {
                console.log("In Progress", status);
            });
        };
        Main.prototype.init_load_lib_scripts = function () {
            /*
            <script src="js-libs/typeahead.bundle.js"></script>
            <script src="js-libs/cytoscape.js"></script>
            <script src="js-libs/cytoscape-panzoom.js"></script>
            <script src="js-libs/cytoscape-cxtmenu.js"></script>
            <script src="js-libs/cytoscape-edgehandles.js"></script>

            <script src="js-libs/cola.v3.min.js"></script>
            <script src="js-libs/cytoscape-cola.js"></script>
            <script src="js-libs/cytoscape-cose-bilkent.js"></script>
            <script src="js-libs/cytoscape-spread.js"></script>
            */
        };
        Main.prototype.on_selection_changed = function () {
            var selected_nodes = this.ui_graph.selected_nodes();
            var selected_edges = this.ui_graph.selected_edges();
            if (selected_edges.length == 0 && (selected_nodes.length > 0 && selected_nodes.length < 3)) {
            }
            else if (selected_nodes.length == 0 && selected_edges.length == 1) {
            }
            else {
            }
            this.ui_panel.on_selected_nodes_change(selected_nodes.length, selected_edges.length);
            this.update_selection_info();
        };
        Main.prototype.bindUI = function () {
            var _this = this;
            this.ui_panel.on('layout-change', function (evt, layout_name) {
                _this.ui_graph.set_layout(layout_name);
            });
            this.ui_panel.on('show-labels-change', function (evt, value) {
                _this.ui_graph.set_labels_visible(value);
            });
            this.ui_panel.on('type-filter-change', function (evt, data) {
                if (data.state)
                    _this.ui_graph.show_nodes_by_type(data.type);
                else
                    _this.ui_graph.hide_nodes_by_type(data.type);
            });
            this.ui_panel.on('snapshot-all-click', function (e, snapshot_type) {
                if (snapshot_type == 'json') {
                    var json_data = _this.ui_graph.snapshot_json_all();
                    _this.data_server.upload_document_data("graph.json", JSON.stringify(json_data));
                }
                else {
                    var img64 = _this.ui_graph.snapshot_image(snapshot_type);
                    _this.data_server.upload_document_data("graph." + snapshot_type, img64);
                }
            });
            this.ui_panel.on('snapshot-selected-click', function (e, snapshot_type) {
                if (snapshot_type == 'json') {
                    var json_data = _this.ui_graph.snapshot_json_selected();
                    _this.data_server.upload_document_data("graph_part.json", JSON.stringify(json_data));
                }
                else {
                    var img64 = _this.ui_graph.snapshot_image(snapshot_type, true);
                    _this.data_server.upload_document_data("graph_part." + snapshot_type, img64);
                }
            });
            this.ui_panel.on('focus-depth-change', function (evt, depth) {
                var selected = _this.ui_graph.cytoscape().nodes(":selected");
                if (selected.length)
                    _this.ui_graph.focus(selected, depth);
            });
            this.ui_panel.on('show-hidden-click', function () {
                _this.ui_graph.cytoscape().elements().show();
            });
            this.ui_panel.on('graph-action', function (e, params) {
                var target = params.target;
                if (params.action == 'center-selected' || params.action == 'fit-selected') {
                    target = _this.ui_graph.cytoscape().$(":selected");
                }
                if (target && target.length) {
                    if (params.action == 'center' || params.action == 'center-selected')
                        _this.ui_graph.cytoscape().center(target);
                    else if (params.action == 'fit' || params.action == 'fit-selected')
                        _this.ui_graph.cytoscape().fit(target);
                    return;
                }
            });
            this.bindSearchUI();
            this.bindContextMenu();
            // TODO: do not use cytoscape here
            this.ui_graph.cytoscape().on('select', function (e) {
                _this.on_selection_changed();
            });
            this.ui_graph.cytoscape().on('unselect', function () {
                _this.on_selection_changed();
            });
            this.viewport.on("vla.graph-link-clicked", function (e, node_id) {
                var node = _this.ui_graph.cytoscape().filter("[id='" + node_id + "']");
                if (node.length) {
                    _this.ui_graph.cytoscape().zoom(1);
                    _this.ui_graph.cytoscape().stop().center(node);
                }
            });
            // UNFINISHED!!!
            var cy = this.ui_graph.cytoscape();
            cy.on('unselect', function (evt) {
                // console.log("unfocus", evt);
                _this.viewport.removeClass("vla-focused");
                _this.ui_graph.clear_focus();
            });
            cy.on('boxend', function (evt) {
                window.setTimeout(function () {
                    var selected = cy.filter(":selected");
                    if (selected.length)
                        cy.center(selected);
                }, 1000);
            });
        };
        Main.prototype.bindContextMenu = function () {
            var _this = this;
            var cy = this.ui_graph.cytoscape();
            cy['cxtmenu']({
                selector: 'node',
                commands: function (target) {
                    return _this.context_commands_node(target);
                }
            });
            cy['cxtmenu']({
                selector: 'edge',
                commands: function (target) {
                    return _this.context_commands_edge(target);
                }
            });
            cy['cxtmenu']({
                selector: 'core',
                commands: function () {
                    return _this.context_commands_core();
                }
            });
        };
        Main.prototype.bindSearchUI = function () {
            var _this = this;
            var search_input = this.ui_panel.search_panel.input;
            var options = {
                highlight: true,
                minLength: 1,
            };
            var dataset;
            var templates;
            templates = {
                suggestion: function (query_item) {
                    var span = $("<div>").html(query_item.label);
                    // let span = $("<div>").html(query_item.label + " <span class='vla-id'>" + query_item.id + "</span>");
                    // span.append("<div>type:" + query_item.type + "</div>");
                    if (query_item.name)
                        span.append("<div>name:" + query_item.name + "</div>");
                    if (query_item.title)
                        span.append("<div>title:" + query_item.title + "</div>");
                    return span[0].outerHTML;
                }
            };
            dataset = {
                name: 'search',
                display: "label",
                source: function (query, callback) {
                    _this.search_elements(query, function (result) {
                        var res = result.map(function (n) { return n.data(); });
                        callback(res);
                    });
                },
                templates: templates
            };
            var typeahead = search_input.typeahead(options, dataset);
            typeahead.on('typeahead:selected', function (e, entry) {
                console.info("typeahead:selected");
                console.log("e", e);
                console.log("entry", entry);
                var n = _this.ui_graph.cytoscape().getElementById(entry.id);
                n.select();
                n.flashClass("flashed", 2000);
                _this.ui_graph.cytoscape().center(n);
                _this.update_selection_info();
                return false;
            });
            this.ui_panel.on('search-submit', function (e, search_string) {
                console.info('app.search-submit', search_string);
                search_input.typeahead("close");
                // search_string
                _this.search_elements(search_string, function (result) {
                    result.select();
                    if (result.length)
                        _this.ui_graph.cytoscape().center(result);
                });
            });
        };
        Main.prototype.search_elements = function (query, callback) {
            function matches(str, q) {
                str = (str || '').toLowerCase();
                q = (q || '').toLowerCase();
                return str.match(q);
            }
            var fields = ['id', 'label', 'name', 'type', 'title'];
            function anyFieldMatches(n) {
                for (var i = 0; i < fields.length; i++) {
                    var f = fields[i];
                    if (matches(n.data(f), query)) {
                        return true;
                    }
                }
                return false;
            }
            function sortByLabel(n1, n2) {
                if (n1.data('label') < n2.data('label')) {
                    return -1;
                }
                else if (n1.data('label') > n2.data('label')) {
                    return 1;
                }
                return 0;
            }
            var result = this.ui_graph.cytoscape().nodes().stdFilter(anyFieldMatches).sort(sortByLabel);
            if (typeof callback == 'function')
                callback(result);
        };
        Main.prototype.context_commands_node = function (context_target) {
            var _this = this;
            var commands = [];
            var cy = this.ui_graph.cytoscape();
            commands.push({
                content: '<span class="fa fa-sitemap fa-2x"></span>',
                select: function (ele) {
                    _this.viewport.addClass("vla-focused");
                    _this.ui_graph.focus(ele, _this.ui_panel.focus_depth_select.value());
                }
            });
            if (this.viewport.hasClass("vla-focused")) {
                commands.push({
                    content: 'Exit focus',
                    select: function () {
                        _this.viewport.removeClass("vla-focused");
                        _this.ui_graph.clear_focus();
                    }
                });
            }
            commands.push({
                content: '<span class="fa fa-eye-slash fa-2x"></span>',
                select: function (ele) {
                    ele.hide();
                }
            });
            // commands.push({
            // 	content: 'Connect to...',
            // 		select: function(ele){
            //
            // 		alert("Not yet implemented");
            // 	}
            // });
            commands.push({
                content: '<span class="fa fa-trash fa-2x"></span>',
                select: function (ele) {
                    ele.remove();
                    console.log(ele.position());
                }
            });
            var selected_count = this.ui_graph.cytoscape().nodes(":selected").length;
            if (selected_count) {
                commands.push({
                    content: 'Hide selected',
                    select: function () {
                        cy.filter(":selected").unselect().hide();
                    }
                });
                commands.push({
                    content: 'Delete selected',
                    select: function () {
                        cy.filter(":selected").unselect().remove();
                    }
                });
                if (this.ui_graph.cytoscape().nodes(":selected").not(context_target).length) {
                    commands.push({
                        content: 'Connect',
                        select: function (node) {
                            var els = cy.filter(":selected");
                            var source = els.eq(0);
                            var target = node;
                            _this.ui_graph.connect_nodes(source, target);
                        }
                    });
                }
            }
            if (this.ui_graph.is_group(context_target) || this.ui_graph.is_inside_group(context_target)) {
                commands.push({
                    content: 'Ungroup nodes',
                    select: function (node) {
                        _this.ui_graph.ungroup_nodes(node);
                    }
                });
            }
            return commands;
        };
        Main.prototype.context_commands_edge = function (context_target) {
            var _this = this;
            var commands = [];
            commands.push({
                content: 'Hide',
                select: function (ele) {
                    ele.hide();
                }
            });
            commands.push({
                content: 'Delete',
                select: function (ele) {
                    ele.remove();
                }
            });
            commands.push({
                content: 'Flip',
                select: function (edge) {
                    var source = edge.target();
                    var target = edge.source();
                    _this.ui_graph.cytoscape().remove(edge);
                    _this.ui_graph.connect_nodes(source, target);
                }
            });
            var selected_count = this.ui_graph.cytoscape().edges(":selected").length;
            if (selected_count) {
                commands.push({
                    content: 'Hide selected',
                    select: function () {
                        _this.ui_graph.cytoscape().edges(":selected").unselect().hide();
                    }
                });
                commands.push({
                    content: 'Delete selected',
                    select: function () {
                        _this.ui_graph.cytoscape().edges(":selected").unselect().remove();
                    }
                });
            }
            return commands;
        };
        Main.prototype.context_commands_core = function () {
            var _this = this;
            var commands = [];
            commands.push({
                content: 'Add Node',
                select: function (evt, e) {
                    // TODO: generate id? show edit/create dialog?
                    var name = prompt("name");
                    var node = { data: {} };
                    node.data['raw'] = { 'name': name };
                    node.data['label'] = name;
                    node['position'] = { x: e.cyPosition.x, y: e.cyPosition.y };
                    _this.ui_graph.cytoscape().add(node);
                }
            });
            var selected_count = this.ui_graph.cytoscape().nodes(":selected").length;
            if (selected_count) {
                commands.push({
                    content: 'Hide selected',
                    select: function () {
                        _this.ui_graph.cytoscape().filter(":selected").unselect().hide();
                    }
                });
                commands.push({
                    content: 'Delete selected',
                    select: function () {
                        _this.ui_graph.cytoscape().filter(":selected").unselect().remove();
                    }
                });
                if (selected_count == 2) {
                    commands.push({
                        content: 'Connect',
                        select: function () {
                            var els = _this.ui_graph.cytoscape().filter(":selected");
                            var source = els.eq(0);
                            var target = els.eq(1);
                            _this.ui_graph.connect_nodes(source, target);
                        }
                    });
                }
                if (selected_count > 1) {
                    commands.push({
                        content: 'Group nodes',
                        select: function (node) {
                            _this.ui_graph.ungroup_nodes(node);
                        }
                    });
                }
            }
            if (this.viewport.hasClass("vla-focused")) {
                commands.push({
                    content: 'Exit focus',
                    select: function () {
                        _this.viewport.removeClass("vla-focused");
                        _this.ui_graph.clear_focus();
                    }
                });
            }
            if (this.ui_graph.cytoscape().filter(":hidden").length) {
                commands.push({
                    content: 'Restore all hidden',
                    select: function () {
                        _this.ui_graph.cytoscape().elements().show();
                    }
                });
            }
            if (commands.length == 1) {
                commands.push({
                    content: ' ',
                    disabled: true,
                    select: function () {
                    }
                });
            }
            return commands;
        };
        Main.prototype.update_selection_info = function () {
            var selected_elements = this.ui_graph.cytoscape().filter(":selected");
            this.ui_panel.selection_info_panel.update_content(selected_elements);
        };
        return Main;
    }());
    VLA.Main = Main;
})(VLA || (VLA = {}));
window['vla_inst'] = new VLA.Main();
//# sourceMappingURL=vla.js.map