/// <reference path="libs/jquery.d.ts" />
/// <reference path="libs/cytoscape.d.ts" />
/// <reference path="libs/typeahead.d.ts" />
/// <reference path="libs/spectrum.d.ts" />
/// <reference path="vla_ui_html_ui.ts"/>
/// <reference path="vla_ui_attrfilter.ts"/>
/// <reference path="libs/moment.d.ts" />

namespace VLA {
	export namespace UI {

		import Moment = moment.Moment;
		export class Panel extends HTML_UI {
			protected toggle_button: JQuery;
			
			// Analysis methods:
			public layout_form: Layouts;
			public vertex_filter_label_form: Show_Labels;
			public filter_by_type_form: Filter_by_type;
			public attr_filter_form: AttrFilter;
			public focus_depth_select: FocusPanel;
			public show_hidden_form: Show_Hidden;
			public selection_info_panel: SelectedInfoPanel;
			public search_panel: SearchPanel;
			public style_panel: NodeStylePanel;
			public edge_style_panel: EdgeStylePanel;
			public time_panel: GraphTimePanel;

			// Snapshot and Export
			public snapshot_buttons: Snapshot_buttons;

			protected bind_events() {
				this.toggle_button.on("click", () => {
					this.parent_element.toggleClass("vla-panel-on");
				});
				
				this.parent_element.delegate( ".vla-graph-link", "click", function() {
					let node_id = $(this).data('id');
					if (typeof node_id != 'undefined')
						$(this).trigger("vla.graph-link-clicked", node_id);
				});

				$(window).on("resize", () => {
					this.update_max_height();
				});
			}

			protected build_html_dom() {
				this.html_element = $('<div class="vla-panel">');
				this.update_max_height();

				//this.html_element.html('');
				this.show_hidden_form = new Show_Hidden(this.html_element);
				this.search_panel = new SearchPanel(this.html_element);
				this.attr_filter_form = new AttrFilter(this.html_element);
				this.focus_depth_select = new FocusPanel(this.html_element);
				this.style_panel = new NodeStylePanel(this.html_element);
				this.edge_style_panel = new EdgeStylePanel(this.html_element);
				//this.filter_by_type_form = new Filter_by_type(this.html_element);
				this.vertex_filter_label_form = new Show_Labels(this.html_element);
				this.layout_form = new Layouts(this.html_element);
				this.snapshot_buttons = new Snapshot_buttons(this.html_element);


				this.time_panel = new GraphTimePanel(this.parent_element, this.options);
				this.time_panel.hide();
				this.selection_info_panel = new SelectedInfoPanel(this.parent_element, this.options);


				this.toggle_button = $("<div class='vla-panel-toggler'>");
				this.toggle_button.html("<span class='fa fa-bars' aria-hidden='true'>");
				this.toggle_button.appendTo(this.parent_element);
			}

			protected update_max_height() {
				this.html_element.css({'max-height':document.documentElement.clientHeight - 10});
			}

			on_selected_nodes_change(selected_nodes: number, selected_edges: number) {
				this.html_element.removeClass("sel-n-one sel-n-two sel-e-one sel-mixed sel-any sel-n-any sel-e-any");
				if (selected_edges > 0 || selected_nodes > 0) {
					this.html_element.addClass("sel-any");
				}
				if (selected_nodes > 0) {
					this.html_element.addClass("sel-n-any");
				}
				if (selected_edges > 0) {
					this.html_element.addClass("sel-e-any");
				}

				if (selected_edges == 0 && (selected_nodes > 0 && selected_nodes < 3)) {
					// 1 or 2 nodes only
					if (selected_nodes == 1)
						this.html_element.addClass("sel-n-one");
					else
						this.html_element.addClass("sel-n-two");
				}
				else if (selected_nodes == 0) {
					if (selected_edges == 1) {
						// one edge only
						this.html_element.addClass("sel-e-one");
					}
				}
				else {
					// mixed selection
					this.html_element.addClass("sel-mixed");
				}

			}
		}

		class Layouts extends HTML_INPUT {
			status_span:JQuery;
			button_stop:JQuery;
			button_start:JQuery;

			protected bind_events() {
				this.control.on("change", () => {
					this.trigger("layout-change", this.value());
				});
			}

			public enable_layout(name, enable) {
				this.control.find(`option[value='${name}']`)[enable? "show" : "hide"]();
			}

			protected build_html_dom() {
				this.html_element = $('<div>').addClass('vla-layouts-form');
				let label_text = _("Re-Arrange");
				this.html_element.append(`<label>${label_text}:
<select>
	<option value="concentric">Concentric</option>
	<option value="breadthfirst">Breadthfirst</option>
	<option value="breadthfirst_c">Breadthfirst (circular)</option>
	<option value="cose">Cose</option>
	<option value="cose-bilkent">Cose-Bilkent</option>
	<option value="cola_inf">Cola (inf)</option>
	<option value="cola">Cola</option>
	<option value="cytoscape-ngraph.forcelayout">ngraph.forcelayout</option>
	<option style="display: none" value="map">Map</option>
</select>
</label>
<span class="vla-layouts-status"></span>
<button class="vla-layouts-start"><span class="fa fa-play"></span></button>
<button class="vla-layouts-stop"><span class="fa fa-stop"></span></button>
`);
				// '<option value="grid">Grid</option>'+
				// '<option value="spread">Spread</option>'+
				// '<option value="cose-bilkent">Cose-Bilkent</option>'+

				this.control = this.html_element.find("select");
				this.status_span = this.html_element.find(".vla-layouts-status");
				this.button_start = this.html_element.find(".vla-layouts-start");
				this.button_stop = this.html_element.find(".vla-layouts-stop");
			}
		}

		class Show_Labels extends HTML_UI {
			protected controls;
			protected bind_events() {
				let self = this;

				self.controls['labels'].on("change", function() {
					self.trigger("show-labels-change", $(this).is(":checked"));
				});
				self.controls['time'].on("change", function() {
					self.trigger("show-time-change", $(this).is(":checked"));
				});
				self.controls['map'].on("change", function() {
					self.trigger("show-map-change", $(this).is(":checked"));
				});

				$(self.html_element).find(".vla-checkbox").on("click", function () {
					let span = $(this);
					let input = $(this).find("input");
					if (span.is(".vla-disabled")) {
						return;
					}

					let checked = input.is(":checked");
					checked = !checked;

					if (checked) {
						span.addClass("vla-cb-checked");
					}
					else {
						span.removeClass("vla-cb-checked");
					}
					input.prop("checked", checked);

					input.trigger("change");
				});
			}

			public set_control_enabled(control_name, value=true, reason?) {
				console.log("set_control_enabled", control_name, value, reason);
				this.controls[control_name]
					.prop('disabled', !value);

				let span = this.controls[control_name].closest('.vla-checkbox');
				span[value? 'removeClass' : 'addClass']('vla-disabled');

				console.log("span", span);
				console.log("value==false && reason", value==false && !!reason);
				if (value==false && !!reason) {
					console.log("set title");
					span.attr('title', reason);
				} else {
					console.log("UNset title");
					span.attr('title', '');
				}
			}

			protected build_html_dom() {
				let labels_text = _("Show all labels");
				let time_panel_text = _("Show time panel");
				let map_text = _("Show map");

				this.html_element = $('<div class="vla-show-labels-form">');
				let labels_span = $(`<span class="vla-checkbox"><span class="fa fa-square vla-cb-empty"></span><span class="fa fa-check-square vla-cb-check"></span><input type="checkbox" value="1" />${labels_text}</span>`);
				let time_span = $(`<span class="vla-checkbox vla-disabled"><span class="fa fa-square vla-cb-empty"></span><span class="fa fa-check-square vla-cb-check"></span><input type="checkbox" value="1" />${time_panel_text}</span>`);
				let map_span = $(`<span class="vla-checkbox vla-disabled"><span class="fa fa-square vla-cb-empty"></span><span class="fa fa-check-square vla-cb-check"></span><input type="checkbox" value="1" />${map_text}</span>`);
				//this.html_element.html('<label><input id="cy_show_labels" type="checkbox" value="1" />Show all labels</label>');

				this.controls = {};
				this.controls['labels'] = labels_span.find("input");
				this.controls['time'] = time_span.find("input");
				this.controls['map'] = map_span.find("input");

				this.html_element
					.append(labels_span)
					.append(time_span)
					.append(map_span);
			}

			public value(control_name:string) {
				return this.controls[control_name].is(":checked");
			}
		}

		class Show_Hidden extends HTML_INPUT {

			protected bind_events() {
				this.control.on("click", ()=> {
					this.trigger("show-hidden-click");
				});
			}

			protected build_html_dom() {
				let restore_text = _("Restore all hidden");
				this.html_element = $('<div class="show-all-block on-any-hidden">');
				this.html_element.html(`<button>${restore_text}</button>`);

				this.control = this.html_element.find("button");
			}

			public value() {
			}
		}

		class Filter_by_type extends HTML_INPUT {
			protected static types = { 'person':'Person', 'company':'Company', 'website':'Website', 'location':'Location', 'company_number':'Company number', 'officer_id':'Officer ID'};
			protected controls: Object;

			protected bind_events() {
				var _this = this;
				$(this.html_element).find("input[type=checkbox]").on("change", function (evt) {
					_this.trigger("type-filter-change", {type: $(this).val(), state: $(this).is(":checked")});
				});

				$(this.html_element).find(".vla-checkbox").on("click", function () {
					let span = $(this);
					let input = $(this).find("input");
					let checked = input.is(":checked");
					checked = !checked;

					if (checked) {
						span.addClass("vla-cb-checked");
					}
					else {
						span.removeClass("vla-cb-checked");
					}
					input.prop("checked", checked);

					input.trigger("change");
				});
			}

			protected build_html_dom() {
				this.html_element = $(
					'<div class="vla-type-filter-form"></div>'
				);

				this.controls = {};
				for (let type_key in Filter_by_type.types) {
					if (!Filter_by_type.types.hasOwnProperty(type_key))
						continue;

					let control = $('<span class="vla-checkbox vla-cb-checked"><span class="fa fa-square vla-cb-empty"></span><span class="fa fa-check-square vla-cb-check"></span><input type="checkbox" value="' + type_key + '" checked />' + Filter_by_type.types[type_key] + '</span>');
					control.appendTo(this.html_element);

					this.controls[type_key] = this.html_element.find("input[value=" + type_key + "]");
				}
			}

			public value() {
				let state = {};

				for (let type_key in Filter_by_type.types) {
					if (!Filter_by_type.types.hasOwnProperty(type_key))
						continue;

					state[type_key] = this.controls[type_key].val();
				}

				return state;
			}

			public type_state(type: string): boolean {
				if (this.controls.hasOwnProperty(type))
					return this.controls[type].is(":checked");
				else
					return false;
			}
		}

		class Snapshot_buttons extends HTML_UI {
			protected select_snapshot_type:JQuery;
			protected button_snapshot_all:JQuery;
			protected button_snapshot_selected:JQuery;
			protected input_file_import:JQuery;
			protected button_import:JQuery;
			protected button_import_case:JQuery;

			protected bind_events() {
				// png
				this.button_snapshot_all.on("click", () => {
					this.trigger("snapshot-all-click", this.select_snapshot_type.val());
				});
				this.button_snapshot_selected.on("click", () => {
					this.trigger("snapshot-selected-click", this.select_snapshot_type.val());
				});


				this.button_import_case.on("click", () => {
					let caseId = prompt("Case ID");
					if (caseId)
						this.trigger("import-case-request", caseId);
				});
				this.button_import.on("click", () => {
					this.input_file_import.trigger("click");
				});
				this.input_file_import.on("change", () => {
					console.warn("file selected!");
					console.log(this.input_file_import.get(0)['files']);

					let file:File = this.input_file_import.get(0)['files'][0];

					this.trigger("import-file-request", file);
				});
			}

			protected build_html_dom() {
				let div;
				let snapshot_label_text = _("Snapshot as");
				this.html_element = $('<div class="vla-snapshot-form">');
				this.html_element.append($(`<div>${snapshot_label_text}:</div>`));

				this.select_snapshot_type = $("<select>" +
					"<option value='png'>PNG</option>"+
					"<option value='jpg'>JPG</option>"+
					"<option value='json'>JSON</option>"+
					"<option value='gexf'>GEXF</option>"+
					"</select>"
				);

				this.button_snapshot_all = $("<button>").html(_("All"));
				this.button_snapshot_selected = $("<button>").html(_("Selected")).addClass("on-any-selected");


				this.html_element.append(this.select_snapshot_type);
				this.html_element.append(this.button_snapshot_all);
				this.html_element.append(this.button_snapshot_selected);


				this.button_import = $("<button>").html(_("Import JSON"));
				this.html_element.append(this.button_import);
				this.input_file_import = $("<input type='file' />").hide();
				this.html_element.append(this.input_file_import);

				this.button_import_case = $("<button>").html(_("Load Case data"));
				this.html_element.append(this.button_import_case);
			}
		}

		class FocusPanel extends HTML_INPUT {
			protected button_exit_focus:JQuery;

			protected bind_events() {
				this.control.on("change", () => {
					this.trigger("focus-depth-change", this.value());
				});

				this.button_exit_focus.on("click", () => {
					this.trigger("graph-exit-focus");
				});
			}

			protected build_html_dom() {
				let maxDepth = 10;
				this.html_element = $('<div class="focus-panel">');
				let html = '<label for="focus_depth_select">Degree of separation:</label>' +
					'<select id="focus_depth_select">';

				for (let depth = 1; depth <= maxDepth; depth++) {
					html += '<option>' + depth + '</option>';
				}

				html += '</select>';
				this.html_element.html(html);

				this.button_exit_focus = $("<button>").html("Exit focus");
				this.button_exit_focus.appendTo(this.html_element);

				this.control = this.html_element.find("select");
			}
		}

		class SelectedInfoPanel extends HTML_UI {
			protected object_info_div:JQuery;
			protected object_info_buttons:JQuery;
			protected object_info_header:JQuery;
			protected object_info_header_label:JQuery;
			protected object_info_header_toggler:JQuery;

			protected selection_info_div:JQuery;
			protected selection_info_buttons:JQuery;

			protected node:Cy.CollectionElements;

			protected bind_events(): void {
				let self = this;

				this.object_info_header_toggler.on('click', () => {
					this.object_info_div.toggle();
					this.object_info_buttons.toggle();
					this.object_info_header_toggler.toggleClass('vla-opened');
				});

				let buttons = this.object_info_buttons.find('button');
				console.log("buttons", buttons);
				buttons.on("click", (e) => {
					let action = $(e.target).data('action');
					console.log("action!", action);
					let params = {action: action, target: this.node};
					this.trigger("graph-action", params);
				});

				buttons = this.selection_info_buttons.find('button');
				buttons.on("click", (e) => {
					let action = $(e.target).data('action');
					this.trigger("graph-action", {action: action});
				});

				this.html_element.on("keydown", ".vla-notes-block textarea", (e) => {
					if ((e.keyCode == 10 || e.keyCode == 13) && e.ctrlKey) {
						this.html_element.find(".vla-notes-block form").trigger("submit");
					}
				});

				this.html_element.on("submit", ".vla-notes-block form", function(e){
					e.preventDefault();

					let time = new Date().getTime();
					let text = $(this).find(".vla-add-text").val();

					self.trigger("add-note", {element: self.node, time: time, text: text});
				});

				this.html_element.on("click", ".vla-notes-block .vla-delete", function(e){
					e.preventDefault();

					self.trigger("delete-note", {element: self.node, index:$(this).closest(".vla-note").data("index")});
				});
			}

			protected build_html_dom(): void {
				this.html_element = $('<div>').addClass("vla-selection-panel");

				this.object_info_header = $('<div>').addClass("vla-object-info-header").appendTo(this.html_element);
				this.object_info_header_label = $('<span>').appendTo(this.object_info_header);
				this.object_info_header_toggler = $('<span class="fa fa-unsorted vla-toggler">').appendTo(this.object_info_header);

				this.object_info_div = $('<div class="vla-object-info"></div>').appendTo(this.html_element);
				this.object_info_buttons = $('<div class="vla-object-info-buttons"><button data-action="center">center</button><button data-action="fit">fit</button></div>').appendTo(this.html_element);
				this.object_info_div.hide();
				this.object_info_buttons.hide();
				
				this.selection_info_div = $('<div class="vla-selection-info"></div>').appendTo(this.html_element);
				this.selection_info_buttons = $('<div class="vla-selection-info-buttons"><button data-action="center-selected">center</button><button data-action="fit-selected">fit</button></div>').appendTo(this.html_element);
			}

			public update_content(selected_elements:Cy.CollectionElements) {

				this.update_selected_object_info(selected_elements);

				this.update_selection_info(selected_elements);

			}

			protected update_selected_object_info(selected_elements:Cy.CollectionElements)
			{
				let node = selected_elements.eq(0);
				this.node = node;
				let html = '';
				if (node.length) {

					// TODO: add option to show full or compact info
					let show_full_info_on_select = false;
					if (show_full_info_on_select)
					{
						this.object_info_div.show();
						this.object_info_buttons.show();
					}
					
					if (node.isNode()) {
						this.object_info_header_label.html(_("Node") + ": <span class='fa'>" + node.data()._icon + "</span> " + GraphLink(node.data('id'), node.data('id') + " " + node.data()._label));
						html += "<div>&lt;x,y&gt;:" + node.position("x") + ", " + node.position("y") + "</div>";
						html += this.node_info(node);
					} else if (node.isEdge()) {
						this.object_info_header_label.html(_("Edge") + ": " + GraphLink(node.data('id')));
						html += this.edge_info(node);
					} else
						console.warn("update_content: Unknown node type:", node);

					this.html_element.show();
				} else {
					this.node = null;
					this.object_info_div.html("");
					this.html_element.hide();

					//this.object_info_div.hide();
					//this.object_info_buttons.hide();
				}


				this.object_info_div.html(html);
			}

			protected update_selection_info(selected_elements:Cy.CollectionElements)
			{
				let selected_num = selected_elements.length;
				if (selected_num > 1) {
					this.selection_info_div.html(_("Total selected") + ":" + selected_num);

					this.selection_info_div.show();
					this.selection_info_buttons.show();
				} else {
					this.selection_info_div.html("");

					this.selection_info_div.hide();
					this.selection_info_buttons.hide();
				}
			}

			public node_info(node):string {
				return this.data_info(node);
			}

			public edge_info(edge):string {
				let edge_direction_type = edge.data('_dir_type');
				let html = '';

				//html += "Edge: " + edge.id() + "<br />";
				html += GraphLink(edge.source().id());
				if (edge_direction_type == 'normal')
					html += " -> ";
				else if (edge_direction_type == 'mutual')
					html += " <-> ";
				else
					html += " - ";

				html += GraphLink(edge.target().id());

				html += this.data_info(edge);

				return html;
			}

			public data_info(element:Cy.CollectionElements):string
			{
				let html = '';
				let data = element.data();

				//html += "Node: " + GraphLink(node.data('id')) + " (" + data.type + ")<br />";

				for (let key in data)
				{
					if (!data.hasOwnProperty(key))
						continue;
					if (key=='risks')
						continue;

					html += `<div><span class='vla-caption'>${key}</span>: ${ data[key] }</div>`;
				}

				// console.log("node.data",data);
				if (data.risks)
				{
					html += `<br><br><b>` + _("Risks") + `</b>:<br>
					<p>${ data.risks["description"] }</p>`;
					for (let key in data.risks)
					{
						if (!data.risks.hasOwnProperty(key))
							continue;
						if (key=='description')
							continue;

						html += `<div><span class='vla-caption'>${key}</span>: ${ data.risks[key] }</div>`;
					}
				}

				if (element.isNode()) {
					if (!element.grabbable())
						html += "<div><b>Ungrabbable</b></div>";
					if (element.locked())
						html += "<div><b>Locked</b></div>";
				}

				if (element.hasClass("_custom_styled"))
				{
					// debug info
					html += "<div><br /><b>Custom styled</b></div>";
					if (data._style) {
						html += "<div><b>_Style:</b></div>";
						for (let key in data._style) {
							if (!data._style.hasOwnProperty(key))
								continue;

							html += `<div><span class='vla-caption'>${key}</span>: ${ data._style[key] }</div>`;
						}
					}
				}

				html += this.notes_html(element);

				return html;
			}

			public notes_html(element:Cy.CollectionElements):string {
				let html = "";

				let notes = element.data("notes");
				html += "<div class='vla-notes-block'>";
				if (notes) {
					html += "<div>" + _("Annotations") + ":</div>";
					html += "<div class='vla-notes'>";
					for (let i=(notes.length-1); i>=0;i--) {
						let n = notes[i];

						html += "<div class='vla-note' data-index='" + i + "'>";

						let time_str = "";
						if (n.time) {
							let m = moment(n.time, "x");
							time_str = m.format(this.options.date_format_short + ". HH:mm");
						}

						html += "<span class='vla-note-time'>" + time_str + "</span> <span title='"  + _("Delete note") + "' class='vla-delete'>&#x2716;</span><br />";
						html += "<span class='vla-note-text'>" + n.text + "</span>";
						html += "</div>";
					}
					html += "</div>";
				}
				html += `<div><form><textarea class="vla-add-text"></textarea><button>` + _("Add note") + `</button></form></div>`;
				html += "</div>";

				return html;
			}
		}

		class SearchPanel extends HTML_UI {
			public input:JQuery;
			public form:JQuery;

			protected bind_events(): void {
				this.form.on('submit', (e) => {
					e.preventDefault();
					//console.log("search form submit");
					this.trigger('search-submit', this.input.val());
				});

				// tt-cursor
			}

			protected build_html_dom(): void {
				this.html_element = $("<div>").addClass("vla-search");

				this.form = $("<form action='' method='get'>");
				this.input = $("<input type='text'>").attr('placeholder', _('Search'));
				$("<input type='submit'>").hide().appendTo(this.form);
				if (is_rtl())
					this.input.attr("dir", "rtl");
				this.input.appendTo(this.form);

				this.html_element.append(this.form);
			}

		}

		const colorpicker_defaults = {
			allowEmpty: true,
			showInitial: true,
			showInput: true,
			preferredFormat: "hex",
			showPallet: true,
			// showPaletteOnly: true,
			// togglePaletteOnly: true,
			// togglePaletteMoreText: 'more',
			// togglePaletteLessText: 'less',
			palette: [
				["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
				["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
				["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
				["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
				["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
				["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
				["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
				["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
			]
		};

		class NodeStylePanel extends HTML_UI {
			public icon_control:JQuery;
			public color_control:JQuery;
			public border_color_control:JQuery;
			public font_control:JQuery;
			public font_size_control:JQuery;

			protected bind_events(): void {
				this.icon_control['selectric']();
				this.font_size_control['selectric']();
				this.font_control['selectric']();

				this.color_control.spectrum(colorpicker_defaults);
				this.border_color_control.spectrum(colorpicker_defaults);


				this.icon_control.on('change', () => {
					this.trigger("select-icon-change", this.icon_control.val());
				});
				this.color_control.on('change.spectrum', (e, color) => {
					//console.log("spec change!", e, color);

					this.trigger("select-color-change", color? color.toHexString() : "");
					// this.trigger("select-color-change", this.color_control.val());
				});
				this.border_color_control.on('change.spectrum', (e, color) => {
					this.trigger("select-border-color-change", color? color.toHexString() : "");
				});

				this.font_control.on('change', () => {
					this.trigger("select-font-change", this.font_control.val());
				});
				this.font_size_control.on('change', () => {
					this.trigger("select-font-size-change", this.font_size_control.val());
				});
			}

			protected build_html_dom(): void {
				this.html_element = $("<div>").addClass("vla-node-style-panel on-any-node-selected");

				this.build_icons_control();
				this.build_color_control();
				this.build_border_color_control();
				this.build_fonts_control();
				this.build_fonts_sizes_control();

				this.build_buttons();
			}

			private build_icons_control():void {
				let block = $("<div>").addClass("vla-icon-select");
				let label = $("<label>").html(_("Icon") + ":").appendTo(block);
				this.icon_control = $("<select>");
				let icons = VLA.UI.Graph.icons();
				for (let name in icons)
				{
					if (!icons.hasOwnProperty(name))
						continue;

					let option = $("<option>").attr("value", name).html(`<span class='fa fa-${ name }'></span> ` + name);

					this.icon_control.append(option);
				}
				label.append(this.icon_control);
				this.html_element.append(block);
			}

			private build_color_control():void {
				let block = $("<div>");
				let color_label = $("<label>").html(_("Color") + ":").appendTo(block);
				this.color_control = $("<input type='text' />");
				color_label.append(this.color_control);
				this.html_element.append(block);
			}

			private build_border_color_control():void {
				let block = $("<div>");
				let border_color_label = $("<label>").html(_("Border") + ":").appendTo(block);
				this.border_color_control = $("<input type='text' />");
				border_color_label.append(this.border_color_control);
				this.html_element.append(block);
			}

			private build_fonts_control():void {
				let block = $("<div>");
				let label = $("<label>").html(_("Font") + ":").appendTo(block);
				let font_families = ["Droid Sans", "Droid Serif", "Rock Salt", "Tangerine"];
				this.font_control = $("<select>");
				for (let family of font_families)
				{
					let option = $("<option>").attr("value", family).html(`${ family } <span style='font-family: ${ family }'>ABCabc</span>`);

					this.font_control.append(option);
				}

				label.append(this.font_control);
				this.html_element.append(block);
			}

			private build_fonts_sizes_control():void {
				let font_label = $("<label>").html(_("Font-size") + ":");
				let font_sizes = [9,10,11,12,14,18,24,30,36,48];
				this.font_size_control = $("<select>");
				for (let size of font_sizes)
				{
					let option = $("<option>").attr("value", size).html(size + ` <span style='font-size: ${ size }px'>ABCabc</span>`);

					this.font_size_control.append(option);
				}

				font_label.append(this.font_size_control);
				this.html_element.append(font_label);
			}

			private build_buttons() {
				let block = $("<div>");


				this.html_element.append(block);
			}
		}

		class EdgeStylePanel extends HTML_UI {
			public color_control:JQuery;
			public size_control:JQuery;
			public type_control:JQuery;
			public font_control:JQuery;
			public font_size_control:JQuery;

			protected bind_events(): void {
				this.type_control['selectric']();
				this.color_control.spectrum(colorpicker_defaults);
				this.font_size_control['selectric']();
				this.font_control['selectric']();

				this.type_control.on('change', () => {
					this.trigger("edge-type-change", this.type_control.val());
				});
				this.color_control.on('change.spectrum', (e, color) => {
					this.trigger("select-edge-color-change", color? color.toHexString() : "");
				});
				this.size_control.on('change', () => {
					this.trigger("edge-size-change", this.size_control.val());
				});

				this.font_control.on('change', () => {
					this.trigger("select-font-change", this.font_control.val());
				});
				this.font_size_control.on('change', () => {
					this.trigger("select-font-size-change", this.font_size_control.val());
				});
			}

			protected build_html_dom(): void {
				this.html_element = $("<div>").addClass("vla-edge-style-panel on-any-edge-selected");

				this.build_color_control();
				this.build_type_control();
				this.build_width_control();
				this.build_fonts_control();
				this.build_fonts_sizes_control();
			}

			private build_color_control():void {
				let block = $("<div>");
				let color_label = $("<label>").html(_("Color") + ":").appendTo(block);
				this.color_control = $("<input type='text' />");
				color_label.append(this.color_control);
				this.html_element.append(block);
			}

			private build_width_control():void {
				let block = $("<div>");
				let label = $("<label>").html(_("Width") + ":").appendTo(block);
				this.size_control = $("<select>");
				for (let i=1;i<=10;i++)
				{
					let option = $("<option>").attr("value", i).html(i.toString());

					this.size_control.append(option);
				}

				label.append(this.size_control);
				this.html_element.append(block);
			}

			private build_type_control():void {
				let block = $("<div>");
				let label = $("<label>").html(_("Type") + ":").appendTo(block);
				let types = ["none"];
				this.type_control = $("<select>");
				for (let type of types)
				{
					let option = $("<option>").attr("value", type).html(type);

					this.type_control.append(option);
				}

				label.append(this.type_control);
				this.html_element.append(block);
			}

			public set_types(types:string[]):void
			{
				console.log("types", types);
				if (types.length==0)
					types = ["none"];

				this.type_control.html("");
				for (let type of types)
				{
					let option = $("<option>").attr("value", type).html(type);
					this.type_control.append(option);
				}
				this.type_control['selectric']("refresh");
			}

			private build_fonts_control():void {
				let block = $("<div>");
				let label = $("<label>").html(_("Font") + ":").appendTo(block);
				let font_families = ["Droid Sans", "Droid Serif", "Rock Salt", "Tangerine"];
				this.font_control = $("<select>");
				for (let family of font_families)
				{
					let option = $("<option>").attr("value", family).html(`${ family } <span style='font-family: ${ family }'>ABCabc</span>`);

					this.font_control.append(option);
				}

				label.append(this.font_control);
				this.html_element.append(block);
			}

			private build_fonts_sizes_control():void {
				let font_label = $("<label>").html(_("Font-size") + ":");
				let font_sizes = [9,10,11,12,14,18,24,30,36,48];
				this.font_size_control = $("<select>");
				for (let size of font_sizes)
				{
					let option = $("<option>").attr("value", size).html(size + ` <span style='font-size: ${ size }px'>ABCabc</span>`);

					this.font_size_control.append(option);
				}

				font_label.append(this.font_size_control);
				this.html_element.append(font_label);
			}
		}

		class GraphTimePanel extends HTML_UI {
			protected input:JQuery;
			protected slider:any;
			protected _value:JSTimestamp;

			protected bind_events(): void {
				let min = +moment().subtract(12, "hours").format("x");
				let max = +moment().format("x");
				let from = +moment().subtract(10, "hours").format("x");
				let to = +moment().subtract(2, "hours").format("x");

				this.input['ionRangeSlider']({
					min: min,
					max: max,
					from: from,
					to: to,
					grid: true,
					force_edges: true,
					prettify: (num) => {
						let m = moment(num, "x");
						return m.format(this.options.date_format_short);
					},
					onChange: (data) => {
						this._value = data.from;
						this.trigger("time-range-change", this._value);
					},
					onUpdate: (data) => {
						this._value = data.from;
					}
				});
				this.slider = this.input.data('ionRangeSlider');
			}

			public update_range_span(min:Moment, max:Moment):void {
				this.slider.update({
					min: +min,
					max: +max
				});
			}
			public set_value(value:JSTimestamp):void {
				this.slider.update({
					from: value
				});
			}

			public get_value():JSTimestamp {
				return this.slider.result.from;
			}

			protected build_html_dom(): void {
				this.html_element = $("<div>").addClass("vla-time-panel").hide();

				this.input = $('<input type="text" value="" />');

				this.html_element.append(this.input);
			}

		}

		export function GraphLink(node_id:string, value?:string):string {
			let label = (typeof value!='undefined')? value : node_id;
			let link = $("<span>").addClass('vla-graph-link').html(label).attr('data-id', node_id);
			return link[0].outerHTML;
		}
	}
}