/// <reference path="libs/jquery.d.ts" />
/// <reference path="libs/selectric.d.ts" />
/// <reference path="libs/typeahead.d.ts" />
/// <reference path="libs/openlayers-3.14.2.d.ts" />
/// <reference path="vla_data.ts" />
/// <reference path="vla_ui_graph.ts" />
/// <reference path="vla_ui_panel.ts" />
/// <reference path="vla_ui.ts" />
/// <reference path="vla_utils.ts" />


'use strict';
namespace VLA {
	import JSTimestamp = VLA.UI.JSTimestamp;
	export interface DebugOptions {
		debug?: boolean;
	}
	export interface ImportOptions extends DebugOptions {
		type: "json" | "gexf" | "cytoscape" | "test_crowd",
		url?:string,
		caseId?:string, // elementexploration json option
		limit?:string, // elementexploration json option
		v?:number // target number of vertices if type is test_crowd
	}
	export interface UIGraphOptions extends DebugOptions {
		layout?:string,
		layout_options?:any,

		default_type?: string,
		default_connection_type?: string,
		types?:CustomType[],
		connection_types?:ConnectionType[],
		coords_scale?: number
	}
	type ConnectionArrowType = "tee" | "triangle" | "triangle-tee" | "triangle-backcurve" | "square" | "circle" | "diamond" | "none";
	type ConnectionDirection = "none" | "mutual" | "normal";
	interface ConnectionType {
		name:string
		color?:string
		size?:string // width
		dir?: ConnectionDirection
		targetArrow?: ConnectionArrowType
		sourceArrow?: ConnectionArrowType
	}
	// border color, fill color and size
	interface CustomType {
		name:string
		rule:string
		icon?:string
		fillColor?:string
		borderColor?:string
		size?:number
	}
	export interface InitOptions extends UIGraphOptions, DebugOptions {
		target_html_element:string,

		userId:number,
//		caseId?:number,

		locale?:string,

		autoload?:ImportOptions,
		tip_event?: "none" | "hover" | "click" | "rclick",
		filter_fields?: string[],
		date_format?: "Do MMMM, YYYY",
		date_format_short?: "Do MMM, YY",
	}

	// let dependencies_loaded:JQueryPromise<void>;
	// let dependencies:{css:string[], js:string[]} = {
	// 	css: [],
	// 	js: [
	// 		// "js-libs/cytoscape.js" => [
	// 		// 	"js-libs/cytoscape-panzoom.js",
	// 		// 	"js-libs/cytoscape-cxtmenu.js",
	// 		// 	"js-libs/cytoscape-edgehandles.js",
	// 		// 	// "js-libs/cytoscape-cose-bilkent.js",
	// 		// 	// "js-libs/cytoscape-spread.js",
	// 		// 	// "js-libs/cola.v3.min.js" => "js-libs/cytoscape-cola.js",
	// 		// ],
	// 		// "js-libs/typeahead.bundle.js",
	// 	]
	// };
	// function load_dependencies():JQueryPromise<void>
	// {
	// 	let progress = $.Deferred<void>();
	//
	// 	console.info("load_dependencies");
	//
	// 	let i = 0;
	// 	for (let file of dependencies.js)
	// 	{
	// 		i++;
	// 		//console.log("load file", i, file);
	//
	// 		$.getScript(file, () => {
	// 			console.log("file loaded", i, file);
	// 			progress.notify(file, i, dependencies.js.length);
	//
	// 			if (i == dependencies.js.length)
	// 			{
	// 				progress.resolve();
	// 			}
	// 		});
	// 	}
	//
	// 	return progress.promise();
	// }

	import ElementDefinition = Cy.ElementDefinition;
	export class Main {
		private viewport: JQuery;

		public ui_graph: UI.Graph;
		public data_server: DataServer;
		public ui_panel: UI.Panel;

		protected userId: number;
		protected caseSeedId: number;

		protected options: InitOptions;

		private double_click_started = false;
		private double_click_timeout = 500;

		constructor(params: InitOptions) {
			// console.time("app constructor");

			// dependencies_loaded = load_dependencies();
			let defaults: InitOptions = {
				target_html_element: "",
				userId: 21220,
				layout: "cose",
				tip_event: "none",

				types: [
					{
						name: "default",
						rule: "",
						// icon: "",
						fillColor: "gray",
						borderColor: "black"
					},
				],
				connection_types: [
					{
						name: "_default",
						dir: "normal",
						color: "#838383",
						targetArrow: "triangle",
						sourceArrow: "none",
					},
				],

				filter_fields: ["_label", "_type"],

				date_format: "Do MMMM, YYYY",
				date_format_short: "Do MMM, YY"
			};

			this.options = $.extend({}, defaults, params);

			if (!this.options.default_type && this.options.types)
				this.options.default_type = this.options.types[0].name;
			if (!this.options.default_connection_type && this.options.connection_types)
				this.options.default_connection_type = this.options.connection_types[0].name;

			// if (params.connection_types)
			// 	this.options.connection_types = defaults.connection_types.concat(params.connection_types);
			console.info("options", this.options);

			this._init();
		}

		protected _init() {
			console.info("init");
			console.time("app _init");

			this.viewport = $("#" + this.options.target_html_element);

			this.viewport.addClass("vla-viewport");
			// this.userId = userId;
			// this.caseSeedId = caseSeedId;
			if (this.options.locale) {
				console.info("locale", this.options.locale);
				VLA.setLocale(this.options.locale);
				if (VLA.is_rtl()) {
					this.viewport.addClass("vla-rtl");
				}
			}


			this.ui_graph = new UI.Graph(this.viewport, this.options);
			this.ui_panel = new UI.Panel(this.viewport, this.options);
			this.data_server = new DataServer(this.options.userId);

			this.ui_panel.layout_form.value(this.options.layout);
			this.ui_graph.set_layout("preset");

			console.time("ui_graph.init");
			let graph_ui_init_progress = this.ui_graph.init();
			graph_ui_init_progress.then(() => {
				console.timeEnd("ui_graph.init");
			});
			let data_autoload_progress;
			if (this.options.autoload) {
				console.time("autoload");
				data_autoload_progress = this.data_server.load_and_import(this.options.autoload);
				data_autoload_progress.then(()=> {
					console.timeEnd("autoload");
				});
			}
			else {
				data_autoload_progress = $.Deferred<void>();
				data_autoload_progress.resolve();
			}

			$.when(graph_ui_init_progress).then(() => {
				console.info('ui init done');
				this.data_server.set_graph_object(this.ui_graph.cytoscape());

				console.time("update ui");
				this.ui_graph.set_layout(this.options.layout);
				this.ui_graph.update();
				console.timeEnd("update ui");

				this.bindUI();
				this.init_types();
				this.init_connection_types();
				this.ui_graph.reassign_all_styles();

				console.timeEnd("app _init");
			});

			$.when(graph_ui_init_progress, data_autoload_progress)
				.then(() => {
						console.log("remote data autoloaded");
						// this.ui_graph.set_layout(this.ui_panel.layout_form.value());
						this.ui_graph.busy(false);

						this.ui_graph.setup_dynamic_content();
						if (this.ui_graph.is_dynamic()) {
							let dynamic_values = this.ui_graph.get_dynamic_values();
							this.ui_panel.time_panel.update_range_span(dynamic_values.start.subtract(1,'day'), dynamic_values.end.add(1,'day'));
							let time_point = this.ui_panel.time_panel.get_value();
							this.ui_graph.apply_time_span(time_point);
							//this.ui_panel.time_panel.show();
							this.ui_panel.vertex_filter_label_form.set_control_enabled('time', true);
						} else {
							//this.ui_panel.time_panel.hide();
							this.ui_panel.vertex_filter_label_form.set_control_enabled('time', false, 'Time data not available');
						}

						this.ui_graph.setup_geo_content();
						if (this.ui_graph.is_geodata_available()) {
							//this.ui_panel.map_panel.show();
							//this.ui_panel.layout_form.enable_layout('map', true);
							this.ui_panel.vertex_filter_label_form.set_control_enabled('map', true);
						} else {
							this.ui_panel.vertex_filter_label_form.set_control_enabled('map', false, 'Geodata not available');
							//this.ui_panel.map_panel.hide();
							//this.ui_panel.layout_form.enable_layout('map', false);
						}


						this.rebind_qtip(); // this should be called on any new elements

						this.apply_types();
						this.apply_connection_types();

						this.ui_graph.update();

						// should init after types are applied
						this.init_attrFilter();
					},
					(status, message) => { // failCallbacks
						this.ui_graph.busy(false);
						console.timeEnd("app _init");

						console.error("Initialisations failed:", status, message);

						if (message)
							UI.error("Initialisations failed: [" + status + "] " + message);
						else
							UI.error("Initialisations failed: " + status);
					},
					(status) => { // progressCallbacks
						// console.log("In Progress", status);
					}
				);
		}

		protected init_types(): void {
			if (typeof this.options.types == 'undefined')
				return;

			for (let i = 0; i < this.options.types.length; i++) {
				let type = this.options.types[i];
				let css: any = {};

				if (type.icon)
					css.content = UI.Graph.icon(type.icon);

				if (type.fillColor)
					css["background-color"] = type.fillColor;
				if (type.borderColor)
					css["border-color"] = type.borderColor;
				if (type.size) {
					css.width = type.size;
					css.height = type.size;
				}

				this.ui_graph.add_type_style(type.name, css);
			}
		}

		public apply_types(): void {
			if (typeof this.options.types == 'undefined')
				return;

			if (this.options.default_type)
				this.ui_graph.set_node_type(this.ui_graph.cytoscape().nodes(), this.options.default_type);
			
			for (let i = 0; i < this.options.types.length; i++) {
				let type = this.options.types[i];
				if (type.rule == "")
					continue;

				let type_rule_selector = "[" + type.rule + "]";
				let nodes = this.ui_graph.cytoscape().nodes(type_rule_selector);

				if (type.icon)
					this.ui_graph.set_node_icon(nodes, type.icon);
				this.ui_graph.set_node_type(nodes, type.name);
			}
		}

		protected init_connection_types(): void {
			if (typeof this.options.connection_types == 'undefined')
				return;

			let type_names: string[] = [];
			for (let type of this.options.connection_types) {
				type_names.push(type.name);

				let css: any = {};

				if (type.size)
					css.width = type.size;

				if (type.color) {
					css["line-color"] = type.color;
					css["source-arrow-color"] = type.color;
					css["target-arrow-color"] = type.color;
				}

				if (type.dir == 'normal') {
					let target_arrow = type["targetArrow"] ? type["targetArrow"] : "triangle";
					let source_arrow = type["sourceArrow"] ? type["sourceArrow"] : "none";

					css["curve-style"] = "bezier";
					css["target-arrow-shape"] = target_arrow;
					css["source-arrow-shape"] = source_arrow;
				} else if (type.dir == "mutual") {
					let target_arrow = type["targetArrow"] ? type["targetArrow"] : "triangle";

					css["curve-style"] = "bezier";
					css["target-arrow-shape"] = target_arrow;
					css["source-arrow-shape"] = target_arrow;
				} else {
					css["curve-style"] = "highstack";
					css["target-arrow-shape"] = "none";
					css["source-arrow-shape"] = "none";
				}

				this.ui_graph.add_connection_type_style(type.name, css);
			}

			this.ui_panel.edge_style_panel.set_types(type_names);
		}

		public apply_connection_types(): void {


			if (typeof this.options.connection_types == 'undefined')
				return;

			if (this.options.default_connection_type)
				this.ui_graph.set_edge_type(this.ui_graph.cytoscape().edges(), this.options.default_connection_type);

			for (let type of this.options.connection_types) {

				let type_rule_selector = this.ui_graph.type_selector(type.name);
				let edges = this.ui_graph.cytoscape().edges(type_rule_selector);

				this.ui_graph.set_edge_type(edges, type.name);
			}
		}

		protected on_selection_changed() {
			let selected_nodes = this.ui_graph.selected_nodes();
			let selected_edges = this.ui_graph.selected_edges();

			if (selected_edges.length == 0 && (selected_nodes.length > 0 && selected_nodes.length < 3)) {
				// 1 or 2 nodes only
			}
			else if (selected_nodes.length == 0 && selected_edges.length == 1) {
				// one edge only
			}
			else {
				// mixed selection
			}

			this.ui_panel.on_selected_nodes_change(selected_nodes.length, selected_edges.length);
			this.update_selection_info();
		}

		private bindUI() {

			this.ui_panel.on('show-labels-change', (evt, value) => {
				this.ui_graph.set_labels_visible(value);
			});
			this.ui_panel.on('show-map-change', (evt, value) => {
				this.ui_graph.map_mode(value);
			});
			this.ui_panel.on('show-time-change', (evt, value) => {
				console.log("show-time-change", value);
				if (value)
					this.ui_panel.time_panel.show();
				else
					this.ui_panel.time_panel.hide();
			});

			this.ui_panel.on('type-filter-change', (evt, data) => {
				if (data.state)
					this.ui_graph.show_nodes_by_type(data.type);
				else
					this.ui_graph.hide_nodes_by_type(data.type);
			});

			this.ui_panel.on('show-hidden-click', () => {
				this.ui_graph.cytoscape().elements().show();
			});

			this.ui_panel.on('graph-action', (e, params: {action: string, target?: Cy.CollectionElements}) => {
				let target = params.target;

				if (params.action == 'center-selected' || params.action == 'fit-selected') {
					target = this.ui_graph.cytoscape().$(":selected");
				}

				if (target && target.length) {
					if (params.action == 'center' || params.action == 'center-selected')
						this.ui_graph.cytoscape().center(target);
					else if (params.action == 'fit' || params.action == 'fit-selected')
						this.ui_graph.cytoscape().fit(target);
					return;
				}
			});

			this.ui_panel.on('time-range-change', (evt, value:JSTimestamp) => {
				let is_any_affected = this.ui_graph.apply_time_span(value);

				if (is_any_affected) {
					// TODO: is there any way to optimize this?
					this.apply_types();
					this.apply_connection_types();
					this.ui_graph.reassign_all_styles();
				}
			});

			this.ui_panel.on('graph-show-map', () => {
				this.ui_graph.map_mode(true);
			});
			this.ui_panel.on('graph-exit-map', () => {
				this.ui_graph.map_mode(false);
			});


			this.bind_focusUI();
			this.bind_importExportUI();
			this.bind_searchUI();
			this.bind_layoutsUI();
			this.bind_stylesUI();
			this.bind_contextMenu();
			this.rebind_qtip();

			this.ui_graph.cytoscape().on('click', 'node', (event) => {
				if (this.double_click_started) {
					// do on double click
					this.ui_graph.focus(event.cyTarget);
				}

				this.double_click_started = true;
				window.setTimeout( () => {
					this.double_click_started = false;
				}, this.double_click_timeout);
			});
			this.ui_graph.cytoscape().on('select', () => {
				this.on_selection_changed();
			});
			this.ui_graph.cytoscape().on('unselect', () => {
				this.on_selection_changed();
			});

			this.viewport.on("vla.graph-link-clicked", (e, node_id) => {
				let node = this.ui_graph.cytoscape().filter("[id='" + node_id + "']");
				if (node.length) {
					this.ui_graph.cytoscape().zoom(1);
					this.ui_graph.cytoscape().stop().center(node);
				}
			});

			this.ui_panel.on('add-note', (evt, data) => {
				if (typeof data.element.data().notes == "undefined") {
					data.element.data("notes", []);
				}
				data.element.data().notes.push({time:data.time, text:data.text});
				// redraw selection info to update annotations
				this.update_selection_info();
			});
			this.ui_panel.on('delete-note', (evt, data) => {
				if (typeof data.element.data().notes == "undefined") {
					return;
				}
				data.element.data().notes.splice(data.index, 1);
				// redraw selection info to update annotations
				this.update_selection_info();
			});
		}

		protected bind_focusUI() {
			this.ui_panel.on('graph-exit-focus', () => {
				this.viewport.removeClass("vla-focused");
				this.ui_graph.clear_focus();
			});

			this.ui_panel.on('focus-depth-change', (evt, depth: number) => {
				if (this.viewport.hasClass("vla-focused"))
					this.ui_graph.focus_depth(depth);
			});
		}

		protected bind_importExportUI() {
			this.ui_panel.on("import-case-request", (e, caseId: string) => {
				let options: ImportOptions = {
					type: "json",
					caseId: caseId,
					limit: "1000"
				};

				this.ui_graph.cytoscape().elements().remove();
				this.ui_graph.set_layout("preset");
				this.data_server.load_and_import_elementexploration_json(options)
					.then(() => {
						this.apply_types();
						this.apply_connection_types();
						this.apply_selected_attrFilters();

						this.ui_graph.set_layout(this.ui_panel.layout_form.value());
						this.ui_graph.update();
					});
			});
			this.ui_panel.on("import-file-request", (e, file: File) => {
				let reader = new FileReader();
				let import_progress = $.Deferred<void>();
				reader.onload = (e) => {
					console.log("reader.onload e", e);

					let contents = e.target['result'];
					let json_data = JSON.parse(contents);

					console.log("json_data", json_data);

					this.ui_graph.cytoscape().elements().remove();
					this.ui_graph.set_layout("preset");
					this.data_server.import_elementexploration_json(json_data, import_progress);
				};
				reader.readAsText(file);

				import_progress.then(() => {
					this.apply_types();
					this.apply_connection_types();
					this.apply_selected_attrFilters();

					this.ui_graph.set_layout(this.ui_panel.layout_form.value());
					this.ui_graph.update();
				});
			});

			this.ui_panel.on('snapshot-all-click', (e, snapshot_type) => {
				// TODO: refactor export/import
				let filename = 'graph_' + Utils.time_string(new Date(), "", "", "_") + "." + snapshot_type;
				switch (snapshot_type) {
					case 'json':
						let json_data = this.ui_graph.snapshot_json_all();
						this.data_server.upload_document_data(filename, JSON.stringify(json_data));
						break;
					case 'gexf':
						let gexf_data = this.ui_graph.snapshot_gexf_all();
						this.data_server.upload_document_data(filename, gexf_data);
						break;
					case 'jpg':
					case 'png':
						let img64 = this.ui_graph.snapshot_image(snapshot_type);
						this.data_server.upload_document_data(filename, img64);
						break;
					default:
						console.error("Unsupported export format:", snapshot_type);
				}
			});

			this.ui_panel.on('snapshot-selected-click', (e, snapshot_type) => {
				let filename = 'graph_part_' + Utils.time_string(new Date(), "", "", "_") + "." + snapshot_type;
				switch (snapshot_type) {
					case 'json':
						let json_data = this.ui_graph.snapshot_json_selected();
						this.data_server.upload_document_data(filename, JSON.stringify(json_data));
						break;
					case 'gexf':
						let gexf_data = this.ui_graph.snapshot_gexf_selected();
						this.data_server.upload_document_data(filename, gexf_data);
						break;
					case 'jpg':
					case 'png':
						let img64 = this.ui_graph.snapshot_image(snapshot_type, true);
						this.data_server.upload_document_data(filename, img64);
						break;
					default:
						console.error("Unsupported export format:", snapshot_type);
				}
			});
		}

		protected bind_layoutsUI() {
			this.ui_panel.on('layout-change', (evt, layout_name: string) => {
				this.ui_graph.set_layout(layout_name);
			});

			this.viewport.on("layout-ready", () => {
				console.log("on layout-ready");
				this.ui_panel.layout_form.button_start.hide();
				this.ui_panel.layout_form.button_stop.show();
			});
			this.viewport.on("layout-stop", () => {
				console.log("on layout-stop");
				this.ui_panel.layout_form.button_start.show();
				this.ui_panel.layout_form.button_stop.hide();
			});

			this.ui_panel.layout_form.button_start.on("click", () => {
				this.ui_graph.update();
			});
			this.ui_panel.layout_form.button_stop.on("click", () => {
				this.ui_graph.stop_layout();
			});
		}

		protected bind_stylesUI() {
			this.ui_graph.cytoscape().on("select", (e) => {
				this.update_style_controls(e.cyTarget);
			});

			this.ui_panel.on("select-icon-change", (e, value) => {
				this.ui_graph.set_node_icon(this.ui_graph.selected_nodes(), value);
			});
			this.ui_panel.on("select-color-change", (e, value) => {
				this.ui_graph.set_element_style(this.ui_graph.selected_nodes(), {
					// nodes
					"background-color": value,
				});
			});
			this.ui_panel.on("select-edge-color-change", (e, value) => {
				this.ui_graph.set_element_style(this.ui_graph.selected_edges(), {
					// edges
					"line-color": value,
					"target-arrow-color": value,
					"source-arrow-color": value
				});
			});
			this.ui_panel.on("edge-type-change", (e, value) => {
				this.ui_graph.set_edge_type(this.ui_graph.selected_elements(), value);
			});
			this.ui_panel.on("select-font-change", (e, value) => {
				this.ui_graph.set_element_style(this.ui_graph.selected_elements(), {
					"font-family": value,
				});
			});
			this.ui_panel.on("select-font-size-change", (e, value) => {
				this.ui_graph.set_element_style(this.ui_graph.selected_elements(), {
					"font-size": value,
				});
			});
			this.ui_panel.on("select-border-color-change", (e, value) => {
				this.ui_graph.set_element_style(this.ui_graph.selected_nodes(), {
					"border-color": value,
				});
			});
			this.ui_panel.on("edge-size-change", (e, value) => {
				this.ui_graph.set_element_style(this.ui_graph.selected_edges(), {
					"width": value + 'px',
				});
			});
		}

		protected update_style_controls(selected_element:Cy.CollectionElements)
		{
			let custom_style = this.ui_graph.get_custom_style(selected_element);
			let is_custom_styled = this.ui_graph.is_custom_styled(selected_element);

			if (selected_element.isNode()) {
				let icon_name = VLA.UI.Graph.icon_name(selected_element.data("_icon"));
				let bg_color = '';
				let border_color = '';
				let font_family = '';
				let font_size = '';
				if ((is_custom_styled || selected_element.isParent()) && custom_style) {
					bg_color = custom_style["background-color"];
					border_color = custom_style["border-color"];
					font_family = custom_style["font-family"];
					font_size = custom_style["font-size"];
				}

				this.ui_panel.style_panel.font_control.val(font_family).selectric('refresh');
				this.ui_panel.style_panel.font_size_control.val(font_size).selectric('refresh');
				this.ui_panel.style_panel.icon_control.val(icon_name).selectric('refresh');
				this.ui_panel.style_panel.color_control.spectrum("set", bg_color);
				this.ui_panel.style_panel.border_color_control.spectrum("set", border_color);
			}

			if (selected_element.isEdge()) {
				let line_color = '';
				let line_width = 1;
				let font_family = '';
				let font_size = '';
				if (is_custom_styled && custom_style) {
					line_color = custom_style["line-color"];
					line_width = custom_style["width"];
					font_family = custom_style["font-family"];
					font_size = custom_style["font-size"];
				}
				let edge_type = this.ui_graph.get_edge_type(selected_element);

				this.ui_panel.edge_style_panel.color_control.spectrum("set", line_color);
				this.ui_panel.edge_style_panel.type_control.val(edge_type).selectric("refresh");
				this.ui_panel.edge_style_panel.size_control.val(line_width);
				this.ui_panel.edge_style_panel.font_control.val(font_family).selectric('refresh');
				this.ui_panel.edge_style_panel.font_size_control.val(font_size).selectric('refresh');
			}
		}

		protected bind_contextMenu() {
			let cy = this.ui_graph.cytoscape();
			cy['cxtmenu']({
				selector: 'node',
				commands: (target) => {
					return this.context_commands_node(target);
				}
			});
			cy['cxtmenu']({
				selector: 'edge',
				commands: (target) => {
					return this.context_commands_edge(target);
				}
			});
			cy['cxtmenu']({
				selector: 'core',
				commands: () => {
					return this.context_commands_core();
				}
			});
		}

		protected rebind_qtip() {
			if (this.options.tip_event == 'none')
				return;

			let self = this;
			let qtip_options: any = {
				content: function () {
					let node = this;
					if (node.isEdge())
						return self.ui_panel.selection_info_panel.edge_info(node);
					else
						return self.ui_panel.selection_info_panel.node_info(node);
				},
				position: {
					my: 'top center',
					at: 'bottom center',
					adjust: {method: "none"}
				},
				style: {
					classes: 'qtip-bootstrap',
					tip: {
						width: 16,
						height: 8
					}
				}
			};
			if (this.options.tip_event == 'hover') {
				qtip_options.show = {event: "mouseover"};
				qtip_options.hide = {event: "mouseout"};
			} else if (this.options.tip_event == 'rclick') {
				qtip_options.show = {event: "cxttap"};
				//qtip_options.hide = {event: "mouseout"};
			} else {
				qtip_options.show = {event: "click"};
			}

			this.ui_graph.cytoscape().elements()['qtip'](qtip_options);
		}

		protected bind_searchUI() {
			if (!this.ui_panel.search_panel)
				return;

			let search_input = this.ui_panel.search_panel.input;
			let options: Twitter.Typeahead.Options = {
				highlight: true,
				minLength: 1,
			};
			let dataset: Twitter.Typeahead.Dataset<any>;
			let templates: Twitter.Typeahead.Templates<any>;
			templates = {
				suggestion: (query_item: any): string => {
					let span = $("<div>").html(query_item._label);
					// let span = $("<div>").html(query_item.label + " <span class='vla-id'>" + query_item.id + "</span>");
					// span.append("<div>type:" + query_item.type + "</div>");

					if (query_item.name)
						span.append("<div>name:" + query_item.name + "</div>");
					if (query_item.title)
						span.append("<div>title:" + query_item.title + "</div>");
					return span[0].outerHTML;
				}
			};
			dataset = {
				name: 'search',
				display: "_label",
				source: (query, callback) => {

					this.search_elements(query, (result) => {
						let res = result.map((n) => {
							return n.data();
						});
						callback(res);
					});

				},
				templates: templates
			};
			let typeahead = search_input.typeahead<any>(options, dataset);

			typeahead.on('typeahead:selected', (e, entry) => {
				// console.info("typeahead:selected");
				// console.log("e", e);
				// console.log("entry", entry);
				this.ui_graph.cytoscape().elements(":selected").unselect();

				let n = this.ui_graph.cytoscape().getElementById(entry.id);
				n.select();
				// n.flashClass("flashed",2000);
				this.ui_graph.cytoscape().center(n);
				this.update_selection_info();

				return false;
			});


			this.ui_panel.on('search-submit', (e, search_string: string) => {
				console.info('app.search-submit', search_string);

				search_input.typeahead("close");

				// search_string
				this.ui_graph.cytoscape().elements(":selected").unselect();
				if (search_string) {
					this.search_elements(search_string, (result) => {
						result.select();
						if (result.length)
							this.ui_graph.cytoscape().center(result);
					});
				}
			});
		}

		search_elements(query: string, callback) {
			function matches(str, q) {
				str = (str || '').toLowerCase();
				q = (q || '').toLowerCase();

				return str.match(q);
			}

			let fields = ['id', '_label', 'name', 'type', 'title'];

			function anyFieldMatches(n) {
				for (let i = 0; i < fields.length; i++) {
					let f = fields[i];

					if (matches(n.data(f), query)) {
						return true;
					}
				}

				return false;
			}

			function sortByLabel(n1, n2) {
				if (n1.data('_label') < n2.data('_label')) {
					return -1;
				} else if (n1.data('_label') > n2.data('_label')) {
					return 1;
				}

				return 0;
			}

			let result = this.ui_graph.cytoscape().nodes().stdFilter(anyFieldMatches).sort(sortByLabel);

			if (typeof callback == 'function')
				callback(result);
		}

		public context_commands_node(context_target): VLA.UI.graph_context_command[] {
			let commands = this.context_commands_common_targeted(context_target);

			commands.push({
				content: '<span title="Focus" class="fa fa-sitemap fa-2x"></span>',
				select: (ele) => {
					this.ui_graph.focus(ele, this.ui_panel.focus_depth_select.value());
				}
			});

			if (this.ui_graph.is_group(context_target) || this.ui_graph.is_inside_group(context_target)) {
				commands.push({
					content: '<span title="Ungroup" class="fa fa-object-ungroup fa-2x"></span>',
					select: (node) => {
						this.ui_graph.ungroup_nodes(node);
					}
				});
			}

			//commands = commands.concat(this.context_commands_selected());
			return commands;
		}

		public context_commands_edge(context_target): VLA.UI.graph_context_command[] {
			let commands = this.context_commands_common_targeted(context_target);

			commands.push({
				content: 'Reverse',
				select: (edge) => {

					let source = edge.target();
					let target = edge.source();

					this.ui_graph.cytoscape().remove(edge);
					this.ui_graph.connect_nodes(source, target);
				}
			});

			//commands = commands.concat(this.context_commands_selected());
			return commands;
		}

		private context_commands_common_targeted(context_target): VLA.UI.graph_context_command[] {
			let commands:VLA.UI.graph_context_command[] = [];

			commands.push({
				content: '<span title="Edit" class="fa fa-pencil fa-2x"></span>',
				select: (ele) => {
					let label = window.prompt("Label", ele.data("_label"));
					if (label) {
						this.ui_graph.actions.do("rename", {target: ele, label: label});
					}
				}
			});
			commands.push({
				content: '<span title="Hide" class="fa fa-eye-slash fa-2x"></span>',
				select: (ele) => {
					this.ui_graph.hide_elements(ele);
				}
			});
			commands.push({
				content: '<span title="Delete" class="fa fa-trash fa-2x"></span>',
				fillColor: 'rgba(200, 0, 0, 0.75)',
				activeFillColor: 'rgba(200, 0, 0, 0.75)',
				select: (ele) => {
					let confirm_message = 'Delete element?';
					if (ele.isNode())
						confirm_message = `Delete node '${ele.data('_label')}'?`;
					else if (ele.isEdge())
					{
						if (ele.data('_label'))
							confirm_message = `Delete connection '${ele.data('_label')}'?`;
						else
							confirm_message = 'Delete connection?';
					}

					if (!UI.confirm(confirm_message))
						return;

					this.ui_graph.remove(ele);
				}
			});

			if (this.ui_graph.cytoscape().nodes(":selected").not(context_target).length) {

				if (context_target.isNode()) {
					commands.push({
						content: '<span title="Group" class="fa fa-object-group fa-2x"></span>',
						select: (node) => {
							let nodes = this.ui_graph.selected_nodes().add(node);
							this.ui_graph.group_nodes(nodes);
						}
					});

					commands.push({
						content: '<span title="Connect" class="fa fa-link fa-2x"></span>',
						select: (node) => {
							let source = this.ui_graph.selected_nodes().not(node);
							let target = node;

							this.ui_graph.connect_nodes(source, target);
							// this.ui_graph.update();
						}
					});

					commands.push({
						content: 'Find route',
						select: (node) => {
							let source = this.ui_graph.selected_nodes().not(node);
							let target = node;

							let path = this.ui_graph.find_shortest_path(source, target);
							// this.ui_graph.update();
						}
					});
				}
			}

			return commands;
		}
		private context_commands_selected(): VLA.UI.graph_context_command[] {
			let commands:VLA.UI.graph_context_command[] = [];

			let selected_nodes_count = this.ui_graph.cytoscape().nodes(":selected").length;
			if (selected_nodes_count) {
				commands.push({
					content: '<span class="fa fa-list-ul fa-2x"></span><span title="Hide" class="fa fa-eye-slash fa-2x"></span>',
					select: () => {
						this.ui_graph.hide_elements(this.ui_graph.cytoscape().filter(":selected"));
					}
				});
				commands.push({
					content: '<span class="fa fa-list-ul fa-2x"></span><span title="Delete selected" class="fa fa-trash fa-2x"></span>',
					fillColor: 'rgba(200, 0, 0, 0.75)',
					activeFillColor: 'rgba(200, 0, 0, 0.75)',
					select: () => {
						if (!UI.confirm("Delete all selected elements?"))
							return;

						this.ui_graph.remove(this.ui_graph.cytoscape().filter(":selected"));
					}
				});

				if (selected_nodes_count > 1) {
					commands.push({
						content: '<span title="Group" class="fa fa-object-group fa-2x"></span>',
						select: () => {
							this.ui_graph.group_nodes(this.ui_graph.cytoscape().nodes(":selected"));
						}
					});
				}

			}
			if (this.ui_graph.in_focus()) {
				commands.push({
					content: '<span title="Exit focus" class="fa fa-sign-out fa-2x"></span>',
					select: () => {
						this.ui_graph.clear_focus();
					}
				});
			}
			if (this.ui_graph.cytoscape().filter(":hidden").length) {
				commands.push({
					content: '<span title="Restore hidden" class="fa fa-eye fa-2x"></span>',
					select: () => {
						this.ui_graph.cytoscape().elements(":hidden").show();
					}
				});
			}

			return commands;
		}

		public context_commands_core(): VLA.UI.graph_context_command[] {
			let commands = [];

			commands.push({
				content: '<span title="Add Node" class="fa fa-plus-circle fa-2x"></span>',
				select: (evt, e) => {

					// TODO: generate id? show edit/create dialog?
					let name = UI.prompt("name");

					let node: Cy.ElementDefinition = {data: {}};
					node.data['name'] = name;
					node.data['_label'] = name;
					node['position'] = {x: e.cyPosition.x, y: e.cyPosition.y};

					this.ui_graph.cytoscape().add(node);
					// this.ui_graph.update();
				}
			});

			if (!this.ui_graph.actions.isUndoStackEmpty()) {
				let stack = this.ui_graph.actions.getUndoStack();
				commands.push({
					content: 'Undo ' + stack[0].name,
					select: (evt, e) => {
						this.ui_graph.actions.undo();
					}
				});
			}
			if (!this.ui_graph.actions.isRedoStackEmpty()) {
				let stack = this.ui_graph.actions.getRedoStack();
				commands.push({
					content: 'Redo ' + stack[0].name,
					select: (evt, e) => {
						this.ui_graph.actions.redo();
					}
				});
			}

			let selected_nodes_count = this.ui_graph.selected_nodes().length;
			if (selected_nodes_count) {
				if (selected_nodes_count == 2) {
					commands.push({
						content: '<span title="Connect" class="fa fa-link fa-2x"></span>',
						select: () => {
							let els = this.ui_graph.selected_nodes();
							let source = els.eq(0);
							let target = els.eq(1);

							this.ui_graph.connect_nodes(source, target);
							// this.ui_graph.update();
						}
					});
				}
			}

			commands = commands.concat(this.context_commands_selected());

			commands.push({
				content: 'Cen-B',
				select: () => {
					this.ui_graph.mark_betweennessCentrality(this.ui_graph.cytoscape().elements());
				}
			});
			commands.push({
				content: 'Cen-Deg',
				select: () => {
					this.ui_graph.mark_degreeCentrality(this.ui_graph.cytoscape().elements());
				}
			});
			commands.push({
				content: 'Cen-Cl',
				select: () => {
					this.ui_graph.mark_closenessCentrality(this.ui_graph.cytoscape().elements());
				}
			});
			commands.push({
				content: 'Cen-CLEAR',
				select: () => {
					this.ui_graph.unmark_centrality();
				}
			});
			commands.push({
				content: 'Cluster',
				select: () => {
					this.ui_graph.clustering();
				}
			});

			if (commands.length == 1) {
				commands.push({
					content: ' ',
					disabled: true,
					select: () => {
					}
				});
			}
			return commands;
		}

		private update_selection_info() {
			let selected_elements = this.ui_graph.cytoscape().filter(":selected");
			this.ui_panel.selection_info_panel.update_content(selected_elements);
		}


		private selected_filters: {a: string,q: string}[] = []; // [attr, query]
		private cached_attributes_values;

		private init_attrFilter(): void {
			if (!this.options.filter_fields || !this.options.filter_fields.length)
			{
				this.ui_panel.attr_filter_form.hide();
				return;
			}

			this.ui_panel.attr_filter_form.set_attributes_list(this.options.filter_fields);

			if (typeof this.cached_attributes_values === 'undefined') {
				this.cached_attributes_values = {};
				this.cache_attrFiltered_fields();
			}

			let search_input = this.ui_panel.attr_filter_form.filter_control;
			let options: Twitter.Typeahead.Options = {
				highlight: true,
				minLength: 1,
			};
			let dataset: Twitter.Typeahead.Dataset<string>;
			dataset = {
				name: 'search',
				// display: "_label",
				source: (query, callback) => {

					// this.search_elements(query, (result) => {
					// 	var res = result.map((n) => { return n.data(); });
					// 	callback(res);
					// });

					let attr = this.ui_panel.attr_filter_form.attr_select.val();

					if (!attr)
						callback([]);

					// filter by all other filters here
					// var filtered_nodes = this.ui_graph.cytoscape().nodes().stdFilter( this._attrFilter );
					// var res = filtered_nodes.map((n) => { return n.data(attr); });

					let res = [];

					for (let value in this.cached_attributes_values[attr]) {
						if (!this.cached_attributes_values[attr].hasOwnProperty(value))
							continue;

						if (Utils.matches(value, query) && this.get_attrFilter_index(attr, value)==-1)
							res.push(value);
					}

					callback(res);
				}
			};
			let typeahead = search_input.typeahead<any>(options, dataset);

			typeahead.on('typeahead:selected', (e, search_string) => {
				// console.info("typeahead:selected");
				// console.log("e", e);
				let attr = this.ui_panel.attr_filter_form.attr_select.val();

				typeahead.typeahead('val', '');

				this.add_attrFilter(attr, search_string);
			});

			this.ui_panel.on('attrfilter-submit', (e, search_string: string) => {
				let attr = this.ui_panel.attr_filter_form.attr_select.val();

				typeahead.typeahead("close");
				typeahead.typeahead('val', '');

				if (search_string)
					this.add_attrFilter(attr, search_string);
			});

			this.ui_panel.on('attrfilter-remove', (e, filter_index) => {
				// console.log("attrfilter-remove!", filter_index);
				this.remove_attrFilter_by_index(filter_index);
			});
		}

		public add_attrFilter(attr: string, search_string: string) {
			this.selected_filters.push({a: attr, q: search_string});

			this.redraw_selected_attrFilters();
			this.apply_selected_attrFilters();
		}

		public get_attrFilter_index(attr: string, search_string: string):number
		{
			if (typeof this.selected_filters === 'undefined' || !this.selected_filters.length)
				return -1;

			for (let i=0; i<this.selected_filters.length; i++)
			{
				let pair = this.selected_filters[i];
				if (!pair)
					continue;

				if (pair.a == attr && pair.q == search_string)
					return i;
			}

			return -1;
		}

		// public remove_attrFilter(attr:string, search_string:string) {
		// 	let index = this.get_attrFilter_index(attr, search_string);
		// 	this.remove_attrFilter_by_index(index);
		// }

		public remove_attrFilter_by_index(index:number) {
			this.selected_filters.splice(index,1);

			this.redraw_selected_attrFilters();
			this.apply_selected_attrFilters();
		}

		// TODO: optimise this!!!
		private cache_attrFiltered_fields():void {

			for (let attr of this.options.filter_fields)
				this.cached_attributes_values[attr] = {};

			// console.log("nodes.length", this.ui_graph.cytoscape().nodes().length);
			// console.time("init: cache attr values");
			let nodes;
			// if (this.selected_filters.length)
			// 	nodes = this.ui_graph.cytoscape().nodes(".af-highlight"); // filtered
			// else
				nodes = this.ui_graph.cytoscape().nodes(); // all

			nodes.forEach((node) => {
				for (let attr of this.options.filter_fields)
				{
					if (attr == 'degree')
						continue;

					let value = node.data(attr);
					if (value)
						this.cached_attributes_values[attr][value] = 1;
				}
			});
			console.timeEnd("init: cache attr values");
		}

		private redraw_selected_attrFilters():void {

			this.ui_panel.attr_filter_form.redraw_selected_attrFilters(this.selected_filters);

		}

		apply_selected_attrFilters():void {
			this.ui_graph.cytoscape().nodes().removeClass("af-highlight");
			
			if (typeof this.selected_filters === 'undefined' || this.selected_filters.length==0) {
				return;
			}
			
			let filtered_nodes = this.ui_graph.cytoscape().nodes().stdFilter( (n) => { return this._attrFilter(n); } );
			filtered_nodes.addClass("af-highlight");

			// this.cache_attrFiltered_fields();
		}

		public _attrFilter(node:Cy.CollectionNodes):boolean {
			if (typeof this.selected_filters === 'undefined' || !this.selected_filters.length)
				return false;

			let match_all = true;
			for (let pair of this.selected_filters)
			{
				if (!pair)
					continue;

				if (pair.a == 'degree')
				{
					let [,operation,value] = (pair.q).match("^\s*(<=|>=|=|>|<)?\s*(-?[0-9]+(\.[0-9]+)?)?\s*$");
					let rule = pair.q;
					if (!operation)
						rule = "=" + rule;
					match_all = match_all && node.is("[[degree" + rule + "]]");
				} else {
					let attr_value = node.data(pair.a);

					match_all = match_all && !!Utils.matches(attr_value, pair.q);
					// if (!Utils.matches(attr_value, pair.q))
					// 	return false;
				}
			}

			return match_all;
		}

		// search_elements(query:string, callback)
		// {
		// 	function matches( str, q ){
		// 		str = (str || '').toLowerCase();
		// 		q = (q || '').toLowerCase();
		//
		// 		return str.match( q );
		// 	}
		//
		// 	var fields = ['id', '_label', 'name', 'type', 'title'];
		//
		// 	function anyFieldMatches( n ){
		// 		for( var i = 0; i < fields.length; i++ ){
		// 			var f = fields[i];
		//
		// 			if( matches( n.data(f), query ) ){
		// 				return true;
		// 			}
		// 		}
		//
		// 		return false;
		// 	}
		//
		// 	function sortByLabel(n1, n2){
		// 		if( n1.data('_label') < n2.data('_label') ){
		// 			return -1;
		// 		} else if( n1.data('_label') > n2.data('_label') ){
		// 			return 1;
		// 		}
		//
		// 		return 0;
		// 	}
		//
		// 	let result = this.ui_graph.cytoscape().nodes().stdFilter( anyFieldMatches ).sort( sortByLabel );
		//
		// 	if (typeof callback == 'function')
		// 		callback( result );
		// }
	}
}