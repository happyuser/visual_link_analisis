namespace VLA {
	export namespace Utils {
		function escapeRegExp(str:string):string {
			return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
		}
		export function matches( str:string, q:string ){
			str = (str || '').toLowerCase();
			q = (q || '').toLowerCase();

			q = escapeRegExp(q);

			return str.match( q );
		}

		export function zero_padded(num: number | string, size: number = 2): string {
			let s = String(num);
			if (s.length < size) {
				let add_zeroes = size - s.length;
				let zeroes = "";
				for (let i = 0; i < add_zeroes; i++)
					zeroes += "0";

				s = zeroes + s;
			}
			return s;
		}

		export function time_string(date = new Date(), date_separator = "-", time_separator = ':', joint = ' '): string {
			let date_part = [date.getFullYear(), (date.getMonth() + 1), date.getDay()].map((value) => {
				return zero_padded(value, 2);
			}).join(date_separator);
			let time_part = [ date.getHours(), date.getMinutes(), date.getSeconds() ].map((value) => {
				return zero_padded(value, 2);
			}).join(time_separator);

			return date_part + joint + time_part;
		}
	}
}
