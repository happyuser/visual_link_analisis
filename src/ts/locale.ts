/**
 * @Чулан https://habrahabr.ru/sandbox/47210/
 */
/*
 //попробуем получить строку которой нету в словаре
 _('test'); //test
 _('two words'); //two words
 //регистрируем словарь
 _.registerLocale('ru', {
 'test': 'Тест',
 'two words': 'Два слова',
 });
 //попробуем снова
 _('test'); //Тест
 _('two words'); //Два слова
 */
namespace VLA {
	const locale_debug = true; // show missing translations in console as error
	let _data:any = {};
	let _defaultLocale:string = 'en_US';

	export function is_rtl(locale?:string):boolean {
		locale = locale || _defaultLocale;
		if (_data.hasOwnProperty(locale) && typeof _data[locale] == 'object') {
			return _data[locale].rtl;
		} else if (locale_debug && locale !='en_US') {
			console.error("Locale is not registered:", locale);
		}
		return false;
	}

	export function setLocale(locale:string) {
		_defaultLocale = locale;
	}

	export function _(str: string, locale?: string): string {
		locale = locale || _defaultLocale;
		if (_data.hasOwnProperty(locale) && typeof _data[locale] == 'object') {
			if (_data[locale].text.hasOwnProperty(str)) {
				return _data[locale].text[str];
			} else if (locale_debug && locale != 'en_US') {
				console.error("[" + locale + "]Translation is missing for", str);
			}
		} else if (locale_debug && locale != 'en_US') {
			console.error("Locale is not registered:", locale);
		}
		return str;
	}

	export function _registerLocale(locale:string, data:any, rtl = false):void {
		if (!_data.hasOwnProperty(locale)) {
			_data[locale] = {rtl:rtl, text:{} };
		}
		for (let str in data) {
			if (data.hasOwnProperty(str)) {
				_data[locale].text[str] = data[str];
			}
		}
	}
}