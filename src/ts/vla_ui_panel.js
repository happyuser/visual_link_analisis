/// <reference path="libs/jquery.d.ts" />
/// <reference path="libs/cytoscape.d.ts" />
/// <reference path="libs/typeahead.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var VLA;
(function (VLA) {
    var UI;
    (function (UI) {
        var HTML_UI = (function () {
            function HTML_UI(target_div) {
                this.parent_element = target_div;
                this.build_html_dom();
                this.bind_events();
                this.html_element.appendTo(this.parent_element);
            }
            HTML_UI.prototype.on = function (event_name, callback) {
                // console.log("bind.on[" + "vla."+event_name + "]", callback);
                this.parent_element.on("vla." + event_name, callback);
            };
            HTML_UI.prototype.trigger = function (event_name, data) {
                // console.log("trigger[" + "vla."+event_name + "] on ", this.html_element);
                $(this.html_element).trigger("vla." + event_name, data);
            };
            HTML_UI.prototype.addClass = function (class_name) {
                this.html_element.addClass(class_name);
            };
            HTML_UI.prototype.removeClass = function (class_name) {
                this.html_element.removeClass(class_name);
            };
            HTML_UI.prototype.hasClass = function (class_name) {
                this.html_element.hasClass(class_name);
            };
            return HTML_UI;
        }());
        var HTML_INPUT = (function (_super) {
            __extends(HTML_INPUT, _super);
            function HTML_INPUT() {
                _super.apply(this, arguments);
            }
            HTML_INPUT.prototype.value = function () {
                if (this.control)
                    return this.control.val();
            };
            return HTML_INPUT;
        }(HTML_UI));
        var Panel = (function (_super) {
            __extends(Panel, _super);
            function Panel() {
                _super.apply(this, arguments);
            }
            Panel.prototype.bind_events = function () {
                var _this = this;
                this.toggle_button.on("click", function () {
                    _this.parent_element.toggleClass("vla-panel-on");
                });
            };
            Panel.prototype.build_html_dom = function () {
                this.html_element = $('<div id="cy_panel">');
                //this.html_element.html('');
                this.show_hidden_form = new Show_Hidden(this.html_element);
                this.search_panel = new SearchPanel(this.html_element);
                this.focus_depth_select = new TestFocusDepth(this.html_element);
                this.filter_by_type_form = new Filter_by_type(this.html_element);
                this.vertex_filter_label_form = new Show_Labels(this.html_element);
                this.layout_form = new Layouts(this.html_element);
                this.snapshot_buttons = new Snapshot_buttons(this.html_element);
                this.selection_info_panel = new SelectedInfoPanel(this.parent_element);
                this.toggle_button = $("<div class='vla-panel-toggler'>");
                this.toggle_button.html("<span class='fa fa-bars' aria-hidden='true'>");
                this.toggle_button.appendTo(this.parent_element);
            };
            Panel.prototype.on_selected_nodes_change = function (selected_nodes, selected_edges) {
                this.html_element.removeClass("sel-n-one sel-n-two sel-e-one sel-mixed sel-any");
                if (selected_edges > 0 || selected_nodes > 0) {
                    this.html_element.addClass("sel-any");
                }
                if (selected_edges == 0 && (selected_nodes > 0 && selected_nodes < 3)) {
                    // 1 or 2 nodes only
                    if (selected_nodes == 1)
                        this.html_element.addClass("sel-n-one");
                    else
                        this.html_element.addClass("sel-n-two");
                }
                else if (selected_nodes == 0 && selected_edges == 1) {
                    // one edge only
                    this.html_element.addClass("sel-e-one");
                }
                else {
                    // mixed selection
                    this.html_element.addClass("sel-mixed");
                }
            };
            return Panel;
        }(HTML_UI));
        UI.Panel = Panel;
        var Layouts = (function (_super) {
            __extends(Layouts, _super);
            function Layouts() {
                _super.apply(this, arguments);
            }
            Layouts.prototype.bind_events = function () {
                var _this = this;
                this.control.on("change", function () {
                    _this.trigger("layout-change", _this.value());
                });
            };
            Layouts.prototype.build_html_dom = function () {
                this.html_element = $('<div>').addClass('vla-layouts-form');
                this.html_element.html('<label for="layout_select">Re-Arrange:</label>' +
                    '<select id="layout_select">' +
                    '<option value="concentric">Concentric</option>' +
                    '<option value="breadthfirst">Breadthfirst</option>' +
                    '<option value="breadthfirst_c" selected>Breadthfirst (circular)</option>' +
                    // '<option value="grid">Grid</option>'+
                    '<option value="cose">Cose</option>' +
                    '<option value="cola">Cola (inf)</option>' +
                    // '<option value="spread">Spread</option>'+
                    // '<option value="cose-bilkent">Cose-Bilkent</option>'+
                    '</select>');
                this.control = this.html_element.find("select");
            };
            return Layouts;
        }(HTML_INPUT));
        var Show_Labels = (function (_super) {
            __extends(Show_Labels, _super);
            function Show_Labels() {
                _super.apply(this, arguments);
            }
            Show_Labels.prototype.bind_events = function () {
                var _this = this;
                this.control.on("change", function () {
                    _this.trigger("show-labels-change", _this.value());
                });
                $(this.html_element).find(".vla-checkbox").on("click", function () {
                    var span = $(this);
                    var input = $(this).find("input");
                    var checked = input.is(":checked");
                    checked = !checked;
                    if (checked) {
                        span.addClass("vla-cb-checked");
                        input.attr("checked", "checked");
                    }
                    else {
                        span.removeClass("vla-cb-checked");
                        input.removeAttr("checked");
                    }
                    input.trigger("change");
                });
            };
            Show_Labels.prototype.build_html_dom = function () {
                this.html_element = $('<div class="vla-show-labels-form">');
                this.html_element.html('<span class="vla-checkbox"><span class="fa fa-square vla-cb-empty"></span><span class="fa fa-check-square vla-cb-check"></span><input type="checkbox" value="1" checked />Show all labels</span>');
                //this.html_element.html('<label><input id="cy_show_labels" type="checkbox" value="1" />Show all labels</label>');
                this.control = this.html_element.find("input");
            };
            Show_Labels.prototype.value = function () {
                return this.control.is(":checked");
            };
            return Show_Labels;
        }(HTML_INPUT));
        var Show_Hidden = (function (_super) {
            __extends(Show_Hidden, _super);
            function Show_Hidden() {
                _super.apply(this, arguments);
            }
            Show_Hidden.prototype.bind_events = function () {
                var _this = this;
                this.control.on("click", function () {
                    _this.trigger("show-hidden-click");
                });
            };
            Show_Hidden.prototype.build_html_dom = function () {
                this.html_element = $('<div class="show-all-block on-any-hidden">');
                this.html_element.html('<button>Restore all hidden</button>');
                this.control = this.html_element.find("button");
            };
            Show_Hidden.prototype.value = function () {
            };
            return Show_Hidden;
        }(HTML_INPUT));
        var Filter_by_type = (function (_super) {
            __extends(Filter_by_type, _super);
            function Filter_by_type() {
                _super.apply(this, arguments);
            }
            Filter_by_type.prototype.bind_events = function () {
                var _this = this;
                $(this.html_element).find("input[type=checkbox]").on("change", function (evt) {
                    _this.trigger("type-filter-change", { type: $(this).val(), state: $(this).is(":checked") });
                });
                $(this.html_element).find(".vla-checkbox").on("click", function () {
                    var span = $(this);
                    var input = $(this).find("input");
                    var checked = input.is(":checked");
                    checked = !checked;
                    if (checked) {
                        span.addClass("vla-cb-checked");
                        input.attr("checked", "checked");
                    }
                    else {
                        span.removeClass("vla-cb-checked");
                        input.removeAttr("checked");
                    }
                    input.trigger("change");
                });
            };
            Filter_by_type.prototype.build_html_dom = function () {
                this.html_element = $('<div class="vla-type-filter-form"></div>');
                this.controls = {};
                for (var type_key in Filter_by_type.types) {
                    if (!Filter_by_type.types.hasOwnProperty(type_key))
                        continue;
                    var control = $('<span class="vla-checkbox vla-cb-checked"><span class="fa fa-square vla-cb-empty"></span><span class="fa fa-check-square vla-cb-check"></span><input type="checkbox" value="' + type_key + '" checked />' + Filter_by_type.types[type_key] + '</span>');
                    control.appendTo(this.html_element);
                    this.controls[type_key] = this.html_element.find("input[value=" + type_key + "]");
                }
            };
            Filter_by_type.prototype.value = function () {
                var state = {};
                for (var type_key in Filter_by_type.types) {
                    if (!Filter_by_type.types.hasOwnProperty(type_key))
                        continue;
                    state[type_key] = this.controls[type_key].val();
                }
                return state;
            };
            Filter_by_type.prototype.type_state = function (type) {
                if (this.controls.hasOwnProperty(type))
                    return this.controls[type].is(":checked");
                else
                    return false;
            };
            Filter_by_type.types = { 'person': 'Person', 'company': 'Company', 'website': 'Website', 'location': 'Location', 'company_number': 'Company number', 'officer_id': 'Officer ID' };
            return Filter_by_type;
        }(HTML_INPUT));
        var Snapshot_buttons = (function (_super) {
            __extends(Snapshot_buttons, _super);
            function Snapshot_buttons() {
                _super.apply(this, arguments);
            }
            Snapshot_buttons.prototype.bind_events = function () {
                var _this = this;
                // png
                this.button_snapshot_all.on("click", function () {
                    _this.trigger("snapshot-all-click", _this.select_snapshot_type.val());
                });
                this.button_snapshot_selected.on("click", function () {
                    _this.trigger("snapshot-selected-click", _this.select_snapshot_type.val());
                });
            };
            Snapshot_buttons.prototype.build_html_dom = function () {
                var div;
                this.html_element = $('<div class="vla-snapshot-form">');
                this.html_element.append($('<div>Snapshot as:</div>'));
                this.select_snapshot_type = $("<select>" +
                    "<option value='png'>PNG</option>" +
                    "<option value='jpg'>JPG</option>" +
                    "<option value='json'>JSON</option>" +
                    "</select>");
                this.button_snapshot_all = $("<button>").html("All");
                this.button_snapshot_selected = $("<button>").html("Selected").addClass("on-any-selected");
                this.html_element.append(this.select_snapshot_type);
                this.html_element.append(this.button_snapshot_all);
                this.html_element.append(this.button_snapshot_selected);
            };
            return Snapshot_buttons;
        }(HTML_UI));
        var TestFocusDepth = (function (_super) {
            __extends(TestFocusDepth, _super);
            function TestFocusDepth() {
                _super.apply(this, arguments);
            }
            TestFocusDepth.prototype.bind_events = function () {
                var _this = this;
                this.control.on("change", function () {
                    _this.trigger("focus-depth-change", _this.value());
                });
            };
            TestFocusDepth.prototype.build_html_dom = function () {
                var maxDepth = 10;
                this.html_element = $('<div class="focus-panel">');
                var html = '<label for="focus_depth_select">Focus depth:</label>' +
                    '<select id="focus_depth_select">';
                for (var depth = 1; depth <= maxDepth; depth++) {
                    html += '<option>' + depth + '</option>';
                }
                html += '</select>';
                this.html_element.html(html);
                this.control = this.html_element.find("select");
            };
            return TestFocusDepth;
        }(HTML_INPUT));
        var SelectedInfoPanel = (function (_super) {
            __extends(SelectedInfoPanel, _super);
            function SelectedInfoPanel() {
                _super.apply(this, arguments);
            }
            SelectedInfoPanel.prototype.bind_events = function () {
                var _this = this;
                this.object_info_header_toggler.on('click', function () {
                    _this.object_info_div.toggle();
                    _this.object_info_buttons.toggle();
                    _this.object_info_header_toggler.toggleClass('vla-opened');
                });
                var buttons = this.object_info_buttons.find('button');
                console.log("buttons", buttons);
                buttons.on("click", function (e) {
                    var action = $(e.target).data('action');
                    console.log("action!", action);
                    var params = { action: action, target: _this.node };
                    _this.trigger("graph-action", params);
                });
                buttons = this.selection_info_buttons.find('button');
                buttons.on("click", function (e) {
                    var action = $(e.target).data('action');
                    _this.trigger("graph-action", { action: action });
                });
            };
            SelectedInfoPanel.prototype.build_html_dom = function () {
                this.html_element = $('<div class="vla-selection-panel"></div>');
                this.object_info_header = $('<div class="vla-object-info-header">').appendTo(this.html_element);
                this.object_info_header_label = $('<span>').appendTo(this.object_info_header);
                this.object_info_header_toggler = $('<span class="fa fa-unsorted vla-toggler">').appendTo(this.object_info_header);
                this.object_info_div = $('<div class="vla-object-info"></div>').appendTo(this.html_element);
                this.object_info_buttons = $('<div class="vla-object-info-buttons"><button data-action="center">center</button><button data-action="fit">fit</button></div>').appendTo(this.html_element);
                this.selection_info_div = $('<div class="vla-selection-info"></div>').appendTo(this.html_element);
                this.selection_info_buttons = $('<div class="vla-selection-info-buttons"><button data-action="center-selected">center</button><button data-action="fit-selected">fit</button></div>').appendTo(this.html_element);
            };
            SelectedInfoPanel.prototype.update_content = function (selected_elements) {
                this.update_selected_object_info(selected_elements);
                this.update_selection_info(selected_elements);
            };
            SelectedInfoPanel.prototype.update_selected_object_info = function (selected_elements) {
                var node = selected_elements.eq(0);
                this.node = node;
                var html = '';
                if (node.length) {
                    if (node.isNode()) {
                        this.object_info_header_label.html("Node: <span class='fa'>" + node.data().icon + "</span> " + GraphLink(node.data('id'), node.data('id') + " " + node.data().label));
                        html += this.node_info(node);
                    }
                    else if (node.isEdge()) {
                        this.object_info_header_label.html("Edge: " + GraphLink(node.data('id')));
                        html += this.edge_info(node);
                    }
                    else
                        console.warn("update_content: Unknown node type:", node);
                    GraphLinksReBind(this.object_info_div);
                    GraphLinksReBind(this.object_info_header_label);
                    this.html_element.show();
                }
                else {
                    this.node = null;
                    this.object_info_div.html("");
                    this.html_element.hide();
                }
                this.object_info_div.html(html);
            };
            SelectedInfoPanel.prototype.update_selection_info = function (selected_elements) {
                var selected_num = selected_elements.length;
                if (selected_num > 1) {
                    this.selection_info_div.html("Total selected:" + selected_num);
                    this.selection_info_div.show();
                    this.selection_info_buttons.show();
                }
                else {
                    this.selection_info_div.html("");
                    this.selection_info_div.hide();
                    this.selection_info_buttons.hide();
                }
            };
            SelectedInfoPanel.prototype.node_info = function (node) {
                var html = '';
                var data = node.data();
                //html += "Node: " + GraphLink(node.data('id')) + " (" + data.type + ")<br />";
                if (typeof data['raw']['name'] !== 'undefined')
                    html += "name:" + data.raw.name + "<br />";
                if (typeof data['raw']['title'] !== 'undefined')
                    html += "title:" + data.raw.title + "<br />";
                if (typeof data['raw']['summary'] !== 'undefined')
                    html += "summary:" + data.raw.summary;
                // console.log("node.data",data);
                if (data.risks) {
                    html += "<b>Risks</b>:<br>";
                    html += "<p>" + data.risks["description"] + "</p>";
                    html += "<b>external-credit-score</b>:" + data.risks["external-credit-score"] + "<br />";
                }
                return html;
            };
            SelectedInfoPanel.prototype.edge_info = function (edge) {
                var html = '';
                //html += "Edge: " + edge.id() + "<br />";
                html += GraphLink(edge.source().id()) + " -> " + GraphLink(edge.target().id());
                return html;
            };
            return SelectedInfoPanel;
        }(HTML_UI));
        var SearchPanel = (function (_super) {
            __extends(SearchPanel, _super);
            function SearchPanel() {
                _super.apply(this, arguments);
            }
            SearchPanel.prototype.bind_events = function () {
                var _this = this;
                this.form.on('submit', function (e) {
                    e.preventDefault();
                    console.log("search form submit");
                    _this.trigger('search-submit', _this.input.val());
                });
                // tt-cursor
            };
            SearchPanel.prototype.build_html_dom = function () {
                this.html_element = $("<div>").addClass("vla-search");
                this.form = $("<form action='' method='get'>");
                this.input = $("<input type='text'>").attr('placeholder', 'Search');
                $("<input type='submit'>").hide().appendTo(this.form);
                this.input.appendTo(this.form);
                this.html_element.append(this.form);
            };
            return SearchPanel;
        }(HTML_UI));
        function GraphLink(node_id, value) {
            var label = (typeof value != 'undefined') ? value : node_id;
            var link = $("<span>").addClass('vla-graph-link').html(label).attr('data-id', node_id);
            return link[0].outerHTML;
        }
        UI.GraphLink = GraphLink;
        function GraphLinksReBind(target_div) {
            target_div.find(".vla-graph-link").unbind("click").bind('click', function () {
                var node_id = $(this).data('id');
                console.log("===node_id.this", this);
                console.log("===node_id", node_id);
                $(this).trigger("vla.graph-link-clicked", node_id);
            });
        }
        UI.GraphLinksReBind = GraphLinksReBind;
    })(UI = VLA.UI || (VLA.UI = {}));
})(VLA || (VLA = {}));
//# sourceMappingURL=vla_ui_panel.js.map