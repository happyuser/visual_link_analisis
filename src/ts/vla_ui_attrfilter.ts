///<reference path="vla_ui_html_ui.ts"/>
///<reference path="vla_ui_panel.ts"/>
///<reference path="libs/randomcolor.d.ts"/>

namespace VLA {
	export namespace UI {

		export class AttrFilter extends HTML_UI {
			attr_select:JQuery;
			filter_control:JQuery;
			filters_block:JQuery;

			attrs_colors:any = {};

			protected bind_events(): void {
				this.html_element.find("form").on('submit', (e) => {
					e.preventDefault();
					this.trigger('attrfilter-submit', this.filter_control.val());
				});


				this.filters_block.delegate( ".vla-filter-remove", "click", (e) => {
					let block = $(e.target).closest(".vla-filter-block");
					console.log("filter_index", block.data("findex"));
					this.trigger('attrfilter-remove', block.data("findex"));
				});
			}

			protected build_html_dom(): void {
				this.html_element = $('<div>').addClass("vla-attr-filter");
				let label_txt = _("Filter");
				let html = `<form>
<div>
	<label>${label_txt}:
		<select name="attribute"></select>	
	</label>
</div>
<div>
	<input name="search" value=""  />	
</div>
<div class="vla-selected-filters"></div>

<input style="display: none;" type="submit" />
</form>`;

				this.html_element.html(html);

				this.attr_select = this.html_element.find("select");
				this.filter_control = this.html_element.find("input[name=search]");
				this.filters_block = this.html_element.find(".vla-selected-filters");
			}

			public set_attributes_list(list:string[]):void {
				this.attr_select.html("");
				// let colors = randomColor({ count: list.length, luminosity: 'light'});
				let i=0;
				for (let attr of list)
				{
					// Returns an array of ten green colors
					//let color = colors[i++];
					// this.attrs_colors[attr] = color;
					let option = $("<option>").attr("value", attr).html(attr); //.css("background-color", color);
					this.attr_select.append(option);
				}
			}

			public redraw_selected_attrFilters(selected_filters:{a:string,q:string}[]):void {
				let filters_block = this.filters_block;
				filters_block.html("");

				for (let i in selected_filters)
				{
					if (!selected_filters.hasOwnProperty(i))
						continue;
					let pair = selected_filters[i];
					let label_short = pair.q;
					let label_wide = `${ pair.a }: ${ pair.q }`;
					let block = $('<span class="vla-filter-block">').data("findex", i);
					let content_block = $('<span class="vla-filter-content"></span>').attr("title", label_wide).html(label_short);
					block.append(content_block);
					block.append('<span class="vla-filter-remove fa fa-close"></span>');

					let color = this.attrs_colors[pair.a];
					block.css("background-color", color);

					block.appendTo(filters_block);
				}
			}

		}

	}
}