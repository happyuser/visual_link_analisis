/// <reference path="libs/jquery.d.ts" />
/// <reference path="libs/cytoscape.d.ts" />
/// <reference path="vla_ui_graph.ts" />

namespace VLA {


	import CollectionData = Cy.CollectionData;
	export class DataServer {
		private _graph:Cy.Instance;
		private graph_ready_deferred:JQueryDeferred<void>;
		public graph_ready:JQueryPromise<void>;

		protected _userId:number;

		constructor(userId:number)
		{
			this._userId = userId;
			this.graph_ready_deferred = $.Deferred<void>();
			this.graph_ready = this.graph_ready_deferred.promise();
		}

		public graph():Cy.Instance {
			return this._graph;
		}

		set_graph_object(graph:Cy.Instance)
		{
			this._graph = graph;
			this.graph_ready_deferred.resolve();
		}

		add_data_on_graph()
		{
			if (!this.graph())
				console.error("GRAPH NOT READY! trying to load data to undefined graph object, too early");

			// this.import_raw_data();
		}

		// getRemote(params:VLA.ImportOptions): JQueryPromise<void> {
		//
		// 	//$.ajax({ url: "http://188.166.40.105:8080/elementexploration-1.0.7-SNAPSHOT/rest/api/caseseed/getData", data:{caseSeedId: this.caseSeedId, limit:1000}, dataType: "json", type:"GET" })
		// 	// $.ajax({ url: "graph.php", data:{}, dataType: "jsonp" })
		// 	//$.ajax({ url: "sample_norm.json", data:{}, dataType: "json" })
		// 	$.ajax({url: "getData_12.json", data: {}, dataType: "json"})
		// 		.then((data:getDataResponse) => {
		//
		// 			if (!data.vertices) {
		// 				// {"type":"unknown-exception","class":"com.orientechnologies.orient.core.exception.OStorageException"}
		// 				console.error("data not loaded", data);
		// 				this.raw_data_loading_progress.reject();
		// 				return;
		// 			}
		//
		// 			this.raw_data = data;
		//
		// 			this.raw_data_loading_progress.resolve();
		// 		}, function (status) {
		// 			console.log("error:", status);
		// 			this.raw_data_loading_progress.reject(status);
		// 		});
		//
		//
		// 	// $.ajax({url: "getRiskDataByCaseId_12.json", data: {}, dataType: "json"})
		// 	// 	.then((data:getRiskDataByCaseResponse) => {
		// 	// 		if (!data.status || data.status != 200) {
		// 	// 			console.error("data.status!=200, status:", data.status);
		// 	// 		}
		// 	//
		// 	// 		if (!data.body) {
		// 	// 			// {"type":"unknown-exception","class":"com.orientechnologies.orient.core.exception.OStorageException"}
		// 	// 			console.error("data not loaded", data);
		// 	// 			return;
		// 	// 		}
		// 	//
		// 	// 		this.risks_data = data;
		// 	// 		this.risks_data_loading_progress.resolve();
		// 	//
		// 	// 	}, function (status) {
		// 	// 		console.log("error:", status);
		// 	// 		this.risks_data_loading_progress.reject(status);
		// 	// 	});
		//
		// 	// $.when(this.raw_data_import_progress, this.risks_data_loading_progress)
		// 	// 	.then(() => {
		// 	// 		console.info("addLoadedRisks!");
		// 	// 		this.addLoadedRisks();
		// 	// 	});
		//
		//
		// 	return this.raw_data_loading_progress.promise();
		// };

		load_and_import(params:ImportOptions):JQueryPromise<void>
		{
			if (params.type=='json')
				return this.load_and_import_elementexploration_json(params);
			else if (params.type=='gexf')
				return this.load_and_import_gexf(params);
			else if (params.type=='cytoscape')
				return this.load_and_import_cytoscape_json(params);
			else if (params.type=='test_crowd')
				return this.load_and_import_test_crowd(params);
		}

		load_and_import_elementexploration_json(params:ImportOptions):JQueryPromise<void>
		{
			let load_and_import_progress = $.Deferred<void>();

			let load_promise:JQueryPromise<VLA.elementexploration.json_response>;
			if (params.url)
				load_promise = $.ajax({url:params.url, dataType:"json"});
			else if (params.caseId && params.limit) {
				load_promise = elementexploration.json_getData(params.caseId, params.limit);
				load_promise.then(() => {
					elementexploration.json_getRisks(params.caseId, params.limit).then((data) => {
						if (data.status && data.status==200)
						{
							this.graph().batch(() => {
								console.time("addLoadedRisks");
								let affected = 0;
								for (let row of data.body)
								{
									if (row.vertex) {
										let node = this.graph().filter("[id='" + row.vertex+"']");
										if (node.length) {
											// for (let key in row) {
											// 	if (!row.hasOwnProperty(key))
											// 		continue;
											//
											// 	node.data("risks_" + key, row[key]);
											// }
											node.data("risks", row);
											node.addClass("risks");
											affected++;
										}
									}
								}
								console.timeEnd("addLoadedRisks");
								console.log("addLoadedRisks affected", affected);
							});
						}
					});
				})
			}
			else {
				console.error("wrong import params");
				load_and_import_progress.reject("wrong import params");
				return load_and_import_progress.promise();
			}

			load_promise.then((data:VLA.elementexploration.json_response) => {

					if (!data.vertices) {
						console.error("unexpected server response", data);
						if (data.error)
							load_and_import_progress.reject(data.error);
						else
							load_and_import_progress.reject(data);

						return;
					}

					this.import_elementexploration_json(data, load_and_import_progress);

				}, function (xmlhttprequest, textstatus, message) {
					console.log("load and import error:", params, textstatus, message);
					var add_source = "";
					if (params.url)
						add_source = params.url;
					else if (params.caseId)
						add_source = params.caseId;
					load_and_import_progress.reject(textstatus, add_source + ":" + message);
				});

			return load_and_import_progress.promise();
		}

		import_elementexploration_json(data:elementexploration.json_response, progress:JQueryDeferred<void>):void
		{
			this.graph_ready.then(() => {
				this.graph().batch(() => {
					console.time("import elementexploration data");

					for (let k of data.vertices) {
						this.graph().add(elementexploration.json_node_toCytoscape(k));
					}

					for (let k of data.edges) {
						this.graph().add(elementexploration.json_edge_toCytoscape(k));
					}

					console.info("n/e", data.vertices.length, data.edges.length);
					console.timeEnd("import elementexploration data");

					progress.resolve();
				});
			});
		}

		load_and_import_gexf(params:ImportOptions):JQueryPromise<void>
		{
			let load_and_import_progress = $.Deferred<void>();

			$.ajax({url: params.url, data: {}, dataType: "xml"})
				.then((data:XMLDocument) => {

					console.log("load_and_import_gexf", data.childNodes);

					this.import_gexf(data, load_and_import_progress);

				}, function (status) {
					console.log("load and import error:", params, status);
					load_and_import_progress.reject(status);
				});

			return load_and_import_progress.promise();
		}

		import_gexf(xmlDoc:XMLDocument, progress:JQueryDeferred<void>):void
		{
			let error = null;
			if (xmlDoc['parseError'] && xmlDoc['parseError'].errorCode != 0) {
				let parseError = xmlDoc['parseError'];
				error = "XML Parsing Error: " + parseError.reason
					+ " at line " + parseError.line
					+ " at position " + parseError.linepos;
			}
			else {
				if (xmlDoc.documentElement) {
					if (xmlDoc.documentElement.nodeName == "parsererror") {
						error = xmlDoc.documentElement.childNodes[0].nodeValue;
					}
				}
			}
			if (error) {
				progress.reject(error);
				return;
			}


			this.graph_ready.then(() => {
				this.graph().batch(() => {
					console.time("import gefx document");
	
					let nodes_attributes_map = gexf_reader.attributes_map(xmlDoc, 'node');
	
					// nodes
					let nodes_tags = xmlDoc.getElementsByTagName("node");
					for (let i = 0; i < nodes_tags.length; i++) {
						this.graph().add(gexf_reader.node_toCytoscape(nodes_tags[i], nodes_attributes_map));
					}
	
					let edges_tags = xmlDoc.getElementsByTagName("edge");
	
					for (let i = 0; i < edges_tags.length; i++) {
						this.graph().add(gexf_reader.edge_toCytoscape(edges_tags.item(i)));
					}
	
					console.info("n/e", nodes_tags.length, edges_tags.length);
					console.timeEnd("import gefx document");
	
					progress.resolve();
				});
			});
		}

		load_and_import_cytoscape_json(params:ImportOptions):JQueryPromise<void>
		{
			let load_and_import_progress = $.Deferred<void>();

			let load_promise:JQueryPromise<any>;

			load_promise = $.ajax({url:params.url, dataType:"json"});

			load_promise.then((data:any) => {

				this.graph().startBatch();
				this.graph().add(data.elements);
				this.graph().endBatch();
				load_and_import_progress.resolve();

			}, function (status) {
				console.log("load and import error:", params, status);
				load_and_import_progress.reject(status);
			});

			return load_and_import_progress.promise();
		}

		// addLoadedRisks() {
		// 	let data = this.risks_data;
		// 	console.log("addLoadedRisks.body.length", data.body.length);
		// 	this.graph().batch(() => {
		// 		console.time("addLoadedRisks");
		// 		let affected = 0;
		// 		for (let row of data.body)
		// 		{
		// 			if (row.vertex) {
		// 				let node = this.graph().filter("[id='" + row.vertex+"']");
		// 				if (node.length) {
		// 					node.data("risks", row);
		// 					affected++;
		// 				}
		// 			}
		// 		}
		// 		console.timeEnd("addLoadedRisks");
		// 		console.log("addLoadedRisks affected", affected);
		// 	});
		// }

		/*
		 Use the user IDs 21220 or 21238 for testing purpose. Please treat all parameters as UTF-8 strings and never as numbers.
		 */
		upload_document_data(filename:string, filecontent:string, remarks?:string)
		{
			// let url = "http://188.166.40.105:8080/elementexploration-1.0.7-SNAPSHOT/rest/api/";
			// let command = "commonStorageDocument/uploadDocument";
			// csid:this.caseSeedId
			$.ajax({url: "upload.php", data:{ uid: this._userId, f:filename, d:filecontent }, method:"POST"})
				.then((response) => {
					console.info("response", response);
					if (response.substr(0,2)=='OK')
					{
						let fn = response.split(",")[1];
						document.location.href = "file.php?fn=" + encodeURIComponent(fn) + "&f=" + encodeURIComponent(filename);
					}
				}, (evt, data) => {
					console.error("error");
					console.error("evt", evt);
					console.error("data", data);
				});
			//?userId=21220&remarks=xxxx&fileTitle=yyyy
		}

		get_documents_list()
		{
			// commonStorageDocument/myDocuments?userId=1&caseseedId=1&orderBy=uploadedOn&orderIn={asc|desc}&pageNumber=1&recordsPerPage=10
			
			// caseseedId, orderBy, orderIn, pageNumber, recordsPerPage are optional



			// $fields = array('docId','title','remark','size','type','uploadedOn','uploadedBy','modifiedBy','modifiedOn','docName');
			/*
			 [paginationInformation] => stdClass Object
			 (
			 [title] => /elementexploration-1.0.7-SNAPSHOT/rest/api/commonStorageDocument/myDocuments
			 [kind] => list
			 [totalResults] => 18
			 [count] => 8
			 [index] => 2
			 [startIndex] => 0
			 [inputEncoding] => utf-8
			 [outputEncoding] => utf-8
			 )
			 */
		}

		private load_and_import_test_crowd(params: VLA.ImportOptions):JQueryPromise<void> {
			let load_and_import_progress = $.Deferred<void>();

			this.graph_ready.then(() => {
				let cy = this.graph();
				let num_vertices = params.v || 10;
				let vertices_added = 1;

				let v = {
					data: {
						id: "n0"
					},
					position: {
						x: 0,
						y: 0
					}
				};


				let leafs: Cy.CollectionElements[] = [cy.add(v)];
				vertices_added++;

				while (vertices_added < num_vertices) {
					console.log("while go!");
					console.log("vertices_added", vertices_added);

					let next_leafs: Cy.CollectionElements[] = [];
					for (let i = 0; i < leafs.length; i++) {
						let local_root = leafs[i];
						let num_leafs = Math.random() * 5;

						for (let ei = 0; ei < num_leafs; ei++) {
							let v = {
								data: {
									id: "n" + vertices_added
								},
								position: {
									x: 0,
									y: 0
								}
							};
							let local_leaf = cy.add(v);
							vertices_added++;

							let e = {
								"group": "edges",
								"data": {
									"id": local_root.id() + "e" + ei,
									"source": local_root.id(),
									"target": local_leaf.id()
								}
							};
							cy.add(e);

							next_leafs.push(local_leaf);
						}
					}
					leafs = next_leafs;
				}
				load_and_import_progress.resolve();
			});

			return load_and_import_progress.promise();
		}
	}

	interface getRiskDataByCaseResponse {
		body: Array<any>,
		headers: Object,
		status: number
	}

	interface external_json_node {
		id:string
	}
	interface external_json_edge {
		id:string,
		to:string,
		from:string
	}
	interface external_json_response {
		vertices:external_json_node[],
		edges:external_json_edge[]
	}
	export namespace elementexploration {
		//const SERVER = "http://178.62.216.53:3101/";
		const SERVER = "";

		export interface json_node extends external_json_node {
			labelV: string,
			caseId: string,
			name: string,
			title: string,
			__jobItemId: string,
			entityId: string,
			__jobId: string
		}
		export interface json_edge extends external_json_edge {
			labelE: string,
		}
		interface json_error_response {
			error?: string,
			error_n?: string,
			type?: string,
			class?: string
		}
		export interface json_response extends external_json_response, json_error_response {
			vertices: json_node[],
			edges: json_edge[],
		}

		export function json_getData(caseId:string, limit = "1000")
		{
			return $.ajax({
				url: SERVER + "getCase.php",
				//url: SERVER + "/elementexploration-1.0.7-SNAPSHOT/rest/api/caseseed/getData",
				//data: {caseSeedId:caseSeedId,limit:limit},
				data: {caseId:caseId,limit:limit},
				dataType: "json",
				cache: false,
				contentType: 'application/json; charset=utf-8',
				crossDomain: true
			})
		}

		export function json_getRisks(caseId:string, limit = "1000")
		{
			return $.ajax({
				url: SERVER + "getRisks.php",
				data: {caseId:caseId,limit:limit},
				dataType: "json",
				cache: false,
				contentType: 'application/json; charset=utf-8',
				crossDomain: true
			})
		}

		export function json_node_toCytoscape(vertice:json_node):Cy.ElementDefinition {
			// console.log("vertice", vertice);

			let v = {
				data: {},
				position: {
					x: 0,
					y: 0
				},
				classes: vertice.labelV
			};

			v.data = vertice;
			v.data['id'] = vertice.id;
			v.data['_label'] = vertice.name ? vertice.name : (vertice.title ? vertice.title : vertice.id);
			//v.data['_icon'] = VLA.UI.Graph.icon(icon(vertice));

			// if (typeof vertice.position !== 'undefined') {
			// 	v.position = vertice.position;
			// }

			return v;
		}

		// function icon(node:json_node):string
		// {
		// 	let icons_by_labelV = {
		// 		"person" : "user",
		// 		"website": "internet-explorer"
		// 	};
		// 	let icon = icons_by_labelV[node.labelV];
		// 	if (typeof icon == 'undefined')
		// 		icon = 'undefined';
		//
		// 	return icon;
		// }

		export function json_edge_toCytoscape(edge:json_edge):Cy.ElementDefinition {
			let e:Cy.ElementDefinition = {
				group: "edges",
				data: {
					id: edge.id,
					source: edge.from,
					target: edge.to
				}
			};

			for (let key in edge) {
				if (!edge.hasOwnProperty(key))
					continue;

				e.data[key] = edge[key];
			}

			if (edge.labelE)
				e.data['_label'] = edge.labelE;

			return e;
		}
	}

	export namespace gexf_reader {

		interface xml_attribute_definition {
			id:string,
			title:string,
			type:string,
			default_value?:string
		}
		export function attributes_map(xmlDoc:XMLDocument, type:string)
		{
			let xml_attr_maps = xmlDoc.getElementsByTagName("attributes");
			if (!xml_attr_maps.length)
				return {};

			let xml_map;
			for (let i=0;i<xml_attr_maps.length;i++)
			{
				if (xml_attr_maps.item(i).getAttribute("class") == type)
				{
					xml_map = xml_attr_maps.item(i);
					break;
				}
			}
			if (!xml_map)
				return {};

			let attributes = xml_map.getElementsByTagName("attribute");
			let map = {};
			for (let i=0; i<attributes.length;i++)
			{
				let item = attributes.item(i);
				let map_item:xml_attribute_definition = {
					id: item.getAttribute("id"),
					title: item.getAttribute("title"),
					type: item.getAttribute("type"),
				};

				let default_value = item.getElementsByTagName("default").item(0);
				if (default_value)
					map_item.default_value = (typeof default_value.textContent != 'undefined')? default_value.textContent : default_value.text;

				map[item.getAttribute("id")] = map_item;
			}

			//console.log("map", map);
			return map;
		}
		export function node_toCytoscape(node:Element, attributes_map):Cy.ElementDefinition {
			let label = node.getAttribute("label");
			let id = node.getAttribute("id");
			if (typeof label == 'undefined')
				label = id;

			let data = {};
			let xml_attvalues = node.getElementsByTagName("attvalue");
			let attvalues = {};
			if (xml_attvalues.length)
			{
				for (let i=0;i<xml_attvalues.length;i++)
				{
					let name = xml_attvalues[i].getAttribute("for");
					attvalues[name] = xml_attvalues[i].getAttribute("value");
				}
			}
			for (let attribute_name in attributes_map)
			{
				if (!attributes_map.hasOwnProperty(attribute_name))
					continue;

				let attribute:xml_attribute_definition = attributes_map[attribute_name];
				let value = attvalues[attribute_name];

				if (typeof value != 'undefined')
					data[attribute.title] = value;
				else if (typeof attribute.default_value != 'undefined')
				{
					data[attribute.title] = attribute.default_value;
				}
			}

			let v = {
				data: data,
				position: {
					x: 0,
					y: 0
				},
			};

			v.data['id'] = id;
			v.data['_label'] = label;
			v.data['_icon'] = "";

			return v;
		}
		export function edge_toCytoscape(edge:Element):Cy.ElementDefinition {
			let id = edge.getAttribute("id");
			if (typeof id != 'undefined')
				id = "e_" + id;

			let res = {
				group: "edges",
				data: {
					id: id,
					source: edge.getAttribute("source"),
					target: edge.getAttribute("target")
				}
			};

			return res;
		}
	}
}