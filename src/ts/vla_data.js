/// <reference path="libs/jquery.d.ts" />
/// <reference path="libs/cytoscape.d.ts" />
/// <reference path="vla_ui_graph.ts" />
var VLA;
(function (VLA) {
    var DataServer = (function () {
        function DataServer(userId) {
            this._userId = userId;
        }
        DataServer.prototype.graph = function () {
            return this._graph;
        };
        DataServer.prototype.set_graph_object = function (graph) {
            this._graph = graph;
        };
        DataServer.prototype.add_data_on_graph = function () {
            if (!this.graph())
                console.error("GRAPH NOT READY! trying to load data to undefined graph object, too early");
            // this.import_raw_data();
        };
        // getRemote(params:VLA.ImportOptions): JQueryPromise<void> {
        //
        // 	//$.ajax({ url: "http://188.166.40.105:8080/elementexploration-1.0.7-SNAPSHOT/rest/api/caseseed/getData", data:{caseSeedId: this.caseSeedId, limit:1000}, dataType: "json", type:"GET" })
        // 	// $.ajax({ url: "graph.php", data:{}, dataType: "jsonp" })
        // 	//$.ajax({ url: "sample_norm.json", data:{}, dataType: "json" })
        // 	$.ajax({url: "getData_12.json", data: {}, dataType: "json"})
        // 		.then((data:getDataResponse) => {
        //
        // 			if (!data.vertices) {
        // 				// {"type":"unknown-exception","class":"com.orientechnologies.orient.core.exception.OStorageException"}
        // 				console.error("data not loaded", data);
        // 				this.raw_data_loading_progress.reject();
        // 				return;
        // 			}
        //
        // 			this.raw_data = data;
        //
        // 			this.raw_data_loading_progress.resolve();
        // 		}, function (status) {
        // 			console.log("error:", status);
        // 			this.raw_data_loading_progress.reject(status);
        // 		});
        //
        //
        // 	// $.ajax({url: "getRiskDataByCaseId_12.json", data: {}, dataType: "json"})
        // 	// 	.then((data:getRiskDataByCaseResponse) => {
        // 	// 		if (!data.status || data.status != 200) {
        // 	// 			console.error("data.status!=200, status:", data.status);
        // 	// 		}
        // 	//
        // 	// 		if (!data.body) {
        // 	// 			// {"type":"unknown-exception","class":"com.orientechnologies.orient.core.exception.OStorageException"}
        // 	// 			console.error("data not loaded", data);
        // 	// 			return;
        // 	// 		}
        // 	//
        // 	// 		this.risks_data = data;
        // 	// 		this.risks_data_loading_progress.resolve();
        // 	//
        // 	// 	}, function (status) {
        // 	// 		console.log("error:", status);
        // 	// 		this.risks_data_loading_progress.reject(status);
        // 	// 	});
        //
        // 	// $.when(this.raw_data_import_progress, this.risks_data_loading_progress)
        // 	// 	.then(() => {
        // 	// 		console.info("addLoadedRisks!");
        // 	// 		this.addLoadedRisks();
        // 	// 	});
        //
        //
        // 	return this.raw_data_loading_progress.promise();
        // };
        DataServer.prototype.load_and_import = function (params) {
            if (params.type == 'json')
                return this.load_elementexploration_json(params.url);
        };
        DataServer.prototype.load_elementexploration_json = function (url) {
            var _this = this;
            var load_and_import_progress = $.Deferred();
            $.ajax({ url: url, data: {}, dataType: "json" })
                .then(function (data) {
                if (!data.vertices) {
                    // {"type":"unknown-exception","class":"com.orientechnologies.orient.core.exception.OStorageException"}
                    console.error("data not loaded", data);
                    load_and_import_progress.reject(data);
                    return;
                }
                // this.raw_data = data;
                // this.raw_data_loading_progress.resolve();
                _this.import_elementexploration_json(data, load_and_import_progress);
            }, function (status) {
                console.log("error:", status);
                load_and_import_progress.reject(status);
            });
            return load_and_import_progress.promise();
        };
        DataServer.prototype.import_elementexploration_json = function (data, progress) {
            var _this = this;
            this.graph().batch(function () {
                console.time("import elementexploration data");
                for (var _i = 0, _a = data.vertices; _i < _a.length; _i++) {
                    var k = _a[_i];
                    //in_node = data.vertices[k];
                    //var node_type = in_node.labelV;
                    // if (typeof this.nodes_by_type[node_type] == 'undefined')
                    // 	this.nodes_by_type[node_type] = [];
                    _this.graph().add(elementexploration.json_node_toCytoscape(k));
                }
                for (var _b = 0, _c = data.edges; _b < _c.length; _b++) {
                    var k = _c[_b];
                    _this.graph().add(elementexploration.json_edge_toCytoscape(k));
                }
                console.timeEnd("import elementexploration data");
                progress.resolve();
            });
        };
        // import_raw_data() {
        // 	let data = this.raw_data;
        // 	// var in_node, cy_node;
        //
        // 	this.graph().batch(() => {
        // 		console.time("import_raw_data");
        // 		for (let k of data.vertices) {
        // 			//in_node = data.vertices[k];
        // 			//var node_type = in_node.labelV;
        // 			// if (typeof this.nodes_by_type[node_type] == 'undefined')
        // 			// 	this.nodes_by_type[node_type] = [];
        //
        // 			this.graph().add(DataServer.vla_verticeToCy(k));
        // 			//cy_node = this.nodes.push(DataServer.vla_verticeToCy(data.vertices[k]));
        // 			//this.nodes_by_type[node_type].push(cy_node[0]);
        // 		}
        //
        // 		for (let k of data.edges) {
        // 			this.graph().add(DataServer.vla_edgeToCy(k));
        // 		}
        // 		console.timeEnd("import_raw_data");
        // 		this.raw_data_import_progress.resolve();
        // 	});
        //
        // }
        // addLoadedRisks() {
        // 	let data = this.risks_data;
        // 	console.log("addLoadedRisks.body.length", data.body.length);
        // 	this.graph().batch(() => {
        // 		console.time("addLoadedRisks");
        // 		let affected = 0;
        // 		for (let row of data.body)
        // 		{
        // 			if (row.vertex) {
        // 				let node = this.graph().filter("[id='" + row.vertex+"']");
        // 				if (node.length) {
        // 					node.data("risks", row);
        // 					affected++;
        // 				}
        // 			}
        // 		}
        // 		console.timeEnd("addLoadedRisks");
        // 		console.log("addLoadedRisks affected", affected);
        // 	});
        // }
        /*
         Use the user IDs 21220 or 21238 for testing purpose. Please treat all parameters as UTF-8 strings and never as numbers.
         */
        DataServer.prototype.upload_document_data = function (filename, filecontent, remarks) {
            // let url = "http://188.166.40.105:8080/elementexploration-1.0.7-SNAPSHOT/rest/api/";
            // let command = "commonStorageDocument/uploadDocument";
            // csid:this.caseSeedId
            $.ajax({ url: "upload.php", data: { uid: this._userId, f: filename, d: filecontent }, method: "POST" })
                .then(function (response) {
                console.info("response", response);
                if (response.substr(0, 2) == 'OK') {
                    var fn = response.split(",")[1];
                    document.location.href = "file.php?fn=" + encodeURIComponent(fn) + "&f=" + encodeURIComponent(filename);
                }
            }, function (evt, data) {
                console.error("error");
                console.error("evt", evt);
                console.error("data", data);
            });
            //?userId=21220&remarks=xxxx&fileTitle=yyyy
        };
        DataServer.prototype.get_documents_list = function () {
            // commonStorageDocument/myDocuments?userId=1&caseseedId=1&orderBy=uploadedOn&orderIn={asc|desc}&pageNumber=1&recordsPerPage=10
            // caseseedId, orderBy, orderIn, pageNumber, recordsPerPage are optional
            // $fields = array('docId','title','remark','size','type','uploadedOn','uploadedBy','modifiedBy','modifiedOn','docName');
            /*
             [paginationInformation] => stdClass Object
             (
             [title] => /elementexploration-1.0.7-SNAPSHOT/rest/api/commonStorageDocument/myDocuments
             [kind] => list
             [totalResults] => 18
             [count] => 8
             [index] => 2
             [startIndex] => 0
             [inputEncoding] => utf-8
             [outputEncoding] => utf-8
             )
             */
        };
        return DataServer;
    }());
    VLA.DataServer = DataServer;
    var elementexploration;
    (function (elementexploration) {
        function json_node_toCytoscape(vertice) {
            // console.log("vertice", vertice);
            var v = {
                data: {
                    id: vertice.id,
                    //					type: vertice.labelV,
                    label: vertice.name ? vertice.name : (vertice.title ? vertice.title : vertice.id),
                    icon: VLA.UI.Graph.icon(icon(vertice)),
                    //icon: "icons/" + vertice.labelV + ".svg",
                    raw: vertice
                },
                position: {
                    x: -1000000,
                    y: -1000000
                },
                classes: vertice.labelV
            };
            // if (typeof vertice.position !== 'undefined') {
            // 	v.position = vertice.position;
            // }
            return v;
        }
        elementexploration.json_node_toCytoscape = json_node_toCytoscape;
        function icon(node) {
            var icons_by_labelV = {
                "person": "user",
                "website": "internet-explorer"
            };
            var icon = icons_by_labelV[node.labelV];
            if (typeof icon == 'undefined')
                icon = 'undefined';
            return icon;
        }
        function json_edge_toCytoscape(edge) {
            return {
                group: "edges",
                data: {
                    id: edge.id,
                    source: edge.from,
                    target: edge.to
                }
            };
        }
        elementexploration.json_edge_toCytoscape = json_edge_toCytoscape;
    })(elementexploration = VLA.elementexploration || (VLA.elementexploration = {}));
})(VLA || (VLA = {}));
//# sourceMappingURL=vla_data.js.map