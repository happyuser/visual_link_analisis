namespace VLA {
	export namespace UI {
		export function error(message:string):void {
			alert(message);
		}

		export function info(message:string):void {
			alert(message);
		}

		export function confirm(message?:string):boolean
		{
			return window.confirm(message);
		}

		export function prompt(message?:string, _default?:string):string
		{
			if (typeof _default === "string")
				return window.prompt(message, _default);
			else
				return window.prompt(message);
		}

	}
}
