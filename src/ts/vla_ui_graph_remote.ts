
namespace VLA {
	export namespace UI {
		export class RemoteGraph extends Graph {

			init_events() {
				super.init_events();


				function pan_handler() {
					// TODO: get/update nodes from server by pan
					let ex = this.cytoscape().extent();
				}
				this.cytoscape().on("pan", pan_handler);

				// TODO: handle "add node"
				// TODO: handle move(drag) of nodes
			}

			/**
			 * check if there any nodes, edges, properties affected by "time"
			 */
			public setup_dynamic_content(): void {
				// TODO: get this from server

				return super.setup_dynamic_content();
			}

			public apply_time_span(value: JSTimestamp): boolean {
			 	// TODO: reload data from server with timestamp
				return super.apply_time_span(value);
			}

			public setup_geo_content() {
				// TODO: get this from server
				super.setup_geo_content();
			}

			public focus(node, depth = 1) {
				// TODO: focus should be remote too
				return super.focus(node, depth);
			}

			snapshot_json_all() {
				// TODO: make it remote!
				return super.snapshot_json_all();
			}

			snapshot_gexf_all(): string {
				// TODO: make it remote!
				return super.snapshot_gexf_all();
			}

			protected _connect_nodes(source: Cy.CollectionNodes, target: Cy.CollectionNodes): Cy.CollectionElements {
				// TODO: make it remote!
				return super._connect_nodes(source, target);
			}

			group_nodes(nodes: Cy.CollectionNodes): void {
				// TODO: make it remote!
				return super.group_nodes(nodes);
			}

			change_nodes_parent(nodes: Cy.CollectionNodes, parent: Cy.CollectionNodes) {
				// TODO: make it remote!
				return super.change_nodes_parent(nodes, parent);
			}

			ungroup_nodes(nodes: Cy.CollectionElements): void {
				// TODO: make it remote!
				return super.ungroup_nodes(nodes);
			}

			_hide_elements(collectionElements: Cy.CollectionElements): Cy.CollectionElements {
				// TODO: make it remote!
				return super._hide_elements(collectionElements);
			}

			remove(collectionElements: Cy.CollectionElements) {
				// TODO: make it remote!
				return super.remove(collectionElements);
			}

			find_shortest_path(source: Cy.CollectionElements, target: Cy.CollectionElements) {
				// TODO: make it remote!
				return super.find_shortest_path(source, target);
			}

			/*
			 mark_degreeCentrality
			 mark_betweennessCentrality
			 mark_closenessCentrality
			 clustering

			 unmark_centrality ?
			 */
		}
	}
}