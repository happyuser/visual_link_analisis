/// <reference path="libs/jquery.d.ts" />
/// <reference path="libs/cytoscape.d.ts" />
var VLA;
(function (VLA) {
    var UI;
    (function (UI) {
        var Graph = (function () {
            // public
            function Graph(target_div) {
                // if (typeof cytoscape === 'undefined')
                // 	throw new Error('VLA UI JavaScript requires js.Cytoscape');
                // if (typeof jQuery === 'undefined')
                // 	throw new Error('VLA UI JavaScript requires jQuery');
                this.layout_name = 'breadthfirst_c';
                this.parent_element = target_div;
            }
            ;
            Graph.prototype.cytoscape = function () {
                return this.cy;
            };
            ;
            Graph.prototype.busy = function (value) {
                if (typeof value !== 'undefined') {
                    this.in_busy_state = value;
                    if (value)
                        this.ui_spinner.show();
                    else
                        this.ui_spinner.hide();
                }
                //<i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>
                return this.in_busy_state;
            };
            ;
            Graph.prototype.show_nodes_by_type = function (type) {
                // TODO: add caching by type?
                this.cy.$('node[type="' + type + '"]').show();
            };
            Graph.prototype.hide_nodes_by_type = function (type) {
                // TODO: add caching by type?
                this.cy.$('node[type="' + type + '"]').hide();
            };
            Graph.prototype.set_labels_visible = function (show_labels) {
                var nodes = this.cy.$("node");
                if (show_labels)
                    nodes.addClass("show_label");
                else
                    nodes.removeClass("show_label");
            };
            Graph.prototype.init = function () {
                var self = this;
                var progress = $.Deferred();
                var cy_container = $("<div id='cy'>");
                cy_container.appendTo(this.parent_element);
                this.ui_spinner = $("<div class='vla_spinner'>");
                this.ui_spinner.html("<span class='fa fa-refresh fa-spin'></span>")
                    .appendTo(this.parent_element);
                cytoscape({
                    container: cy_container[0],
                    // textureOnViewport: true, // do not redraw graph on drag
                    ready: function () {
                        self.cy = this;
                        self.on_cytoscape_ready();
                        progress.resolve();
                    }
                });
                return progress.promise();
            };
            ;
            Graph.prototype.load_remote_css = function () {
                var _this = this;
                $.ajax({ url: "graph_style.css", dataType: "text" })
                    .then(function (cycss) {
                    _this.cy['style'](cycss);
                });
            };
            Graph.prototype.on_cytoscape_ready = function () {
                this.init_plugins();
                this.init_events();
                this.load_remote_css();
            };
            // init_layout = function() {
            // 	// setup default layout
            // 	layout = cy.makeLayout({name:'grid', fit:false});
            // };
            Graph.prototype.init_events = function () {
                // this.cy.on('layoutstart', function (evt) {
                // 	// console.log("layoutstart", evt);
                // 	//self.busy(true);
                // });
                // this.cy.on('layoutstop', function (evt) {
                // 	//console.log("layoutstop", evt);
                // 	//self.busy(false);
                // });
                var _this = this;
                // hover emulation
                this.cy.on('mouseover', 'edge, node', function (evt) {
                    var edge = evt.cyTarget;
                    edge.addClass("hover");
                    edge.addClass("show_label");
                });
                this.cy.on('mouseout', 'edge, node', function (evt) {
                    var edge = evt.cyTarget;
                    edge.removeClass("hover");
                    edge.removeClass("show_label");
                });
                this.cy.on('click', 'node', function (evt) {
                    // console.log('click node', evt);
                    _this.cy.center(evt.cyTarget);
                });
                this.cy.on('select', function (evt) {
                    var target = evt.cyTarget;
                    target.addClass("selected");
                    if (target.isEdge()) {
                        // highlight connected nodes
                        target.connectedNodes().addClass("selected");
                    }
                    else {
                        target.connectedEdges().addClass("selected");
                    }
                });
                this.cy.on('unselect', function (evt) {
                    var target = evt.cyTarget;
                    if (target.isEdge()) {
                        if (target.connectedNodes(":selected").length == 0) {
                            target.removeClass("selected");
                            target.connectedNodes().removeClass("selected");
                        }
                    }
                    else {
                        target.removeClass("selected");
                        target.connectedEdges().each(function (i, edge) {
                            if (edge.connectedNodes(":selected").length == 0) {
                                edge.removeClass("selected");
                            }
                        });
                    }
                });
                // this.cy.on('zoom', () => {
                // 	console.info('zoom event', this.cy.zoom());
                // 	let radius = (20/this.cy.zoom());
                //
                // 	let font = Math.max(14, (14/this.cy.zoom()));
                // 	console.log("font", font);
                //
                // 	// this.cy.nodes().style("width", radius.toString());
                // 	// this.cy.nodes().style("height", radius.toString());
                // 	this.cy.nodes().style("font-size", font.toString());
                // });
            };
            Graph.prototype.init_plugins = function () {
                var panzoom_defaults = {
                    zoomFactor: 0.05,
                    zoomDelay: 45,
                    minZoom: 0.1,
                    maxZoom: 10,
                    fitPadding: 50,
                    panSpeed: 10,
                    panDistance: 10,
                    panDragAreaSize: 75,
                    panMinPercentSpeed: 0.25,
                    panInactiveArea: 8,
                    panIndicatorMinOpacity: 0.5,
                    zoomOnly: false,
                    fitSelector: undefined,
                    animateOnFit: function () {
                        return false;
                    },
                    fitAnimationDuration: 1000,
                    // icon class names
                    sliderHandleIcon: 'fa fa-minus',
                    zoomInIcon: 'fa fa-plus',
                    zoomOutIcon: 'fa fa-minus',
                    resetIcon: 'fa fa-expand'
                };
                this.cy['panzoom'](panzoom_defaults); // TODO: do it normal way.... dig the d.ts files
                var edgehandles_defaults = {
                    preview: true,
                    stackOrder: 4,
                    handleSize: 10,
                    handleColor: '#ff0000',
                    handleLineType: 'ghost',
                    handleLineWidth: 3,
                    handleIcon: false,
                    handleNodes: 'node',
                    hoverDelay: 150,
                    cxt: false,
                    enabled: false,
                    toggleOffOnLeave: false,
                    edgeType: function (sourceNode, targetNode) {
                        // can return 'flat' for flat edges between nodes or 'node' for intermediate node between them
                        // returning null/undefined means an edge can't be added between the two nodes
                        return 'flat';
                    },
                    loopAllowed: function (node) {
                        // for the specified node, return whether edges from itself to itself are allowed
                        return false;
                    },
                    nodeLoopOffset: -50,
                    nodeParams: function (sourceNode, targetNode) {
                        // for edges between the specified source and target
                        // return element object to be passed to cy.add() for intermediary node
                        return {};
                    },
                    edgeParams: function (sourceNode, targetNode, i) {
                        // for edges between the specified source and target
                        // return element object to be passed to cy.add() for edge
                        // NB: i indicates edge index in case of edgeType: 'node'
                        return {};
                    },
                    start: function (sourceNode) {
                        // fired when edgehandles interaction starts (drag on handle)
                    },
                    complete: function (sourceNode, targetNodes, addedEntities) {
                        // fired when edgehandles is done and entities are added
                    },
                    stop: function (sourceNode) {
                        // fired when edgehandles interaction is stopped (either complete with added edges or incomplete)
                    }
                };
                // this.cy['edgehandles']( edgehandles_defaults );
            };
            ;
            Graph.prototype.add = function (nodes, edges) {
                var _this = this;
                this.cy.batch(function () {
                    console.log("add nodes");
                    _this.cy.add(nodes);
                    if (typeof edges !== 'undefined')
                        _this.cy.add(edges);
                });
            };
            ;
            Graph.prototype.update = function () {
                if (this.layout)
                    this.layout.stop();
                this.layout = this.cy.makeLayout(this.layout_options(this.layout_name));
                this.layout.run();
            };
            Graph.prototype.set_layout = function (_layout_name) {
                this.layout_name = _layout_name;
                this.update();
            };
            Graph.prototype.focus = function (node, depth) {
                var _this = this;
                if (depth === void 0) { depth = 1; }
                // add focused=true property somewhere
                // may be we should store all hidden elements to not show everything, just what we hide here
                // mey be we should not show hidden neighbours
                this.clear_focus(false);
                var layoutPadding = 50;
                var layoutDuration = 500;
                // highlight
                var nhood = node.closedNeighborhood();
                console.time("cycle");
                // 1 depth level we already have in nhood
                for (var i = 1; i < depth; i++) {
                    nhood = nhood.closedNeighborhood();
                }
                console.timeEnd("cycle");
                node.select();
                var cy = this.cy;
                cy.batch(function () {
                    // cy.elements().not( nhood ).removeClass('highlighted').addClass('faded');
                    // nhood.removeClass('faded').addClass('highlighted');
                    nhood.show();
                    _this.unfocused_elements = cy.elements().not(nhood).remove();
                    console.log("removed %d elements", _this.unfocused_elements.length);
                    //this.update();
                    // var npos = node.position();
                    // var w = window.innerWidth;
                    // var h = window.innerHeight;
                    cy.stop().animate({
                        fit: {
                            eles: nhood,
                            padding: layoutPadding
                        }
                    }, {
                        duration: layoutDuration
                    });
                    // 	.delay( layoutDuration, function(){
                    // 	nhood.layout({
                    // 		name: 'concentric',
                    // 		padding: layoutPadding,
                    // 		animate: true,
                    // 		animationDuration: layoutDuration,
                    // 		boundingBox: {
                    // 			x1: npos.x - w/2,
                    // 			x2: npos.x + w/2,
                    // 			y1: npos.y - w/2,
                    // 			y2: npos.y + w/2
                    // 		},
                    // 		fit: true,
                    // 		concentric: function( n ){
                    // 			if( node.id() === n.id() ){
                    // 				return 2;
                    // 			} else {
                    // 				return 1;
                    // 			}
                    // 		},
                    // 		levelWidth: function(){
                    // 			return 1;
                    // 		}
                    // 	});
                    // } );
                });
            };
            ;
            Graph.prototype.clear_focus = function (do_layout_update) {
                var _this = this;
                if (do_layout_update === void 0) { do_layout_update = true; }
                // add clear focus ONLY if focused
                var cy = this.cy;
                cy.batch(function () {
                    // cy.$('.highlighted').forEach(function(n){
                    // 	n.animate({
                    // 		position: n.data('orgPos')
                    // 	});
                    // });
                    //cy.elements().removeClass('highlighted').removeClass('faded');
                    // cy.elements().show();
                    if (_this.unfocused_elements) {
                        _this.unfocused_elements.restore();
                    }
                });
                // if (do_layout_update)
                // 	this.update();
            };
            Graph.icons = function () {
                if (typeof Graph._icons == 'undefined') {
                    Graph._icons = {};
                    // Graph._icons['undefined'] = String.fromCharCode(0xf128);
                    Graph._icons['undefined'] = '';
                    var reg = new RegExp("^\\.fa-([^:]*)::before$");
                    for (var i = 0; i < document.styleSheets.length; i++) {
                        var styleSheet = document.styleSheets[i];
                        var classes = styleSheet['rules'] || styleSheet['cssRules'];
                        for (var x = 0; x < classes.length; x++) {
                            if (typeof classes[x].selectorText != 'string')
                                continue;
                            var match = classes[x].selectorText.match(reg);
                            if (match) {
                                //console.warn("css ---- ", classes[x].selectorText, (classes[x].cssText) ? classes[x].cssText : classes[x].style.cssText );
                                if (!classes[x].style || !classes[x].style.content)
                                    continue;
                                var icon = classes[x].style.content;
                                if (icon.length == 3)
                                    icon = icon[1];
                                Graph._icons[match[1]] = icon;
                            }
                        }
                    }
                    console.log(Graph._icons);
                }
                return Graph._icons;
            };
            Graph.icon = function (name) {
                var code = Graph.icons()[name];
                if (typeof code === 'undefined')
                    code = Graph.icons()['undefined'];
                return code;
            };
            Graph.prototype.layout_options = function (layout_name) {
                var layouts = {};
                layouts['cose'] = {
                    name: 'cose',
                    fit: false,
                    idealEdgeLength: 100,
                    nodeOverlap: 20,
                    animate: true,
                    randomize: true
                };
                layouts['grid'] = { name: 'grid', animate: true };
                layouts['concentric'] = {
                    name: 'concentric',
                    fit: true,
                    padding: 30,
                    startAngle: 3 / 2 * Math.PI,
                    sweep: undefined,
                    clockwise: true,
                    equidistant: false,
                    minNodeSpacing: 10,
                    boundingBox: undefined,
                    avoidOverlap: true,
                    height: undefined,
                    width: undefined,
                    concentric: function (node) {
                        // var levels = {
                        // 	person: 0,
                        // 	website: 1,
                        // 	company: 2,
                        // 	location: 3,
                        // 	company_number: 4,
                        // 	officer_id: 5
                        // };
                        // console.log("conc", levels[node.data('labelV')]);
                        // return levels[node.data('labelV')];
                        return node.degree();
                    },
                    levelWidth: function (nodes) {
                        return nodes.maxDegree() / 4;
                    },
                    animate: true,
                    animationDuration: 500,
                    animationEasing: undefined,
                    ready: undefined,
                    stop: undefined // callback on layoutstop
                };
                layouts['breadthfirst'] = {
                    name: 'breadthfirst',
                    fit: true,
                    directed: false,
                    padding: 150,
                    circle: false,
                    spacingFactor: 3.75,
                    boundingBox: undefined,
                    avoidOverlap: true,
                    roots: undefined,
                    maximalAdjustments: 0,
                    animate: true,
                    animationDuration: 500,
                    animationEasing: undefined,
                    ready: undefined,
                    stop: undefined // callback on layoutstop
                };
                layouts['breadthfirst_c'] = {
                    name: 'breadthfirst',
                    fit: false,
                    directed: false,
                    padding: 150,
                    circle: true,
                    spacingFactor: 3.75,
                    boundingBox: undefined,
                    avoidOverlap: true,
                    roots: undefined,
                    maximalAdjustments: 0,
                    animate: true,
                    animationDuration: 500,
                    animationEasing: undefined,
                    ready: undefined,
                    stop: undefined // callback on layoutstop
                };
                // layouts.cola = {
                // 	name: 'cola',
                // 	nodeSpacing: 5,
                // 	edgeLengthVal: 45,
                // 	animate: true,
                // 	randomize: false,
                // 	maxSimulationTime: 1500
                // };
                layouts['cola'] = {
                    name: 'cola',
                    animate: true,
                    refresh: 1,
                    maxSimulationTime: 4000,
                    ungrabifyWhileSimulating: false,
                    fit: false,
                    padding: 30,
                    boundingBox: undefined,
                    // layout event callbacks
                    ready: function () {
                    },
                    stop: function () {
                    },
                    // positioning options
                    randomize: false,
                    avoidOverlap: false,
                    handleDisconnected: true,
                    nodeSpacing: function (node) {
                        return 10;
                    },
                    flow: undefined,
                    alignment: undefined,
                    // different methods of specifying edge length
                    // each can be a constant numerical value or a function like `function( edge ){ return 2; }`
                    edgeLength: 200,
                    edgeSymDiffLength: undefined,
                    edgeJaccardLength: undefined,
                    // iterations of cola algorithm; uses default values on undefined
                    unconstrIter: undefined,
                    userConstIter: undefined,
                    allConstIter: undefined,
                    // infinite layout options
                    infinite: true // overrides all other options for a forces-all-the-time mode
                };
                layouts['cose-bilkent'] = {
                    name: 'cose-bilkent',
                    // Called on `layoutready`
                    ready: function () {
                    },
                    // Called on `layoutstop`
                    stop: function () {
                    },
                    // Whether to fit the network view after when done
                    fit: false,
                    // Padding on fit
                    padding: 10,
                    // Whether to enable incremental mode
                    randomize: true,
                    // Node repulsion (non overlapping) multiplier
                    nodeRepulsion: 4500,
                    // Ideal edge (non nested) length
                    idealEdgeLength: 50,
                    // Divisor to compute edge forces
                    edgeElasticity: 0.45,
                    // Nesting factor (multiplier) to compute ideal edge length for nested edges
                    nestingFactor: 0.1,
                    // Gravity force (constant)
                    gravity: 0.25,
                    // Maximum number of iterations to perform
                    numIter: 2500,
                    // For enabling tiling
                    tile: true,
                    // Type of layout animation. The option set is {'during', 'end', false}
                    animate: 'during',
                    // Represents the amount of the vertical space to put between the zero degree members during the tiling operation(can also be a function)
                    tilingPaddingVertical: 10,
                    // Represents the amount of the horizontal space to put between the zero degree members during the tiling operation(can also be a function)
                    tilingPaddingHorizontal: 10,
                    // Gravity range (constant) for compounds
                    gravityRangeCompound: 1.5,
                    // Gravity force (constant) for compounds
                    gravityCompound: 1.0,
                    // Gravity range (constant)
                    gravityRange: 3.8
                };
                layouts['spread'] = {
                    name: 'spread',
                    animate: true,
                    ready: undefined,
                    stop: undefined,
                    fit: true,
                    minDist: 20,
                    padding: 20,
                    expandingFactor: -1.0,
                    // criterium then it expands the network of this amount
                    // If it is set to -1.0 the amount of expansion is automatically
                    // calculated based on the minDist, the aspect ratio and the
                    // number of nodes
                    maxFruchtermanReingoldIterations: 50,
                    maxExpandIterations: 4,
                    boundingBox: undefined,
                    randomize: false // uses random initial node positions on true
                };
                return layouts[layout_name];
            };
            ;
            Graph.prototype.snapshot_image = function (format, only_selected) {
                if (only_selected === void 0) { only_selected = false; }
                if (format != 'png' && format != 'jpg') {
                    console.error("unsupported format:", format);
                    return "";
                }
                if (!only_selected) {
                    return this.cy[format]({ bg: 'white', full: true });
                }
                else {
                    var selected = this.cy.$(":selected");
                    var all_else = this.cy.$(":visible").difference(selected);
                    var selected_connections = selected.edgesWith(selected); // edges between selected nodes
                    all_else.not(selected_connections).hide();
                    this.cy.fit(selected, 100);
                    var img64 = this.cy.png({ bg: 'white', full: false });
                    all_else.show();
                    return img64;
                }
            };
            Graph.prototype.snapshot_json_all = function () {
                return this.cy.json();
            };
            Graph.prototype.snapshot_json_selected = function () {
                // console.log("selected", this.cy.filter(":selected"));
                return this.cy.filter(":selected").jsons();
            };
            Graph.prototype.selected_nodes = function () {
                return this.cy.nodes(":selected");
            };
            Graph.prototype.selected_edges = function () {
                return this.cy.edges(":selected");
            };
            Graph.prototype.connect_nodes = function (source, target) {
                var edge = {
                    data: {
                        source: source.id(),
                        target: target.id()
                    },
                    group: "edges"
                };
                this.cy.add(edge);
            };
            Graph.prototype.group_nodes = function (nodes) {
            };
            Graph.prototype.ungroup_nodes = function (nodes) {
            };
            Graph.prototype.is_group = function (node) {
                return true;
            };
            Graph.prototype.is_inside_group = function (node) {
                return true;
            };
            return Graph;
        }());
        UI.Graph = Graph;
    })(UI = VLA.UI || (VLA.UI = {}));
})(VLA || (VLA = {}));
//# sourceMappingURL=vla_ui_graph.js.map