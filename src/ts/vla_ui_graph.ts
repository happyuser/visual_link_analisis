/// <reference path="libs/jquery.d.ts" />
/// <reference path="libs/cytoscape.d.ts" />
/// <reference path="ui_icons.ts" />
/// <reference path="libs/gexf.d.ts"/>
/// <reference path="libs/moment.d.ts" />

namespace Cy {
	export interface undoRedo {
		action(actionName: string, actionFunction: (args: any) => void, undoFunction: (args: any) => void): Cy.undoRedo // Register action with its undo function & action name. actionFunction's return value will be used to call undoFunction by argument and vice versa. This function is chainable: ur.action(...).action(...)

		do(actionName: string, args: any): any //Calls registered function with action name actionName via actionFunction(args)

		undo() // Undo last action. Returns arguments that are passed to redo.

		redo() // Redo last action. Returns arguments that are passed to undo.

		isUndoStackEmpty() //Get whether undo stack is empty (namely is undoable)

		isRedoStackEmpty() //Get whether undo stack is empty (namely is redoable)

		getUndoStack() //Gets actions (with their args) in undo stack

		getRedoStack() //Gets actions (with their args) in redo stack

		reset() //Resets undo and redo stacks
	}
}

namespace VLA {
	export namespace UI {
		import CollectionElements = Cy.CollectionElements;
		import EdgeDefinition = Cy.EdgeDefinition;
		import DegreeCentralityNormalizedUndirectedResult = Cy.DegreeCentralityNormalizedUndirectedResult;
		import Moment = moment.Moment;
		import months = moment.months;
		import Promise = Cy.Promise;

		export interface graph_context_command {
			content: string, // label of command, html or string
			select(target?, event?): void,
			fillColor?: string,
			activeFillColor?: string,
			disabled?: boolean
		}

		type TimeSpell = {
			"for"?: string,
			value?: any,
			start?: Moment,
			end?: Moment
		};
		type Snapshot_image_format = "png" | "jpg";
		export type Timestamp = number;
		export type JSTimestamp = number;
		export class Graph {

			// private
			private dynamic_values: {start: Moment, end: Moment};
			private dynamic_nodes: Cy.CollectionElements[];
			public actions: Cy.undoRedo;
			private parent_element: JQuery;
			private layout: Cy.LayoutInstance;
			private cy: Cy.Instance;
			private layout_name: string = 'preset';
			private in_busy_state: boolean;
			private ui_spinner: JQuery;
			private options: VLA.UIGraphOptions;
			private basic_styles = {
				"node": {
					"width": "50px",
					"height": "50px",
					"background-color": "gray",
					"text-outline-width": 0,
					"text-outline-color": "black",
					"color": "#ffffff",
					"border-color": "black",
					"border-width": "2px",

					"font-size": "18px",
					"font-family": "FontAwesome, helvetica",
					"font-style": "normal",
					"font-weight": "normal",
					"min-zoomed-font-size": "4",
				},

				"edge": {
					// "content": "data(_label)",
					"width": (e) => {
						let w = e.data('weight') * 1;
						if (!w || isNaN(w))
							w = 1;
						return w;
					},

					"line-color": "black",
					"source-arrow-color": "black",
					"target-arrow-color": "black",
					"font-size": "12px",
					"font-family": "helvetica",
					"font-style": "normal",
					"font-weight": "normal",
					"min-zoomed-font-size": 4,
				},
			};
			private type_styles = [];
			private connection_type_styles: [string, any][] = [];
			private time_point: Moment;

			public coords_scale: number; // 1px = coords_scale in meters
			private ol_loaded: JQueryPromise<void>;
			private ol_load: JQueryDeferred<void>;
			private map: ol.Map;
			private map_div: JQuery;
			private map_view: ol.View;
			private geo_nodes: Cy.CollectionNodes[];
			private temp_data: any = {};

			constructor(target_div: JQuery, options: VLA.UIGraphOptions) {
				// if (typeof cytoscape === 'undefined')
				// 	throw new Error('VLA UI JavaScript requires js.Cytoscape');
				// if (typeof jQuery === 'undefined')
				// 	throw new Error('VLA UI JavaScript requires jQuery');
				let default_options: VLA.UIGraphOptions = {coords_scale: 1200};
				this.options = $.extend({}, default_options, options);
				this.coords_scale = this.options.coords_scale;
				this.parent_element = target_div;

				this.ol_load = $.Deferred<void>();
				this.ol_loaded = this.ol_load.promise();
			};

			public cytoscape() {
				return this.cy;
			};

			public is_dynamic(): boolean {
				return this.dynamic_nodes.length > 0;
			}

			public get_dynamic_values() {
				return this.dynamic_values;
			}

			public busy(value: boolean): boolean {
				if (typeof value !== 'undefined') {
					this.in_busy_state = value;
					if (value)
						this.ui_spinner.show();
					else
						this.ui_spinner.hide();
				}
				//<i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>
				return this.in_busy_state;
			};

			public show_nodes_by_type(type: string) {
				// TODO: add caching by type?
				this.cy.$('node[_type="' + type + '"]').show();
			}

			public hide_nodes_by_type(type: string) {
				// TODO: add caching by type?
				this.cy.$('node[_type="' + type + '"]').hide();
			}

			public set_labels_visible(show_labels: boolean) {
				let nodes = this.cy.$("node");
				if (show_labels)
					nodes.addClass("show_label");
				else
					nodes.removeClass("show_label");
			}

			init(): JQueryPromise<void> {
				let self = this;
				let progress = $.Deferred<void>();
				let cy_container = $("<div>").addClass("vla-cy");
				cy_container.appendTo(this.parent_element);
				this.ui_spinner = $("<div class='vla_spinner'>");
				this.ui_spinner.html("<span class='fa fa-refresh fa-spin'></span>")
					.appendTo(this.parent_element);

				cytoscape({
					container: cy_container[0],
					// textureOnViewport: true, // do not redraw graph on drag
					ready: function () {
						self.cy = this;

						self.on_cytoscape_ready();

						progress.resolve();
					}
				});

				return progress.promise();
			};

			private load_remote_css() {
				$.ajax({url: "graph_style.css", dataType: "text"})
					.then((cycss) => {
						this.cy.style(cycss);
					});
			}

			protected on_cytoscape_ready() {
				this.init_plugins();
				this.init_events();
				this.init_undoRedo();

				this.reassign_all_styles();
			}

			private clear_all_styles() {
				this.cytoscape()['style'](null);
			}

			public add_type_style(type_name: string, style) {
				let selector = this.type_selector(type_name);
				this.type_styles.push([selector, style]);
			}

			public add_connection_type_style(type_name: string, style) {
				let selector = this.type_selector(type_name);
				// console.log("assign connection type style", selector, style);

				this.connection_type_styles.push([selector, style]);
			}

			public reassign_all_styles() {
				this.cytoscape().startBatch();

				this.clear_all_styles();
				this.reassign_system_basic_styles();

				// add types stylesheets here
				this.reassign_types_styles();

				// TODO: add conditional styles here, attribute dependent

				this.reassign_connection_types_styles();

				this.reassign_elements_custom_styles();

				this.reassign_system_important_styles();


				this.cytoscape().endBatch();
			}

			private reassign_connection_types_styles() {
				let style = this.cytoscape().style();

				// console.warn("reassign_connection_types_styles");

				for (let pair of this.connection_type_styles) {
					// console.log("add selector", "edge"+pair[0], pair[1]);
					style.selector("edge" + pair[0]).css(pair[1]);
				}
			}

			private reassign_types_styles() {
				let style = this.cytoscape()['style']();

				for (let pair of this.type_styles) {
					style.selector(pair[0]).css(pair[1]);
				}
			}

			private custom_style_mapper(node, property) {
				let defaults = node.isEdge() ? this.basic_styles.edge : this.basic_styles.node;
				let _style = node.data("_style");

				// console.log("defaults", defaults);
				// console.log("property", property);
				// console.log("_style", _style);
				// console.log("defaults[property]", defaults[property]);

				if (_style && _style[property]) {
					//console.log("_style[property]", _style[property]);
					if (node.isEdge() && property === 'width') {
						// width weighted
						let width = _style[property];
						//console.log("_style[property]1", width);
						let w = node.data('weight') * 1;
						if (!w || isNaN(w))
							w = 1;
						// check for units
						let pair = width.match(/(\d+)(.*)?/);
						if (pair && pair[2]) {
							width = pair[1] * w + pair[2];
						} else {
							width *= w;
						}
						//console.log("_style[property]2", width);
						return width;
					}

					return _style[property];
				}

				return defaults[property];
			}

			/**
			 * create stylesheets like:
			 * node._custom_styled {
			 * 		width: function(element){ return element.data._style.width || default_width;  }
			 * 		color: function(element){ return element.data._style.color || default_color;  }
			 * }
			 * to dynamically assign custom color or font-size or whatever inside style rule
			 */
			private reassign_elements_custom_styles() {
				// colouring, changing size/font
				let cy_style = this.cytoscape()['style']();

				// NODES
				let custom_mapped = ["border-color", "background-color"];
				let custom_mapped_label = ["font-size"];

				let css = {};
				for (let prop of custom_mapped)
					css[prop] = (n) => {
						return this.custom_style_mapper(n, prop);
					};

				let label_css = {};
				for (let prop of custom_mapped_label)
					label_css[prop] = (n) => {
						return this.custom_style_mapper(n, prop);
					};

				cy_style.selector("node._custom_styled").css(css);
				// apply only when label is visible
				cy_style.selector("node._custom_styled.show_label").css(label_css);

				// EDGES
				custom_mapped = ["font-family", "font-size", "line-color", "width", "source-arrow-color", "target-arrow-color"];
				css = {};
				for (let prop of custom_mapped)
					css[prop] = (n) => {
						return this.custom_style_mapper(n, prop);
					};

				cy_style.selector("edge._custom_styled").css(css);
			}

			private reassign_system_basic_styles() {
				// console.info("assign basic styles");
				let style = this.cytoscape()['style']();
				style.selector("node").css(this.basic_styles.node);
				style.selector("node.compound").css({
					"color": "#000000",
					"content": "data(_label)",
					"background-color": "#e2e2e2",
					"z-index": 1
				});

				style.selector("edge").css(this.basic_styles.edge);
			}

			private node_cumulative_risk_scale(risks):number {
				const max_scale = 1, min_scale = 0, scale_span = max_scale - min_scale;

				let CUMRISK = 1- ((1-risks.direct)*(1-risks.indirect)*(1-risks.transactional));
				return min_scale + scale_span*CUMRISK;
			}
			private node_font_size_by_risks(n):number {
				let size:number = 30;
				if (n && n.data() && n.data().risks) {
					size *= this.node_cumulative_risk_scale(n.data().risks);
				}
				return size;
			}
			private node_size_by_risks(n):number {
				let size:number = 50;
				if (n && n.data() && n.data().risks) {
					size *= this.node_cumulative_risk_scale(n.data().risks);
				}
				return size;
			}

			private reassign_system_important_styles() {
				// console.info("assign important styles");
				let style = this.cytoscape()['style']();
				style.selector("node").css({
					"text-valign": "center",
					"content": "data(_icon)",
					"font-size": 30,
					"font-family": "FontAwesome, helvetica",
				});
				style.selector("node.risks").css({
					"font-size": (n) => { return this.node_font_size_by_risks(n) },
					"width": (n) => { return this.node_size_by_risks(n) },
					"height": (n) => { return this.node_size_by_risks(n) }
				});

				style.selector("node.compound").css({
					"text-halign": "center", //The vertical alignment of a node’s label; may have value left, center, or right.
					"text-valign": "top", //The vertical alignment of a node’s label; may have value top, center, or bottom.
					"content": "data(_label)",
					"font-size": (n) => {
						return this.custom_style_mapper(n, "font-size");
					},
					"font-family": (n) => {
						return this.custom_style_mapper(n, "font-family");
					},
				});
				/* label */
				style.selector(".show_label, .hover").css({
					"content": "data(_label)",
					"text-outline-width": 2,
					"text-outline-color": "white",
					"color": "black",
					"font-size": (n) => {
						return this.custom_style_mapper(n, "font-size");
					},
					"font-family": (n) => {
						return this.custom_style_mapper(n, "font-family");
					},
				});

				style.selector("edge:selected, edge.co-selected").css({
					// "width": (e) => {
					// 	let w = e.data('weight')*1;
					// 	if (!w || isNaN(w))
					// 		w = 1;
					// 	return w*4;
					// },
					"width": 4,
					"content": "data(_label)",
					"text-outline-width": 2,
					"text-outline-color": "white",
				});

				style.selector("node.co-selected").css({
					"opacity": 1,
					"border-width": 4,
					"border-color": "purple",
					"z-index": 9998,
				});
				style.selector("edge.co-selected").css({
					"line-color": "purple",
					"target-arrow-color": "purple",
					"source-arrow-color": "purple"
				});

				style.selector("node.af-highlight").css({
					opacity: 1,
					"border-width": 4,
					"border-color": "#ff00ff",
					"z-index": 9998,
				});

				// find path highlight
				style.selector("node.path-highlight").css({
					opacity: 1,
					"border-width": 4,
					"border-color": "#00ff00",
					"z-index": 9998,
				});
				style.selector("edge.path-highlight").css({
					"line-color": "#00ff00",
					"target-arrow-color": "#00ff00",
					"source-arrow-color": "#00ff00"
				});


				// emulated :hover for edges
				style.selector("edge.hover").css({
					"line-color": "red",
					"target-arrow-color": "red",
					"source-arrow-color": "red",
					"z-index": 9999,
				});
				style.selector("node.hover").css({
					"border-width": 1,
					"border-color": "red",
					"z-index": 9999,
				});

				style.selector(":selected").css({
					// "background-color": "black",
					"line-color": "red",
					"target-arrow-color": "red",
					"source-arrow-color": "red",
					// opacity: 1,
					"border-width": 6,
					"border-color": "#FF0000",
					"z-index": 9999
				});
			}

			public set_node_icon(nodes: Cy.CollectionElements|Cy.CollectionNodes|Cy.CollectionEdges, icon_name: string) {
				nodes.not(":child").data("_icon", VLA.UI.Graph.icon(icon_name));

				nodes.filter(":parent").children().data("_icon", VLA.UI.Graph.icon(icon_name));
			}

			public set_element_style(elements: Cy.CollectionElements|Cy.CollectionNodes|Cy.CollectionEdges, style) {
				if (!elements.length)
					return;

				this.cytoscape().startBatch();
				// store custom styles in data._style attribute, ._custom_styled elements get some styles from data(_style)
				elements.forEach((element) => {

					if (!element.data("_style"))
						element.data("_style", {});

					let upd_style = $.extend({}, element.data("_style"), style);
					let all_styles_empty = true;

					for (let key in upd_style) {
						if (!upd_style.hasOwnProperty(key))
							continue;
						let value = upd_style[key];

						if (typeof value != 'undefined' && value != '') {
							all_styles_empty = false;
							break;
						}
					}

					element.data("_style", upd_style);

					if (element.isParent()) {
						this.set_element_style(element.children(), upd_style);
						return;
					}

					if (all_styles_empty)
						element.removeClass("_custom_styled");
					else
						element.addClass("_custom_styled");
				});
				this.cytoscape().endBatch();
			}

			public get_custom_style(element: Cy.CollectionElements): any {
				return element.data("_style");
			}

			public is_custom_styled(element: Cy.CollectionElements): boolean {
				return element.hasClass("_custom_styled");
			}

			public type_selector(type_name: string): string {
				return `[_type="${ type_name }"]`;
			}

			set_edge_type(elements: Cy.CollectionEdges, type_name: string) {
				let edges = elements.filter("edge");
				edges.data("_type", type_name);
			}

			get_edge_type(elements: Cy.CollectionEdges): string {
				return elements.data("_type");
			}

			set_node_type(elements: Cy.CollectionNodes, type_name: string) {
				let nodes = elements.filter("node");
				nodes.data("_type", type_name);
			}

			public not_neighbour_of_selected(element: Cy.CollectionElements): boolean {
				if (element.isEdge())
					return element.connectedNodes(":selected").length == 0;

				// node
				if (element.connectedEdges(":selected").length != 0)
					return false;

				return element.connectedEdges().connectedNodes(":selected").not(element).length == 0;
			}

			init_events() {

				// this.cy.on('layoutstart', function (evt) {
				// 	 console.log("layoutstart", evt.cyTarget['options'].name);
				// 	//self.busy(true);
				// });
				// this.cy.on('layoutstop', function (evt) {
				// 	console.log("layoutstop", evt.cyTarget['options'].name);
				// 	//self.busy(false);
				// });

				// hover emulation
				this.cy.on('mouseover', 'edge, node', function (evt) {
					var edge = evt.cyTarget;
					edge.addClass("hover");
				});
				this.cy.on('mouseout', 'edge, node', function (evt) {
					var edge = evt.cyTarget;
					edge.removeClass("hover");
				});

				// this.cy.on('click', 'node', (evt) => {
				// 	// console.log('click node', evt);
				// 	this.cy.center(evt.cyTarget);
				// });


				this.cy.on('select', (evt) => {
					this.highlight_element_neighbourhood(evt.cyTarget);
				});
				this.cy.on('unselect', (evt) => {
					this.unhighlight_element_neighbourhood(evt.cyTarget);
				});

				// this.cy.on('zoom', () => {
				// 	console.info('zoom event', this.cy.zoom());
				// 	let radius = (20/this.cy.zoom());
				//
				// 	let font = Math.max(14, (14/this.cy.zoom()));
				// 	console.log("font", font);
				//
				// 	this.cy.nodes().style("width", radius.toString());
				// 	this.cy.nodes().style("height", radius.toString());
				// 	this.cy.nodes().style("font-size", font.toString());
				// });
			}

			highlight_element_neighbourhood(target) {
				// mark connected nodes and edges as "co-selected"
				if (target.isEdge()) {
					// highlight connected nodes
					target.connectedNodes().addClass("co-selected");
				}
				else {
					target.connectedEdges().addClass("co-selected");
					target.connectedEdges().connectedNodes().not(target).addClass("co-selected");
					if (target.isChild()) // highlight parent
						target.parent().addClass("co-selected");
					else if (target.isParent()) // highlight kids
						target.children().addClass("co-selected");
				}
			}

			update_node_highlight(element: Cy.CollectionElements) {
				if (this.not_neighbour_of_selected(element))
					element.removeClass("co-selected");
				else
					element.addClass("co-selected");
			}

			unhighlight_element_neighbourhood(target) {
				// remove "co-selected" mark from connected nodes and edges (beware if those are co-selected with some else node/edge)
				if (target.isEdge()) {
					if (target.connectedNodes(":selected").length == 0) {
						// check connected nodes are not connected to selected edges or nodes
						target.connectedNodes().stdFilter(this.not_neighbour_of_selected).removeClass("co-selected");
					}
				}
				else {
					target.connectedEdges().stdFilter(this.not_neighbour_of_selected).removeClass("co-selected");
					target.connectedEdges().connectedNodes().stdFilter(this.not_neighbour_of_selected).removeClass("co-selected");

					if (target.isChild())
						target.parent().stdFilter(this.not_neighbour_of_selected).removeClass("co-selected");
					else if (target.isParent())
						target.children().stdFilter(this.not_neighbour_of_selected).removeClass("co-selected");
				}
			}

			// highlight_selection_neighbourhood_MASS() {
			// 	console.time("HN");
			// 	let target_nodes = this.selected_nodes();
			// 	let target_edges = this.selected_edges();
			//
			// 	this.cytoscape().startBatch();
			// 	// edges -> highlight connected nodes
			// 	target_edges.connectedNodes().addClass("co-selected");
			//
			// 	// mark connected nodes and edges as "co-selected"
			// 	target_nodes.connectedEdges().addClass("co-selected");
			// 	target_nodes.connectedEdges().connectedNodes().addClass("co-selected");
			// 	// targets.connectedEdges().connectedNodes().not(target).addClass("co-selected");
			// 	// highlight parents
			// 	target_nodes.filter(":child").parent().addClass("co-selected");
			// 	// highlight kids
			// 	target_nodes.filter(":parent").children().addClass("co-selected");
			//
			// 	this.cytoscape().endBatch();
			// 	console.timeEnd("HN");
			// }

			init_plugins() {
				let panzoom_defaults = {
					zoomFactor: 0.05, // zoom factor per zoom tick
					zoomDelay: 45, // how many ms between zoom ticks
					minZoom: 0.1, // min zoom level
					maxZoom: 10, // max zoom level
					fitPadding: 50, // padding when fitting
					panSpeed: 10, // how many ms in between pan ticks
					panDistance: 10, // max pan distance per tick
					panDragAreaSize: 75, // the length of the pan drag box in which the vector for panning is calculated (bigger = finer control of pan speed and direction)
					panMinPercentSpeed: 0.25, // the slowest speed we can pan by (as a percent of panSpeed)
					panInactiveArea: 8, // radius of inactive area in pan drag box
					panIndicatorMinOpacity: 0.5, // min opacity of pan indicator (the draggable nib); scales from this to 1.0
					zoomOnly: false, // a minimal version of the ui only with zooming (useful on systems with bad mousewheel resolution)
					fitSelector: undefined, // selector of elements to fit
					animateOnFit: function () { // whether to animate on fit
						return false;
					},
					fitAnimationDuration: 1000, // duration of animation on fit

					// icon class names
					sliderHandleIcon: 'fa fa-minus',
					zoomInIcon: 'fa fa-plus',
					zoomOutIcon: 'fa fa-minus',
					resetIcon: 'fa fa-expand'
				};
				this.cy['panzoom'](panzoom_defaults); // TODO: do it normal way.... dig the d.ts files

				let edgehandles_defaults = {
					preview: true, // whether to show added edges preview before releasing selection
					stackOrder: 4, // Controls stack order of edgehandles canvas element by setting it's z-index
					handleSize: 10, // the size of the edge handle put on nodes
					handleColor: '#ff0000', // the colour of the handle and the line drawn from it
					handleLineType: 'ghost', // can be 'ghost' for real edge, 'straight' for a straight line, or 'draw' for a draw-as-you-go line
					handleLineWidth: 3, // width of handle line in pixels
					handleIcon: false, // Pass an Image-object to use as icon on handle. Icons are resized according to zoom and centered in handle.
					handleNodes: 'node', // selector/filter function for whether edges can be made from a given node
					hoverDelay: 150, // time spend over a target node before it is considered a target selection
					cxt: false, // whether cxt events trigger edgehandles (useful on touch)
					enabled: false, // whether to start the extension in the enabled state
					toggleOffOnLeave: false, // whether an edge is cancelled by leaving a node (true), or whether you need to go over again to cancel (false; allows multiple edges in one pass)
					edgeType: function (sourceNode, targetNode) {
						// can return 'flat' for flat edges between nodes or 'node' for intermediate node between them
						// returning null/undefined means an edge can't be added between the two nodes
						return 'flat';
					},
					loopAllowed: function (node) {
						// for the specified node, return whether edges from itself to itself are allowed
						return false;
					},
					nodeLoopOffset: -50, // offset for edgeType: 'node' loops
					nodeParams: function (sourceNode, targetNode) {
						// for edges between the specified source and target
						// return element object to be passed to cy.add() for intermediary node
						return {};
					},
					edgeParams: function (sourceNode, targetNode, i) {
						// for edges between the specified source and target
						// return element object to be passed to cy.add() for edge
						// NB: i indicates edge index in case of edgeType: 'node'
						return {};
					},
					start: function (sourceNode) {
						// fired when edgehandles interaction starts (drag on handle)
					},
					complete: function (sourceNode, targetNodes, addedEntities) {
						// fired when edgehandles is done and entities are added
					},
					stop: function (sourceNode) {
						// fired when edgehandles interaction is stopped (either complete with added edges or incomplete)
					}
				};

				// this.cy['edgehandles']( edgehandles_defaults );
			};

			protected init_undoRedo() {
				let options = {
					isDebug: true, // Debug mode for console messages
					actions: {},   // actions to be added
					undoableDrag: true // Whether dragging nodes are undoable can be a function as well
				};
				this.actions = this.cytoscape()['undoRedo'](options); // Can also be set whenever wanted.

				this.init_action_connect();
				this.init_action_rename();
				this.init_action_hide();
			}

			init_action_connect() {
				this.actions.action("connect", (args) => {
					args.links = this._connect_nodes(args.source, args.target);
					return args;
				}, (args) => {
					let target = args.target;
					this.cytoscape().remove(args.links);
					this.update_node_highlight(target);

					return args;
				});
			}

			init_action_rename() {
				this.actions.action("rename", (args) => {
					args.saved_label = args.target.data("_label") || "";
					let label = args.label || "";
					args.target.data("_label", label);
					return args;
				}, (args) => {
					args.target.data("_label", args.saved_label);
					return args;
				});
			}

			init_action_hide() {
				this.actions.action("hide", (args) => {
					return this._hide_elements(args);
				}, (args) => {
					args.show();
					return args;
				});
			}


			public add(nodes: Array<any>, edges?: Array<any>) {
				this.cy.batch(() => {
					console.log("add nodes");
					this.cy.add(nodes);
					if (typeof edges !== 'undefined')
						this.cy.add(edges);
				});
			};

			/**
			 * check if there any nodes, edges, properties affected by "time"
			 */
			public setup_dynamic_content(): void {
				// "A big ball of wibbly wobbly, timey wimey stuff"
				console.info("setup_dynamic_content");
				this.dynamic_values = {
					start: moment("9999-12-31").endOf('day'),
					end: moment("0000-01-01").startOf('day')
				};
				this.dynamic_nodes = [];

				this.cytoscape().elements().each((i, el) => {
					// nodes and edges here
					let spells = this.import_spells_data(el);

					if (spells.length) {
						this.dynamic_nodes.push(el);
					}
				});

				// if (this.dynamic_values.start === +Infinity) {
				// 	this.dynamic_values.start = +moment(this.dynamic_values.end, "x").startOf('day').subtract(1,'day');
				// }
				// if (this.dynamic_values.end === -Infinity) {
				// 	this.dynamic_values.end = +moment(this.dynamic_values.start, "x").endOf('day').add(1,'day');
				// }

				console.log("this.dynamic_values", this.dynamic_values);
				console.log("this.dynamic_values.start", this.dynamic_values.start.format("YYYY MMM Do"));
				console.log("this.dynamic_values.end", this.dynamic_values.end.format("YYYY MMM Do"));
				console.log("this.dynamic_nodes", this.dynamic_nodes);
			}

			/**
			 * import all time affected data, collect it to "spells" array
			 * @param el
			 * @returns {TimeSpell[]}
			 */
			private import_spells_data(el: Cy.CollectionElements): TimeSpell[] {
				let spells: TimeSpell[] = [];
				let raw_spells: any[];

				// top level spell
				raw_spells = el.data("spells");
				if (typeof raw_spells === 'undefined' || typeof raw_spells.length === 'undefined') {
					raw_spells = [];
				}
				raw_spells.push({
					start: el.data("start"),
					end: el.data("end")
				});

				// external spells array
				if (raw_spells) {
					let ts;  // timestamp
					for (let raw_spell of raw_spells) {
						let spell: TimeSpell = {};

						if (raw_spell["for"]) {
							spell["for"] = raw_spell["for"];
							spell.value = raw_spell.value;
						}

						let start = raw_spell.start;
						if (start) {
							start = moment(start).startOf('day');
							spell.start = start;
							this.extend_dynamic_values(start);
						}
						let end = raw_spell.end;
						if (end) {
							end = moment(end).endOf('day');
							spell.end = end;
							this.extend_dynamic_values(end);
						}
						if (spell.start || spell.end) {
							spells.push(spell);
						}
					}
				}

				// overwrite
				if (spells.length) {
					//console.log("spells --> ", spells);
					el.data("spells", spells);
				}

				return spells;
			}

			private extend_dynamic_values(time: Moment) {
				let start = time.clone().startOf('day');
				let end = time.clone().endOf('day');
				if (start.isBefore(this.dynamic_values.start)) {
					this.dynamic_values.start = start;
				}
				if (end.isAfter(this.dynamic_values.end)) {
					this.dynamic_values.end = end;
				}
			}

			public apply_time_span(value: JSTimestamp): boolean {
				if (!value)
					return;

				this.time_point = moment(value, "x").endOf('day');
				//console.info("time_point", this.time_point.format("YYYY.MM.DD"));

				let is_any_affected = false;
				// apply changes
				//this.stop_layout();
				this.cytoscape().startBatch();
				for (let el of this.dynamic_nodes) {
					let visible = false;
					//console.log("time check", el.data());

					let spells = el.data("spells");
					if (spells) {
						let affect_attributes: any = {};
						for (let spell of spells) {
							//console.log("spell", spell);
							let check = Graph.match_time_point(this.time_point, spell.start, spell.end);
							//console.log("check", check);
							if (typeof spell.for === 'string') {
								// attribute life
								if (check) {
									affect_attributes[spell.for] = spell.value;
								}
							} else {
								// element life
								visible = visible || check;
							}
						}

						for (let spell of spells) {
							if (typeof spell.for === 'string') {
								let initial_value = el.data("_initial_" + spell.for);
								if (typeof affect_attributes[spell.for] !== 'undefined') {
									// apply time affected value
									is_any_affected = true;
									if (typeof initial_value === 'undefined') {
										// save original value, if it's not saved yet
										let current_value = el.data(spell.for);
										el.data("_initial_" + spell.for, current_value);
									}
									el.data(spell.for, spell.value);
								} else if (typeof initial_value !== 'undefined') {
									// revert initial value
									is_any_affected = true;
									el.data(spell.for, initial_value);
									el.removeData("_initial_" + spell.for);
								}
							}
						}
					}

					if (visible) {
						el.show();
					} else {
						el.hide();
					}
				}
				this.cytoscape().endBatch();
				//this.start_layout();

				return is_any_affected;
			}

			static match_time_point(time_point: Moment, start?: Moment, end?: Moment): boolean {
				//console.info("match_time_point", time_point.format("YYYY.MM.DD"));

				if (start && end) {
					// console.info("TO2",
					// 	start.format("YYYY.MM.DD"),
					// 	end.format("YYYY.MM.DD"),
					// 	time_point.isBetween(start, end, 'day')
					// );
					return time_point.isBetween(start, end, 'day');
				} else if (start) {
					// console.info("STARTS",
					// 	start.format("YYYY.MM.DD"),
					// 	time_point.isAfter(start, 'day')
					// );
					return time_point.isAfter(start, 'day');
				} else if (end) {
					// console.info("ENDS",
					// 	start.format("YYYY.MM.DD"),
					// 	time_point.isBefore(end, 'day')
					// );
					return time_point.isBefore(end, 'day');
				}

				return true; // if no end and no start -- exists always
			}

			public setup_geo_content() {
				this.geo_nodes = [];

				this.cy.nodes().each((i, node) => {
					if (this.node_have_raw_geo_data(node)) {
						this.geo_nodes.push(node);
					}
				});

				this.init_map();
			}

			public init_map() {
				$.getScript("https://openlayers.org/en/v3.20.1/build/ol.js", () => {
					//console.log("map loaded!!!");
					//console.log("coords_scale", this.coords_scale);

					let min_zoom_coeff = 0.000022667184963479284; //0.00005693739435028159
					let min_zoom = this.coords_scale * min_zoom_coeff;
					//console.log("min_zoom", min_zoom);
					this.cytoscape().minZoom(min_zoom);
					//this.cytoscape().maxZoom(1000);

					// setup positions
					for (let i = 0; i < this.geo_nodes.length; i++)
						this.setup_geo_position(this.geo_nodes[i]);
				});
			}

			public map_mode(on: boolean) {
				let self = this;

				function pan_handler() {
					self.sync_map_pan();
				}

				//console.log("call stop layout");
				this.stop_layout();

				if (on) {
					//console.log("Show Map!");
					this.cytoscape().on("pan", pan_handler);
					this.store_viewport();
					this.show_map();
					this.apply_geo_positions();
					this.cytoscape().fit(this.cytoscape().nodes(":locked"), 100);
					// TODO: store layout + apply Cola (or any else working)
				} else {
					//console.log("Hide Map!");
					this.restore_viewport();
					this.cytoscape().off("pan", pan_handler);
					this.hide_map();
					this.undo_geo_positions();
				}
			}

			private store_viewport() {
				let ex = this.cytoscape().extent();
				this.temp_data.premap_pos = {x: ex.x1, y: ex.y1};
				this.temp_data.premap_zoom = this.cytoscape().zoom();
			}

			private restore_viewport() {
				let c = this.temp_data.premap_pos;
				if (typeof c !== 'undefined') {
					this.cytoscape().zoom(this.temp_data.premap_zoom); // important to zoom first

					let tmp_node_data: Cy.ElementDefinition = {
						group: "nodes",
						data: {},
						position: {x: c.x, y: c.y}
					};
					let tmp_node = this.cytoscape().add(tmp_node_data);
					tmp_node.hide();

					let pos = tmp_node.renderedPosition();
					this.cytoscape().remove(tmp_node);
					let delta = {x: pos.x * -1, y: pos.y * -1};

					this.cytoscape().panBy(delta);
				}
			}

			private show_map() {
				this.create_map_if_needed();
				this.map_div.show();
			}

			private hide_map() {
				this.map_div.hide();
			}

			private create_map_if_needed() {
				if (typeof this.map !== 'undefined')
					return;

				let parent = this.parent_element;// .parent()
				this.map_div = $("<div>").addClass("vla-map").appendTo(parent);

				this.map_view = new ol.View({
					center: [0, 0],
					zoom: 6
				});
				this.map = new ol.Map({
					layers: [
						new ol.layer.Tile({
							source: new ol.source.OSM()
						})
					],
					target: this.map_div[0],
					controls: ol.control.defaults({
						attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
							collapsible: false
						})
					}),
					view: this.map_view
				});

				window['gmap'] = this.map;
				window['gview'] = this.map_view;
			}

			private sync_map_pan() {
				let ex = this.cytoscape().extent();
				let cs = this.coords_scale;
				let line = new ol.geom.LineString([[ex.x1 * cs, ex.y1 * cs * -1], [ex.x2 * cs, ex.y2 * cs * -1]]);
				// line.transform('EPSG:4326', 'EPSG:3857');
				//console.log("fit line", line);

				this.map_view.fit(line, this.map.getSize(), {
					padding: [0, 0, 0, 0],
					nearest: true,
					constrainResolution: false
				});
			}

			public is_geodata_available(): boolean {
				return this.geo_nodes.length > 0;
			}

			private node_have_raw_geo_data(el): boolean {
				let lat = el.data('lat') * 1;
				let lon = el.data('lon') * 1;

				return (!isNaN(lat) && !isNaN(lon));
			}

			private node_have_geo_data(el): boolean {
				return typeof this.geo_scratch(el).geo_coords !== 'undefined';
			}

			private setup_geo_position(el: Cy.CollectionNodes): boolean {
				let lat = el.data('lat') * 1;
				let lon = el.data('lon') * 1;

				if (!isNaN(lat) && !isNaN(lon)) {
					let coords = ol.proj.fromLonLat([lon, lat]);
					let graph_coords = {x: coords[0] / this.coords_scale, y: coords[1] / this.coords_scale * -1};

					this.geo_scratch(el).geo_coords = graph_coords;
					return true;
				}
				return false;
			}

			private apply_geo_positions() {
				// 1. save all positions
				this.cytoscape().nodes().each((i, node) => {
					let pos = node.modelPosition();
					this.geo_scratch(node).original_pos = {x: pos.x, y: pos.y};
				});

				// 2. set geo nodes positions
				for (let i = 0; i < this.geo_nodes.length; i++)
					this.apply_geo_position(this.geo_nodes[i]);

				// 3. layout geo nodes neighbours
				this.layout_name = 'cola';
				this.update({
					maxSimulationTime: 2500, stop: () => {
						this.cytoscape().nodes().removeScratch('cola');
					}
				});

			}

			private undo_geo_positions() {
				// 1. restore all positions
				this.cytoscape().nodes().each((i, node) => {
					this.undo_geo_position(node);
				});
			}

			private apply_geo_position(node: Cy.CollectionNodes) {
				//console.log("node", node.data('name'));

				let pos = node.modelPosition();
				this.geo_scratch(node).original_pos = {x: pos.x, y: pos.y};

				let geo_coords = this.geo_scratch(node).geo_coords;
				node.modelPosition(geo_coords);
				node.lock();
			}

			private undo_geo_position(node: Cy.CollectionNodes) {
				let coords = this.geo_scratch(node).original_pos;

				node.unlock();
				node.modelPosition(coords);

				delete this.geo_scratch(node).original_pos;
			}

			private geo_scratch(el: Cy.CollectionNodes): any {
				let geo_scratch = el.scratch('_geo');
				if (typeof geo_scratch === 'undefined') {
					el.scratch('_geo', {});
				}
				geo_scratch = el.scratch('_geo');
				return geo_scratch;
			}


			public update(params?) {
				//let is_new_layout = !this.layout || (this.layout && this.layout.options.name != this.layout_name);
				//console.log("update.layout",this.layout_name);
				//console.log("update.elements", this.cytoscape().elements().length);

				//this.stop_layout();

				let options = this.layout_options(this.layout_name);
				if (this.options.layout_options)
					options = $.extend({}, this.layout_options(this.layout_name), this.options.layout_options);
				if (params)
					options = $.extend({}, options, params);
				// to prevent name override
				options.name = this.layout_options(this.layout_name)['name'];
				if (this.cytoscape().nodes(":locked").length)
					options.randomize = false;

				options.ready = () => {
					this.parent_element.trigger("layout-ready");
				};
				options.stop = () => {
					this.parent_element.trigger("layout-stop");
					if (params && typeof params.stop !== 'undefined')
						params.stop();
				};


				this.layout = this.cy.makeLayout(options);
				this.start_layout();
			}

			public stop_layout() {
				let promise;
				if (this.layout) {
					promise = this.cytoscape().promiseOn('layoutstop');
					this.layout.stop();
				} else {
					promise = Promise.resolve();
				}

				return promise;
			}

			public start_layout() {
				if (this.layout)
					this.layout.run();
			}

			public set_layout(layout_name: string) {
				// if (layout_name === 'map') {
				// 	console.log("switching to map");
				// 	// if switching to map
				// 	if (this.layout_name !== 'map') // init only if realy switching
				// 		this.map_mode(true);
				//
				// 	return;
				// } else if (this.map_div && this.map_div.is(":visible")) {
				// 	// if switching from map
				// 	console.log("turn map off");
				// 	this.map_mode(false);
				// }

				this.layout_name = layout_name;
				if (this.cy)
					this.update();
			}

			// public on(...arg)
			// {
			// 	console.warn("ui on", arg);
			// 	this.cy.on.apply(arg);
			// }

			private unfocused_elements: CollectionElements;
			private focus_target: CollectionElements;

			public in_focus(): boolean {
				return this.parent_element.hasClass("vla-focused") && this.focus_target && this.focus_target.length > 0;
			}

			public focus(node, depth = 1) {
				// add focused=true property somewhere
				// may be we should store all hidden elements to not show everything, just what we hide here
				// mey be we should not show hidden neighbours
				this.clear_focus(false);

				this.parent_element.addClass("vla-focused");
				this.focus_target = node;

				var layoutPadding = 50;
				var layoutDuration = 500;
				// highlight
				var nhood = node;//.closedNeighborhood();
				console.time("cycle");

				for (let i = 0; i < depth; i++) {
					nhood = nhood.closedNeighborhood();
					nhood = nhood.add(nhood.children().closedNeighborhood());
					nhood = nhood.add(nhood.parent());
				}


				// console.log("nhood.length", nhood.length);

				console.timeEnd("cycle");

				//node.select();
				var cy = this.cy;

				cy.batch(() => {

					// cy.elements().not( nhood ).removeClass('highlighted').addClass('faded');
					// nhood.removeClass('faded').addClass('highlighted');
					nhood.show();
					this.unfocused_elements = cy.elements().not(nhood).unselect().remove();
					//console.log("removed %d elements", this.unfocused_elements.length);

					//this.update();

					// var npos = node.position();
					// var w = window.innerWidth;
					// var h = window.innerHeight;


					cy.stop().animate({
						fit: {
							eles: nhood,//cy.elements(),
							padding: layoutPadding
						}
					}, {
						duration: layoutDuration
					});


					// 	.delay( layoutDuration, function(){
					// 	nhood.layout({
					// 		name: 'concentric',
					// 		padding: layoutPadding,
					// 		animate: true,
					// 		animationDuration: layoutDuration,
					// 		boundingBox: {
					// 			x1: npos.x - w/2,
					// 			x2: npos.x + w/2,
					// 			y1: npos.y - w/2,
					// 			y2: npos.y + w/2
					// 		},
					// 		fit: true,
					// 		concentric: function( n ){
					// 			if( node.id() === n.id() ){
					// 				return 2;
					// 			} else {
					// 				return 1;
					// 			}
					// 		},
					// 		levelWidth: function(){
					// 			return 1;
					// 		}
					// 	});
					// } );

				});
			};

			focus_depth(depth: number) {
				this.focus(this.focus_target, depth);
			}

			clear_focus(do_layout_update = false) {
				this.parent_element.removeClass("vla-focused");
				this.focus_target = null;
				// add clear focus ONLY if focused
				var cy = this.cy;
				cy.batch(() => {
					// cy.$('.highlighted').forEach(function(n){
					// 	n.animate({
					// 		position: n.data('orgPos')
					// 	});
					// });

					//cy.elements().removeClass('highlighted').removeClass('faded');
					// cy.elements().show();
					if (this.unfocused_elements) {
						this.unfocused_elements.restore();
					}
					if (do_layout_update)
						this.update();
				});
			}

			private static _icons: {};

			public static icons() {
				// if (typeof Graph._icons == 'undefined') {
				// 	Graph._icons = {};
				// 	// Graph._icons['undefined'] = String.fromCharCode(0xf128);
				// 	Graph._icons['no icon'] = '';
				//
				// 	let reg = new RegExp("^\\.fa-([^:]*)::before$");
				//
				// 	for (let i = 0; i< document.styleSheets.length; i++) {
				// 		let styleSheet = document.styleSheets[i];
				// 		let classes = styleSheet['rules'] || styleSheet['cssRules'];
				//
				// 		for (let x = 0; x < classes.length; x++) {
				// 			if (typeof classes[x].selectorText != 'string')
				// 				continue;
				//
				// 			let match = classes[x].selectorText.match(reg);
				// 			if (match) {
				// 				//console.warn("css ---- ", classes[x].selectorText, (classes[x].cssText) ? classes[x].cssText : classes[x].style.cssText );
				// 				if (!classes[x].style || !classes[x].style.content)
				// 					continue;
				// 				let icon = classes[x].style.content;
				// 				if (icon.length==3)
				// 					icon = icon[1];
				// 				Graph._icons[match[1]] = icon;
				// 			}
				// 		}
				// 	}
				// 	console.log(Graph._icons);
				// }
				// return Graph._icons;
				return VLA.UI.icons_set;
			}

			public static icon(name: string): string {
				let code = Graph.icons()[name];
				if (typeof code === 'undefined')
					code = Graph.icons()['no icon'];
				return code;
			}

			public static icon_name(icon: string): string {
				let name = "";
				let icons = Graph.icons();
				for (var key in icons) {
					if (!icons.hasOwnProperty(key))
						continue;

					if (icons[key] == icon)
						return key;
				}
				return name;
			}


			layout_options(layout_name) {
				let layouts = {};

				console.log("layout_options", layout_name);

				// The null layout puts all nodes at (0, 0). It’s useful for debugging purposes.
				layouts['null'] = {
					name: 'null',
					ready: function () {
					}, // on layoutready
					stop: function () {
					} // on layoutstop
				};

				// The random layout puts nodes in random positions within the viewport.
				layouts['random'] = {
					name: 'random',

					fit: false, // whether to fit to viewport
					padding: 30, // fit padding
					boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
					animate: false, // whether to transition the node positions
					animationDuration: 500, // duration of animation in ms if enabled
					animationEasing: undefined, // easing of animation if enabled
					ready: undefined, // callback on layoutready
					stop: undefined // callback on layoutstop
				};

				// The preset layout puts nodes in the positions you specify manually.
				layouts['preset'] = {
					name: 'preset',

					positions: undefined, // map of (node id) => (position obj); or function(node){ return somPos; }
					zoom: undefined, // the zoom level to set (prob want fit = false if set)
					pan: undefined, // the pan level to set (prob want fit = false if set)
					fit: false, // whether to fit to viewport
					padding: 30, // padding on fit
					animate: false, // whether to transition the node positions
					animationDuration: 500, // duration of animation in ms if enabled
					animationEasing: undefined, // easing of animation if enabled
					ready: undefined, // callback on layoutready
					stop: undefined // callback on layoutstop
				};

				// Compound Spring Embedder
				layouts['cose'] = {
					name: 'cose',

					// Called on `layoutready`
					ready: function () {
					},

					// Called on `layoutstop`
					stop: function () {
					},

					// Whether to animate while running the layout
					animate: true,

					// The layout animates only after this many milliseconds
					// (prevents flashing on fast runs)
					animationThreshold: 250,

					// Number of iterations between consecutive screen positions update
					// (0 -> only updated on the end)
					refresh: 20,

					// Whether to fit the network view after when done
					fit: false,

					// Padding on fit
					padding: 30,

					// Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
					boundingBox: undefined,

					// Randomize the initial positions of the nodes (true) or use existing positions (false)
					randomize: true,

					// Extra spacing between components in non-compound graphs
					componentSpacing: 100,

					// Node repulsion (non overlapping) multiplier
					nodeRepulsion: function (node) {
						return 400000;
					},

					// Node repulsion (overlapping) multiplier
					nodeOverlap: 20,

					// Ideal edge (non nested) length
					idealEdgeLength: function (edge) {
						return 100;
					},

					// Divisor to compute edge forces
					edgeElasticity: function (edge) {
						return 100;
					},

					// Nesting factor (multiplier) to compute ideal edge length for nested edges
					nestingFactor: 5,

					// Gravity force (constant)
					gravity: 80,

					// Maximum number of iterations to perform
					numIter: 1000,

					// Initial temperature (maximum node displacement)
					initialTemp: 200,

					// Cooling factor (how the temperature is reduced between consecutive iterations
					coolingFactor: 0.95,

					// Lower temperature threshold (below this point the layout will end)
					minTemp: 1.0,

					// Whether to use threading to speed up the layout
					useMultitasking: true
				};

				layouts['cose-bilkent'] = {
					name: 'cose-bilkent',
					// Called on `layoutready`
					ready: function () {
					},
					// Called on `layoutstop`
					stop: function () {
					},
					// Whether to fit the network view after when done
					fit: false,
					// Padding on fit
					padding: 10,
					// Whether to enable incremental mode
					randomize: true,
					// Node repulsion (non overlapping) multiplier
					nodeRepulsion: 4500,
					// Ideal edge (non nested) length
					idealEdgeLength: 50,
					// Divisor to compute edge forces
					edgeElasticity: 0.45,
					// Nesting factor (multiplier) to compute ideal edge length for nested edges
					nestingFactor: 0.1,
					// Gravity force (constant)
					gravity: 0.25,
					// Maximum number of iterations to perform
					numIter: 2500,
					// For enabling tiling
					tile: true,
					// Type of layout animation. The option set is {'during', 'end', false}
					animate: 'during',
					// Represents the amount of the vertical space to put between the zero degree members during the tiling operation(can also be a function)
					tilingPaddingVertical: 10,
					// Represents the amount of the horizontal space to put between the zero degree members during the tiling operation(can also be a function)
					tilingPaddingHorizontal: 10,
					// Gravity range (constant) for compounds
					gravityRangeCompound: 1.5,
					// Gravity force (constant) for compounds
					gravityCompound: 1.0,
					// Gravity range (constant)
					gravityRange: 3.8
				};

				layouts['grid'] = {name: 'grid', animate: true};
				layouts['concentric'] = {
					name: 'concentric',

					fit: false, // whether to fit the viewport to the graph
					padding: 30, // the padding on fit
					startAngle: 3 / 2 * Math.PI, // where nodes start in radians
					sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
					clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
					equidistant: false, // whether levels have an equal radial distance betwen them, may cause bounding box overflow
					minNodeSpacing: 10, // min spacing between outside of nodes (used for radius adjustment)
					boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
					avoidOverlap: true, // prevents node overlap, may overflow boundingBox if not enough space
					height: undefined, // height of layout area (overrides container height)
					width: undefined, // width of layout area (overrides container width)
					concentric: function (node) { // returns numeric value for each node, placing higher nodes in levels towards the centre
						return node.degree();
					},
					levelWidth: function (nodes) { // the variation of concentric values in each level
						return nodes.maxDegree() / 4;
					},
					animate: true, // whether to transition the node positions
					animationDuration: 500, // duration of animation in ms if enabled
					animationEasing: undefined, // easing of animation if enabled
					ready: undefined, // callback on layoutready
					stop: undefined // callback on layoutstop
				};

				layouts['breadthfirst'] = {
					name: 'breadthfirst',
					fit: false, // whether to fit the viewport to the graph
					directed: false, // whether the tree is directed downwards (or edges can point in any direction if false)
					padding: 150, // padding on fit
					circle: false, // put depths in concentric circles if true, put depths top down if false
					spacingFactor: 3.75, // positive spacing factor, larger => more space between nodes (N.B. n/a if causes overlap)
					boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
					avoidOverlap: true, // prevents node overlap, may overflow boundingBox if not enough space
					roots: undefined, // the roots of the trees
					maximalAdjustments: 0, // how many times to try to position the nodes in a maximal way (i.e. no backtracking)
					animate: true, // whether to transition the node positions
					animationDuration: 500, // duration of animation in ms if enabled
					animationEasing: undefined, // easing of animation if enabled
					ready: undefined, // callback on layoutready
					stop: undefined // callback on layoutstop
				};

				layouts['breadthfirst_c'] = $.extend({}, layouts['breadthfirst'], {
					name: 'breadthfirst',
					fit: false, // whether to fit the viewport to the graph
					circle: true, // put depths in concentric circles if true, put depths top down if false
				});

				layouts['cola'] = {
					name: 'cola',
					animate: true, // whether to show the layout as it's running
					refresh: 1, // number of ticks per frame; higher is faster but more jerky
					maxSimulationTime: 5000, // max length in ms to run the layout
					ungrabifyWhileSimulating: true, // so you can't drag nodes during layout
					fit: false, // on every layout reposition of nodes, fit the viewport
					padding: 30, // padding around the simulation
					boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }

					// layout event callbacks
					ready: function () {
					}, // on layoutready
					stop: function () {
					}, // on layoutstop

					// positioning options
					randomize: false, // use random node positions at beginning of layout
					avoidOverlap: false, // if true, prevents overlap of node bounding boxes
					handleDisconnected: true, // if true, avoids disconnected components from overlapping
					nodeSpacing: function (node) {
						return 10;
					}, // extra spacing around nodes
					flow: undefined, // use DAG/tree flow layout if specified, e.g. { axis: 'y', minSeparation: 30 }
					alignment: undefined, // relative alignment constraints on nodes, e.g. function( node ){ return { x: 0, y: 1 } }

					// different methods of specifying edge length
					// each can be a constant numerical value or a function like `function( edge ){ return 2; }`
					edgeLength: 200, // sets edge length directly in simulation
					edgeSymDiffLength: undefined, // symmetric diff edge length in simulation
					edgeJaccardLength: undefined, // jaccard edge length in simulation

					// iterations of cola algorithm; uses default values on undefined
					unconstrIter: undefined, // unconstrained initial layout iterations
					userConstIter: undefined, // initial layout iterations with user-specified constraints
					allConstIter: undefined, // initial layout iterations with all constraints including non-overlap

					// infinite layout options
					infinite: false // overrides all other options for a forces-all-the-time mode
				};

				layouts['cola_inf'] = $.extend({}, layouts['cola'], {
					name: 'cola',
					fit: false,
					ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
					infinite: true // overrides all other options for a forces-all-the-time mode
				});

				layouts['cytoscape-ngraph.forcelayout'] = {
					name: 'cytoscape-ngraph.forcelayout',
					async: {
						// tell layout that we want to compute all at once:
						maxIterations: 1000,
						stepsPerCycle: 30,

						// Run it till the end:
						waitForStep: false
					},
					physics: {
						/**
						 * Ideal length for links (springs in physical model).
						 */
						springLength: 300,

						/**
						 * Hook's law coefficient. 1 - solid spring.
						 */
						springCoeff: 0.0008,

						/**
						 * Coulomb's law coefficient. It's used to repel nodes thus should be negative
						 * if you make it positive nodes start attract each other :).
						 */
						gravity: -5.2,

						/**
						 * Theta coefficient from Barnes Hut simulation. Ranged between (0, 1).
						 * The closer it's to 1 the more nodes algorithm will have to go through.
						 * Setting it to one makes Barnes Hut simulation no different from
						 * brute-force forces calculation (each node is considered).
						 */
						theta: 0.8,

						/**
						 * Drag force coefficient. Used to slow down system, thus should be less than 1.
						 * The closer it is to 0 the less tight system will be.
						 */
						dragCoeff: 0.02,

						/**
						 * Default time step (dt) for forces integration
						 */
						timeStep: 20,
						iterations: 10000,
						fit: true,

						/**
						 * Maximum movement of the system which can be considered as stabilized
						 */
						stableThreshold: 0.000009
					},
					iterations: 10000,
					refreshInterval: 16, // in ms
					refreshIterations: 10, // iterations until thread sends an update
					stableThreshold: 2,
					animate: true,
					fit: true
				};

				return layouts[layout_name];
			};


			snapshot_image(format: Snapshot_image_format, only_selected = false): string {
				if (format != 'png' && format != 'jpg') {
					console.error("unsupported format:", format);
					return "";
				}

				if (!only_selected) {
					return this.cy[format]({bg: 'white', full: true});
				}
				else {
					let selected = this.cy.$(":selected");
					selected = selected.add(selected.children());
					let all_else = this.cy.$(":visible").difference(selected);
					let selected_connections = selected.edgesWith(selected); // edges between selected nodes

					all_else.not(selected_connections).hide();
					this.cy.fit(selected, 100);

					let img64 = this.cy.png({bg: 'white', full: false});

					all_else.show();

					return img64;
				}
			}

			snapshot_json_all() {
				return this.cy.json();
			}

			snapshot_json_selected() {
				let selected = this.cy.filter(":selected");
				selected = selected.add(selected.children());
				return selected.jsons();
			}

			snapshot_gexf_selected(): string {
				let selected = this.cy.filter(":selected");
				selected = selected.add(selected.children());
				return this.snapshot_gexf(selected);
			}

			snapshot_gexf_all(): string {
				return this.snapshot_gexf(this.cy.elements());
			}

			snapshot_gexf(elements: Cy.CollectionElements): string {
				let nodes: gexf.NodeDefinition[] = [];
				let edges: gexf.EdgeDefinition[] = [];
				let nodes_attributes: gexf.AttributeDefinition[] = [];

				let all_attributes = {};
				// collect attibutes
				elements.filter("node").forEach((node) => {
					let node_data = node.data();
					for (let key in node_data) {
						if (!node_data.hasOwnProperty(key))
							continue;
						if (all_attributes[key])
							continue;

						all_attributes[key] = "string";
					}
				});

				elements.filter("node").forEach((node) => {
					let gexf_node: gexf.NodeDefinition = {
						id: node.id(),
						label: node.data("_label"),
						attributes: {}
						// viz: // TODO: add viz export
					};
					let node_data = node.data();

					let attr_id = 0;
					for (let key in all_attributes) {
						if (!all_attributes.hasOwnProperty(key))
							continue;

						let value = node_data[key];
						if (typeof value != 'undefined') {
							gexf_node.attributes[attr_id] = value;
						}
						attr_id++;
					}

					nodes.push(gexf_node);
				});

				let attr_id = 0;
				for (let attr_name in all_attributes) {
					if (!all_attributes.hasOwnProperty(attr_name))
						continue;

					nodes_attributes.push({
						id: attr_id,
						title: attr_name,
						type: all_attributes[attr_name]
					});
					attr_id++;
				}

				elements.filter("edge").forEach((edge) => {
					let gexf_edge: gexf.EdgeDefinition = {
						id: edge.id(),
						source: edge.source().id(),
						target: edge.target().id(),
						// label: edge.data("_label"),
						// attributes: edge.data()
						// viz: // TODO: add viz export
					};
					edges.push(gexf_edge);
				});

				let myGexf: gexf.writer = gexf.create({
					model: {
						node: nodes_attributes
					},
					nodes: nodes,
					edges: edges
				});

				return myGexf.serialize();
			}


			selected_elements() {
				return this.cy.elements(":selected");
			}

			selected_nodes() {
				return this.cy.nodes(":selected");
			}

			selected_edges() {
				return this.cy.edges(":selected");
			}

			connect_nodes(source: Cy.CollectionNodes, target: Cy.CollectionNodes) {
				this.actions.do("connect", {source: source, target: target});
			}

			protected _connect_nodes(source: Cy.CollectionNodes, target: Cy.CollectionNodes): Cy.CollectionElements {
				this.cytoscape().startBatch();
				let collection = this.cytoscape().collection();
				source.forEach((n) => {
					let edge: Cy.ElementDefinition = {
						data: {
							source: n.id(),
							target: target.id(),
						},
						group: "edges",
					};
					let e = this.cy.add(edge);
					e.data("_type", "_default");
					collection = collection.add(e);
				});


				if (this.options.default_connection_type)
					this.set_edge_type(collection, this.options.default_connection_type);
				this.cytoscape().endBatch();


				this.highlight_element_neighbourhood(source.filter(":selected"));
				this.highlight_element_neighbourhood(target.filter(":selected"));

				return collection;
			}

			group_nodes(nodes: Cy.CollectionNodes): void {
				let cy_group: Cy.CollectionNodes;

				if (nodes.filter(":parent").length) {
					cy_group = nodes.filter(":parent");
				} else {
					let group: Cy.ElementDefinition = {data: {}};
					let bbox = nodes.boundingBox({
						includeNodes: true,
						includeLabels: false,
						includeShadows: false
					});

					let center = {
						x: bbox.x1 + bbox.w / 2,
						y: bbox.y1 + bbox.h / 2,
					};

					group.data['_label'] = "";
					group['position'] = center;

					cy_group = this.cytoscape().add(group);
					cy_group.addClass("compound");
				}

				this.change_nodes_parent(nodes, cy_group);

				cy_group.select();
				// this.update();
			}

			change_nodes_parent(nodes: Cy.CollectionNodes, parent: Cy.CollectionNodes) {

				this.cytoscape().batch(() => {
					// nodes.remove();

					let new_edges: Cy.EdgeDefinition[] = [];

					nodes.deselect();
					nodes.forEach((n) => {
						let this_is_compound_node_or_child = (n.isParent() || (parent && n.isChild()));
						let same_parent = (!parent && !n.parent()) || (parent && parent.id() == n.parent().id());

						if (this_is_compound_node_or_child || same_parent)
							return;

						let node: Cy.NodeDefinition = n.json();
						if (parent)
							node.data.parent = parent.id();
						else
							delete node.data.parent;

						// TODO: bug - sometimes nodes became ungrabbable
						node.grabbable = true; // hack...

						let edges = n.connectedEdges();
						edges.forEach((e) => {
							let edge: Cy.EdgeDefinition = e.json() as Cy.EdgeDefinition;

							new_edges.push(edge);
						});
						edges.remove();


						// remove original
						n.remove();
						// add modified clone
						this.cytoscape().add(node);

					});

					if (new_edges.length) {
						for (let edge of new_edges)
							this.cytoscape().add(edge);
					}

				});
			}

			ungroup_nodes(nodes: Cy.CollectionElements): void {
				if (nodes.isParent()) {
					nodes.deselect();
					let child_nodes = nodes.children();
					this.change_nodes_parent(child_nodes, null);
					nodes.remove();
				}
				else {
					let parent_node = nodes.parent();
					parent_node.deselect();

					this.change_nodes_parent(nodes, null);

					if (parent_node.children().length == 0)
						parent_node.remove();
				}
			}

			is_group(node: Cy.CollectionElements): boolean {
				return node.isParent();
			}

			is_inside_group(node: Cy.CollectionElements): boolean {
				return node.isChild();
			}

			hide_elements(collectionElements: Cy.CollectionElements): Cy.CollectionElements {
				return this.actions.do("hide", collectionElements);
			}

			_hide_elements(collectionElements: Cy.CollectionElements): Cy.CollectionElements {
				let affectedElements = this.cytoscape().collection().union(collectionElements);

				if (collectionElements.isParent()) {
					affectedElements = affectedElements.union(collectionElements.children());
					//collectionElements.children().unselect().hide();
				}
				//collectionElements.unselect().hide();
				affectedElements.unselect().hide();
				return affectedElements;
			}

			remove(collectionElements: Cy.CollectionElements) {
				collectionElements.unselect();//.remove();
				this.actions.do("remove", collectionElements);
				// update layout
				// this.update();
			}

			find_shortest_path(source: Cy.CollectionElements, target: Cy.CollectionElements) {
				let res = this.cytoscape().elements().aStar({root: source, goal: target});
				this.cytoscape().elements().removeClass("path-highlight");
				// console.log("res", res);
				if (res.found) {
					res.path.addClass("path-highlight");
				}
			}

			mark_degreeCentrality(elements: Cy.CollectionElements) {

				let dc = elements.degreeCentralityNormalized({directed: false}) as Cy.DegreeCentralityNormalizedUndirectedResult;
				elements.css({'background-color': "#ffffff"});
				elements.forEach((node) => {
					let value = dc.degree(node);

					// node.css({'background-opacity': value});
					node.css({'background-blacken': value});
				});

			}

			mark_betweennessCentrality(elements: Cy.CollectionElements) {

				// let ccn = elements.closenessCentralityNormalized();
				//
				// elements.forEach((node) => {
				// 	let closeness = ccn.closeness(node);
				// 	console.log("node", node.data('_label'));
				// 	console.log("closeness", closeness);
				// });

				let bc = elements.betweennessCentrality();
				elements.css({'background-color': "#ffffff"});
				elements.forEach((node) => {
					let value = bc.betweennessNormalized(node);

					// node.css({'background-opacity': value});
					node.css({'background-blacken': value});
				});

			}

			mark_closenessCentrality(elements: Cy.CollectionElements) {

				let ccn = elements.closenessCentralityNormalized();
				elements.css({'background-color': "#ffffff"});
				elements.forEach((node) => {
					let value = ccn.closeness(node);

					// node.css({'background-opacity': value});
					node.css({'background-blacken': value});
				});
			}

			unmark_centrality() {
				this.cytoscape().elements().removeCss();
				this.cytoscape().style().update();
			}

			clustering() {
				let clusters = this.cytoscape().elements().markovCluster({
					expandFactor: 2,        // affects time of computation and cluster granularity to some extent: M * M
					inflateFactor: 1,       // affects cluster granularity (the greater the value, the more clusters): M(i,j) / E(j)
					multFactor: 1,          // optional self loops for each node. Use a neutral value to improve cluster computations.
					maxIterations: 10,      // maximum number of iterations of the MCL algorithm in a single run
					attributes: [           // attributes/features used to group nodes, ie. similarity values between nodes
						function (edge) {
							let w = parseFloat(edge.data('weight'));
							if (isNaN(w))
								w = 1;
							
							return w;
						}

						// ... and so on
					]
				});

				console.log("CLUSTERS: ", clusters);

				// TODO: add palette?
				// Assign random colors to each cluster!
				for (let c = 0; c < clusters.length; c++) {
					clusters[c].style('background-color', '#' + Math.floor(Math.random() * 16777215).toString(16));
				}
			}
		}
	}
}