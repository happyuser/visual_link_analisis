<?php
//$_POST['uid'] = "21220";
//$_POST['csid'] = "12";
//$_POST['f'] = "test_zp.txt";
//$_POST['d'] = "12345";
/**
 * POST params
 * uid - user id
 * f - file name
 * d - file content
 * r - remark
 */
$userId = $_POST['uid'];
$caseSeedId = $_POST['csid'];
$filename = $_POST['f'];
$filedata = $_POST['d'];

$api_url = "http://188.166.40.105:8080/elementexploration-1.0.7-SNAPSHOT/rest/api/";
$command = "commonStorageDocument/uploadDocument";
// ?userId=21220&remarks=xxxx&fileTitle=yyyy
$params = array( 'userId' => $userId, 'remarks' => "", 'fileTitle' => $filename );
if ($caseSeedId)
	$params['caseseedId'] = $caseSeedId;


$errmsg = "";
if ($filedata != '')
{
	if (substr($filedata, 0, 21) == "data:image/png;base64")
	{
		$filedata = base64_decode(substr($filedata, 21));
	} elseif (substr($filedata, 0, 22) == "data:image/jpeg;base64")
	{
		$filedata = base64_decode(substr($filedata, 22));
	}

	if (!file_exists("files"))
		mkdir("files", 0755);

	$localfilename = time()."_".$filename;
	file_put_contents("files/".$localfilename, $filedata);

	$get_url = $api_url . $command . "?" . http_build_query($params);
	$headers = array("Content-Type:multipart/form-data"); // cURL headers for file uploading
	$postfields = array("uploadFile" => "@$filedata", "filename" => $filename);
	$filesize = strlen($filedata);
	$ch = curl_init();
	$options = array(
		CURLOPT_URL => $get_url,
//		CURLOPT_HEADER => true,
//		CURLOPT_POST => 1,
//		CURLOPT_SAFE_UPLOAD => 1,
//		CURLOPT_HTTPHEADER => $headers,
//		CURLOPT_POSTFIELDS => $postfields,
//		CURLOPT_INFILESIZE => $filesize,
		CURLOPT_RETURNTRANSFER => true
	); // cURL options
	curl_setopt_array($ch, $options);


	// curl_custom_postfields($ch, array("uploadFile" => $filedata), array("uploadFile" => "icons/person.png");
	curl_custom_postfiles($ch, array(), array("uploadFile" => array("filename" => $filename, "data" => $filedata)));

	$r = curl_exec($ch);

	if(!curl_errno($ch))
	{
		$info = curl_getinfo($ch);
		if ($info['http_code'] == 200)
			$errmsg = "OK,".$localfilename;
	}
	else
	{
		header("Status Code: 400", true, 400);
		$errmsg = curl_error($ch);
	}
	curl_close($ch);
}
else
{
	header("Status Code: 400", true, 400);
	$errmsg = "File data is missing";
}
echo $errmsg;

exit;



/**
 * For safe multipart POST request for PHP5.3 ~ PHP 5.4.
 *
 * @param resource $ch cURL resource
 * @param array $assoc "name => value"
 * @param array $files "name => path"
 * @return bool
 */
function curl_custom_postfields($ch, array $assoc = array(), array $files = array()) {

	// invalid characters for "name" and "filename"
	static $disallow = array("\0", "\"", "\r", "\n");

	// build normal parameters
	foreach ($assoc as $k => $v) {
		$k = str_replace($disallow, "_", $k);
		$body[] = implode("\r\n", array(
			"Content-Disposition: form-data; name=\"{$k}\"",
			"",
			filter_var($v),
		));
	}

	// build file parameters
	foreach ($files as $k => $v) {
		switch (true) {
			case false === $v = realpath(filter_var($v)):
			case !is_file($v):
			case !is_readable($v):
				continue; // or return false, throw new InvalidArgumentException
		}
		$data = file_get_contents($v);
		$v = call_user_func("end", explode(DIRECTORY_SEPARATOR, $v));
		$k = str_replace($disallow, "_", $k);
		$v = str_replace($disallow, "_", $v);
		$body[] = implode("\r\n", array(
			"Content-Disposition: form-data; name=\"{$k}\"; filename=\"{$v}\"",
			"Content-Type: application/octet-stream",
			"",
			$data,
		));
	}

	// generate safe boundary
	do {
		$boundary = "---------------------" . md5(mt_rand() . microtime());
	} while (preg_grep("/{$boundary}/", $body));

	// add boundary for each parameters
	array_walk($body, function (&$part) use ($boundary) {
		$part = "--{$boundary}\r\n{$part}";
	});

	// add final boundary
	$body[] = "--{$boundary}--";
	$body[] = "";

	// set options
	return @curl_setopt_array($ch, array(
		CURLOPT_POST       => true,
		CURLOPT_POSTFIELDS => implode("\r\n", $body),
		CURLOPT_HTTPHEADER => array(
			"Expect: 100-continue",
			"Content-Type: multipart/form-data; boundary={$boundary}", // change Content-Type
		),
	));
}

/**
 * For safe multipart POST request for PHP5.3 ~ PHP 5.4.
 *
 * @param resource $ch cURL resource
 * @param array $assoc "name => value"
 * @param array $files "name => { filename => content }"
 * @return bool
 */
function curl_custom_postfiles($ch, array $assoc = array(), array $files = array()) {

	// invalid characters for "name" and "filename"
	static $disallow = array("\0", "\"", "\r", "\n");

	// build normal parameters
	foreach ($assoc as $k => $v) {
		$k = str_replace($disallow, "_", $k);
		$body[] = implode("\r\n", array(
			"Content-Disposition: form-data; name=\"{$k}\"",
			"",
			filter_var($v),
		));
	}

	// build file parameters
	foreach ($files as $k => $v) {
		$data = $v['data'];
		$filename = $v['filename'];

		$k = str_replace($disallow, "_", $k);
		$filename = str_replace($disallow, "_", $filename);

		$body[] = implode("\r\n", array(
			"Content-Disposition: form-data; name=\"{$k}\"; filename=\"{$filename}\"",
			"Content-Type: application/octet-stream",
			"",
			$data,
		));
	}

	// generate safe boundary
	do {
		$boundary = "---------------------" . md5(mt_rand() . microtime());
	} while (preg_grep("/{$boundary}/", $body));

	// add boundary for each parameters
	array_walk($body, function (&$part) use ($boundary) {
		$part = "--{$boundary}\r\n{$part}";
	});

	// add final boundary
	$body[] = "--{$boundary}--";
	$body[] = "";

	// set options
	return @curl_setopt_array($ch, array(
		CURLOPT_POST       => true,
		CURLOPT_POSTFIELDS => implode("\r\n", $body),
		CURLOPT_HTTPHEADER => array(
			"Expect: 100-continue",
			"Content-Type: multipart/form-data; boundary={$boundary}", // change Content-Type
		),
	));
}